package patient.com.yoursmile.helper;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class SharedPref {

    private static final int MODE_PRIVATE = 0;
    private static SharedPref pref;
    private SharedPreferences appSharedPrefs;
    private Editor prefsEditor;

    private SharedPref(Context context) {
        this.appSharedPrefs = context.getSharedPreferences("patient_pref", MODE_PRIVATE);
    }

    public static SharedPref getInstance(Context context) {
        if (pref == null) {
            pref = new SharedPref(context);
        }
        return pref;
    }

    public boolean isAlertONTF() {
        return appSharedPrefs.getBoolean("AlertONTF", false);
    }

    public void setAlertONTF(boolean value) {
        this.prefsEditor = appSharedPrefs.edit();
        prefsEditor.putBoolean("AlertONTF", value);
        prefsEditor.commit();
    }

    public boolean isMenuRed() {
        return appSharedPrefs.getBoolean("MenuRed", false);
    }

    public void setMenuRed(boolean value) {
        appSharedPrefs.edit().putBoolean("MenuRed", value).apply();
    }

    public boolean isFirstTime() {
        return appSharedPrefs.getBoolean("FirstTime", true);
    }

    public void setFirstTime(boolean isVerified) {
        appSharedPrefs.edit().putBoolean("FirstTime", isVerified).apply();
    }

    public String getDoctorName() {
        return appSharedPrefs.getString("DoctorName", "");
    }

    public void setDoctorName(String value) {
        this.prefsEditor = appSharedPrefs.edit();
        prefsEditor.putString("DoctorName", value);
        prefsEditor.commit();
    }

    public String getAccessToken() {
        return appSharedPrefs.getString("AccessToken", "");
    }

    public void setAccessToken(String value) {
        this.prefsEditor = appSharedPrefs.edit();
        prefsEditor.putString("AccessToken", value);
        prefsEditor.apply();
    }

    public String getDoctorID() {
        return appSharedPrefs.getString("DoctorID", "");
    }

    public void setDoctorID(String value) {
        this.prefsEditor = appSharedPrefs.edit();
        prefsEditor.putString("DoctorID", value);
        prefsEditor.commit();
    }

    public String getPatientID() {
        return appSharedPrefs.getString("PatientID", "");
    }

    public void setPatientID(String value) {
        this.prefsEditor = appSharedPrefs.edit();
        prefsEditor.putString("PatientID", value);
        prefsEditor.commit();
    }

    public boolean isLoggedIn() {
        return appSharedPrefs.getBoolean("LoggedIn", false);
    }

    public void setLoggedIn(boolean value) {
        this.prefsEditor = appSharedPrefs.edit();
        prefsEditor.putBoolean("LoggedIn", value);
        prefsEditor.apply();
    }

    public String getLoginType() {
        return appSharedPrefs.getString("LoggedInType", null);
    }

    public void setLoginType(String value) {
        this.prefsEditor = appSharedPrefs.edit();
        prefsEditor.putString("LoggedInType", value);
        prefsEditor.apply();
    }


    public long getReminderTime() {
        return appSharedPrefs.getLong("reminder", 0);
    }

    public void setReminderTime(long time) {
        SharedPreferences.Editor editor = appSharedPrefs.edit();
        editor.putLong("reminder", time);
        editor.apply();
    }


    public String getTimeStamp() {
        return appSharedPrefs.getString("TimeStamp", "");
    }

    public void setTimeStamp(String value) {
        this.prefsEditor = appSharedPrefs.edit();
        prefsEditor.putString("TimeStamp", value);
        prefsEditor.commit();
    }

    public void setPrefClear() {
        this.prefsEditor = appSharedPrefs.edit();
        prefsEditor.clear();
        prefsEditor.commit();
    }

    public boolean isFirstTimeClickingPhoto() {
        return appSharedPrefs.getBoolean("firstTimeTakingPhotos", true);
    }

    public void setIsFirstTimeClickingPhoto(boolean b) {
        this.appSharedPrefs.edit().putBoolean("firstTimeTakingPhotos", b).apply();
    }
}