package patient.com.yoursmile.helper;

/**
 * Created by vineet on 29/8/15.
 */
public class PatientConstant {
    public final static String BASE_URL = "http://dentist.yoursmiledirect.com/doctors/public/main/";
    //    public final static String BASE_URL = "http://ec2-52-208-91-60.eu-west-1.compute.amazonaws.com/doctors/public/main/";
    public final static String PATIENT_URL = BASE_URL + "patient.json";
    public final static String SIGNUP_URL = BASE_URL + "signup.json";
    public final static String SIGNUP_NEW_URL = BASE_URL + "signup-new.json";
    public final static String LINK_DOCTOR = BASE_URL + "patient.json";
    public final static String ALBUM = BASE_URL + "album.json";
    public final static String PROFILE = BASE_URL + "patient.json";
    public final static String NOTIFICATION = BASE_URL + "notifications.json/";
    public final static String SETING = BASE_URL + "setting.json/";
    public final static String PLATFORM = "2";
    public final static String USERTYPE = "1";
    public final static String ACTION = "2";
    public static final String SENDER_ID = "573278349444";
    public static final String GCM = "GCM";

    public final static String NOTIFICATION_ACCEPTED = "accountAccepted";     // - Open Photo Request View
    public final static String NOTIFICATION_REJECTED = "albumRejected";       //  - Edit Album View
    public final static String NOTIFICATION_SOS = "sosactive";                //  -  Open Photo Request View
    public final static String NOTIFICATION_NEWALBUM = "newAlbumRequest";     // - Open Photo Request View
    public final static String NOTIFICATION_TREATMENT_COMP = "treatmentCompleted"; // Open Dashboard View
    public final static String NOTIFICATION_MORPH_COMP = "morphCreated";

    public final static String FB_LOGIN = "Facebook_Login";
    public final static String G_LOGIN = "Google_Login";
    public final static String S_LOGIN = "Smile_Login";

    public static final CharSequence PROFILE_IMAGE_BASE_URL = "http://ec2-52-208-91-60.eu-west-1.compute.amazonaws.com/";
}
