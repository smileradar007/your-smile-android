package patient.com.yoursmile.helper;

import android.util.Log;

public class Logger {
    public static boolean logstatus = false;

    private Logger() {
    }

    public static void warn(String tag, String s) {
        if (logstatus)
            Log.w(tag, s);
    }

    public static void warn(String tag, String s, Throwable throwable) {
        if (logstatus)
            Log.w(tag, s, throwable);
    }

    public static void warn(String tag, Throwable throwable) {
        if (logstatus)
            Log.w(tag, throwable);
    }

    public static void verbose(String tag, String s) {
        if (logstatus)
            Log.v(tag, s);
    }

    public static void debug(String tag, String s) {
        if (logstatus)
            Log.d(tag, s);
    }

    public static void info(String tag, String s) {
        if (logstatus)
            Log.i(tag, s);
    }

    public static void info(String tag, String s, Throwable throwable) {
        if (logstatus)
            Log.i(tag, s, throwable);
    }

    public static void error(String tag, String s) {
        if (logstatus)
            Log.e(tag, s);
    }

    public static void error(String tag, Throwable throwable) {
        if (logstatus)
            Log.e(tag, null, throwable);
    }

    public static void error(String tag, String s, Throwable throwable) {
        if (logstatus)
            Log.e(tag, s, throwable);
    }
}
