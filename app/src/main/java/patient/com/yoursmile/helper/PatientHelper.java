package patient.com.yoursmile.helper;

import android.Manifest;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.provider.CalendarContract;
import android.support.v4.app.ActivityCompat;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import patient.com.yoursmile.R;

public class PatientHelper {

    private static PatientHelper helper;

    private PatientHelper() {
    }

    public static PatientHelper getInstance() {
        if (helper == null)
            helper = new PatientHelper();
        return helper;
    }

    public static String getPath(Bitmap bitmap, Activity activity) {
        FileOutputStream out = null;
        String path = createNewFile(activity);
        try {
            out = new FileOutputStream(path);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 70, out); // bmp is your Bitmap instance
            // PNG is a lossless format, the compression factor (100) is ignored
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return path;
    }

    public static String createNewFile(Context context) {
        String dirctory = Environment.getExternalStorageDirectory() + "/" + context.getString(R.string.app_name);
        File f = new File(dirctory);
        boolean isdirectory;
        isdirectory = f.isDirectory() || f.mkdir();
        if (isdirectory) {
            dirctory = dirctory + "/" + Calendar.getInstance().getTimeInMillis() + "captureImage.jpg";
            f = new File(dirctory);
            if (f.isFile())
                f.delete();
            try {
                f.createNewFile();

                return dirctory;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return dirctory;
    }

    public boolean emailValidate(String email) { // validate the entered
        String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        Pattern pattern;
        Matcher matcher;
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email);
        return matcher.matches();
    }

    public static String getDateFromString(String dateStr) {
        try {
            String pattern = "yyyy-MM-dd hh:mm:ss";
            SimpleDateFormat format = new SimpleDateFormat(pattern, Locale.getDefault());
            format.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date date = format.parse(dateStr);
//			Date date = format.parse(dateStr);
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd MMM yyyy hh:mm a");
            return simpleDateFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
            return dateStr;
        }
    }

    public void showExitToast(Context context, String message) {
        LayoutInflater inflater = ((Activity) context).getLayoutInflater();
        View layout = inflater.inflate(R.layout.toast_calender, (ViewGroup) ((Activity) context).findViewById(R.id.ll_toast));
        TextView msgTv = (TextView) layout.findViewById(R.id.tv_msg);
        msgTv.setText(message);
        Toast toast = new Toast(context);
        toast.setGravity(Gravity.BOTTOM, 0, 0);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setView(layout);
        toast.show();
    }

    public static String getImageString(String imageURL) {
        if (imageURL != null && imageURL.length() > 0) {
            if (imageURL.startsWith("http://demo.sawbros.com/")) {
                imageURL = imageURL.replace("http://demo.sawbros.com/", "http://demo.sawbros.com/doctors/public/");
            }
        }
        return imageURL;
    }

    public static String getNewImageString(String imageURL) {
        if (imageURL != null && imageURL.length() > 0) {
            if (imageURL.startsWith("http://dentist.yoursmiledirect.com")) {
                imageURL = imageURL.replace("http://dentist.yoursmiledirect.com", PatientConstant.PROFILE_IMAGE_BASE_URL+"doctors/public");
            }
        }
        return imageURL;
    }

    public static long pushAppointmentsToCalender(Activity curActivity, String title, String addInfo, String place, int status, long startDate, boolean needReminder, boolean needMailService) {
        /***************** Event: note(without alert) *******************/

        long eventID = 0;
        try {
            String eventUriString = "content://com.android.calendar/events";
            ContentValues eventValues = new ContentValues();

            eventValues.put("calendar_id", 1); // id, We need to choose from
            // our mobile for primary
            // its 1
            eventValues.put("title", title);
            eventValues.put("description", addInfo);
            eventValues.put("eventLocation", place);

            TimeZone timeZone = TimeZone.getDefault();


            eventValues.put(CalendarContract.Events.EVENT_TIMEZONE, timeZone.getID());

            long endDate = startDate + 1000 * 60 * 60; // For next 1hr

            eventValues.put("dtstart", startDate);
            eventValues.put("dtend", endDate);
            eventValues.put("eventStatus", status); // This information is

            eventValues.put("hasAlarm", 1); // 0 for false, 1 for true

            Uri eventUri = curActivity.getApplicationContext().getContentResolver().insert(Uri.parse(eventUriString), eventValues);
            eventID = Long.parseLong(eventUri.getLastPathSegment());

            if (needReminder) {
                /***************** Event: Reminder(with alert) Adding reminder to event *******************/

                String reminderUriString = "content://com.android.calendar/reminders";

                ContentValues reminderValues = new ContentValues();

                reminderValues.put("event_id", eventID);
                reminderValues.put("minutes", 5); // Default value of the
                // system. Minutes is a
                // integer
                reminderValues.put("method", 1); // Alert Methods: Default(0),
                // Alert(1), Email(2),
                // SMS(3)

                Uri reminderUri = curActivity.getApplicationContext().getContentResolver().insert(Uri.parse(reminderUriString), reminderValues);
            }

            /***************** Event: Meeting(without alert) Adding Attendies to the meeting *******************/

            if (needMailService) {
                String attendeuesesUriString = "content://com.android.calendar/attendees";

                /********
                 * To add multiple attendees need to insert ContentValues multiple
                 * times
                 ***********/
                ContentValues attendeesValues = new ContentValues();

                attendeesValues.put("event_id", eventID);
                attendeesValues.put("attendeeName", "xxxxx"); // Attendees name
                attendeesValues.put("attendeeEmail", "yyyy@gmail.com");// Attendee
                // E
                // mail
                // id
                attendeesValues.put("attendeeRelationship", 0); // Relationship_Attendee(1),
                // Relationship_None(0),
                // Organizer(2),
                // Performer(3),
                // Speaker(4)
                attendeesValues.put("attendeeType", 0); // None(0), Optional(1),
                // Required(2), Resource(3)
                attendeesValues.put("attendeeStatus", 0); // NOne(0), Accepted(1),
                // Decline(2),
                // Invited(3),
                // Tentative(4)

                Uri attendeuesesUri = curActivity.getApplicationContext().getContentResolver().insert(Uri.parse(attendeuesesUriString), attendeesValues);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }


        return eventID;
    }

    // Add an event to the calendar of the user.
    public static void addEvent(Context context, Calendar calDate, String _title, String _description) {
        try {
            ContentResolver cr = context.getContentResolver();
            ContentValues values = new ContentValues();
            values.put(CalendarContract.Events.DTSTART, calDate.getTimeInMillis());
            values.put(CalendarContract.Events.DTEND, calDate.getTimeInMillis() + 60 * 60 * 1000);
            values.put(CalendarContract.Events.TITLE, _title);
            values.put(CalendarContract.Events.DESCRIPTION, _description);
            values.put(CalendarContract.Events.CALENDAR_ID, 1);
            values.put(CalendarContract.Events.HAS_ALARM,1);
            values.put(CalendarContract.Events.EVENT_TIMEZONE, Calendar.getInstance().getTimeZone().getID());
            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.WRITE_CALENDAR) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            Uri uri = cr.insert(CalendarContract.Events.CONTENT_URI, values);

			// Save the eventId into the Task object for possible future delete.
            long _eventId = Long.parseLong(uri.getLastPathSegment());
			// Add a 5 minute, 1 hour and 1 day reminders (3 reminders)
			setReminder(cr, _eventId, 5);
			setReminder(cr, _eventId, 60);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// routine to add reminders with the event
	private static void setReminder(ContentResolver cr, long eventID, int timeBefore) {
		try {
			ContentValues values = new ContentValues();
			values.put(CalendarContract.Reminders.MINUTES, timeBefore);
			values.put(CalendarContract.Reminders.EVENT_ID, eventID);
			values.put(CalendarContract.Reminders.METHOD, CalendarContract.Reminders.METHOD_ALERT);
			Uri uri = cr.insert(CalendarContract.Reminders.CONTENT_URI, values);
			Cursor c = CalendarContract.Reminders.query(cr, eventID,
					new String[]{CalendarContract.Reminders.MINUTES});
			if (c.moveToFirst()) {
				System.out.println("calendar"
						+ c.getInt(c.getColumnIndex(CalendarContract.Reminders.MINUTES)));
			}

            c.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
