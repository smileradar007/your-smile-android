package patient.com.yoursmile.utils;

import android.annotation.SuppressLint;
import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;

import com.google.android.gms.gcm.GoogleCloudMessaging;

import patient.com.yoursmile.R;
import patient.com.yoursmile.activity.AlbumActivity;
import patient.com.yoursmile.activity.HomeActivity;
import patient.com.yoursmile.activity.TakePhotoActivity;
import patient.com.yoursmile.helper.Logger;
import patient.com.yoursmile.helper.PatientConstant;

/**
 * This is a service class for handing push of requester and runner.
 */

public class GCMIntentService extends IntentService {

    public static int NOTIFICATION_ID = 1;

    public GCMIntentService() {
        super(PatientConstant.SENDER_ID);
    }

    /**
     * Issues a notification to inform the user that server has sent a message.
     */
    @Override
    protected void onHandleIntent(Intent intent) {
        Bundle extras = intent.getExtras();
        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
        String messageType = gcm.getMessageType(intent);
        String typeNotification = "";
        String id = "";
        Logger.error("Push ", "Push Notification=====" + extras);
        if (!extras.isEmpty()) {
            if (GoogleCloudMessaging.MESSAGE_TYPE_SEND_ERROR.equals(messageType)) {
                Logger.error("error", "error");
            } else if (GoogleCloudMessaging.MESSAGE_TYPE_DELETED.equals(messageType)) {
                Logger.error("error", "error");
            } else if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE.equals(messageType)) {
                boolean foregorund = Foreground.init(getApplication()).isForeground();
                String message = extras.getString("alert");
                typeNotification = extras.getString("type");
                id = extras.getString("id");
                Logger.error(" Push ", message + " " + typeNotification + " " + id);
//				public final static String NOTIFICATION_ACCEPTED="accountAccepted";
//				public final static String NOTIFICATION_REJECTED="albumRejected";       //  - Edit Album View
//				public final static String NOTIFICATION_SOS="sosactive";                //  -  Open Photo Request View
//				public final static String NOTIFICATION_NEWALBUM="newAlbumRequest";     // - Open Photo Request View
//				public final static String NOTIFICATION_TREATMENT_COMP="treatmentCompleted"; // Open Dashboard View
//				public final static String NOTIFICATION_MORPH_COMP = "morphCreated";

                if (foregorund) {
                    sendBroadCastForAction(typeNotification);
                } else {
                    if (typeNotification != null) {
                        if (typeNotification.equalsIgnoreCase(PatientConstant.NOTIFICATION_ACCEPTED)) {
                            Intent notificationIntent = new Intent(GCMIntentService.this, TakePhotoActivity.class);
                            Bundle bundle = new Bundle();
                            bundle.putString("Message", message);
                            bundle.putString("Action", typeNotification);
                            notificationIntent.putExtras(bundle);
                            sendNotification(notificationIntent, message);
                        } else if (typeNotification.equalsIgnoreCase(PatientConstant.NOTIFICATION_REJECTED)) {
                            String albumid = extras.getString("object_id");
                            Logger.error("Album id", albumid);
                            Intent notificationIntent = new Intent(GCMIntentService.this, AlbumActivity.class);
                            Bundle bundle = new Bundle();
                            bundle.putString("ID", albumid);
                            bundle.putString("Message", message);
                            bundle.putString("Action", typeNotification);
                            notificationIntent.putExtras(bundle);
                            sendNotification(notificationIntent, message);
                        } else if (typeNotification.equalsIgnoreCase(PatientConstant.NOTIFICATION_SOS)) {
                            Intent notificationIntent = new Intent(GCMIntentService.this, TakePhotoActivity.class);
                            Bundle bundle = new Bundle();
                            bundle.putString("Message", message);
                            bundle.putString("Action", typeNotification);
                            notificationIntent.putExtras(bundle);
                            sendNotification(notificationIntent, message);
                        } else if (typeNotification.equalsIgnoreCase(PatientConstant.NOTIFICATION_NEWALBUM)) {
                            Intent notificationIntent = new Intent(GCMIntentService.this, TakePhotoActivity.class);
                            Bundle bundle = new Bundle();
                            bundle.putString("Message", message);
                            bundle.putString("Action", typeNotification);
                            notificationIntent.putExtras(bundle);
                            sendNotification(notificationIntent, message);
                        } else if (typeNotification.equalsIgnoreCase(PatientConstant.NOTIFICATION_TREATMENT_COMP)) {
                            Intent notificationIntent = new Intent(GCMIntentService.this, HomeActivity.class);
                            Bundle bundle = new Bundle();
                            bundle.putString("Message", message);
                            bundle.putString("Action", typeNotification);
                            notificationIntent.putExtras(bundle);
                            sendNotification(notificationIntent, message);
                        } else if (typeNotification.equalsIgnoreCase(PatientConstant.NOTIFICATION_MORPH_COMP)) {
                            Intent notificationIntent = new Intent(GCMIntentService.this, HomeActivity.class);
                            Bundle bundle = new Bundle();
                            bundle.putString("Message", message);
                            bundle.putString("Action", typeNotification);
                            notificationIntent.putExtras(bundle);
                            sendNotification(notificationIntent, message);
                        }
                    }
                }
            }
        }
        GcmBroadcastReceiver.completeWakefulIntent(intent);
    }

    @SuppressLint("NewApi")
    private void sendNotification(Intent notificationIntent, String message) {
        NotificationManager mNotificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        PendingIntent contentIntent = PendingIntent.getActivity(this, NOTIFICATION_ID, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle(getString(R.string.app_name))
                        .setContentText(message)
                        .setAutoCancel(true)
                        .setNumber(NOTIFICATION_ID);

        mBuilder.setDefaults(Notification.DEFAULT_SOUND);
        mBuilder.setContentIntent(contentIntent);

        Notification notification = mBuilder.build();
        notification.flags |= Notification.FLAG_AUTO_CANCEL;
        mNotificationManager.notify(NOTIFICATION_ID, notification);
    }

    private void sendBroadCastForAction(String action) {
        Intent intent = new Intent(action);
        LocalBroadcastManager.getInstance(GCMIntentService.this).sendBroadcast(intent);
    }
}



