package patient.com.yoursmile.utils;

import android.content.Context;

import com.google.android.gms.gcm.GcmNetworkManager;
import com.google.android.gms.gcm.OneoffTask;
import com.google.android.gms.gcm.PeriodicTask;
import com.google.android.gms.gcm.Task;

/**
 * Created by mohitesh on 30/01/17.
 */

public class GcmNetworkManagerUtil {

    public static void schedulePeriodicTask(Context context, String tags, long period, long flex) {
        Task task = new PeriodicTask.Builder()
                .setService(CustomTaskService.class)
                .setPeriod(period)
                .setFlex(flex)
                .setTag(tags)
                .setRequiresCharging(true)
                .build();

        GcmNetworkManager.getInstance(context).schedule(task);
    }


    public static void scheduleOneTimeTask(Context context, String tags, long startTime, long endTime) {

        Task task = new OneoffTask.Builder()
                .setService(CustomTaskService.class)
                .setExecutionWindow(startTime, endTime)
                .setTag(tags)
                .setUpdateCurrent(true)
                .setPersisted(true)
                .setRequiresCharging(false)
                .setRequiredNetwork(Task.NETWORK_STATE_ANY)
                .build();

        GcmNetworkManager.getInstance(context).schedule(task);
    }

}
