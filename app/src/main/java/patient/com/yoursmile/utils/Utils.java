package patient.com.yoursmile.utils;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.provider.Settings;

import java.util.Date;

/**
 * Created by mohitesh on 18/10/16.
 */

public class Utils {

    public static void goToSettings(Context context) {
        Intent myAppSettings = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.parse("package:" + context.getPackageName()));
        myAppSettings.addCategory(Intent.CATEGORY_DEFAULT);
        myAppSettings.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(myAppSettings);
    }

    public static int getSecondsDifference(long startTime, long endTime) {
        int diff = -1;
        try {
            Date dateStart =new Date(startTime);
            Date dateEnd = new Date(endTime);

            //time is always 00:00:00 so rounding should help to ignore the missing hour when going from winter to summer time as well as the extra hour in the other direction
            diff = (int) Math.round((endTime - startTime) / (double) 1000);
        } catch (Exception e) {
            //handle the exception according to your own situation
        }
        return diff;
    }
}
