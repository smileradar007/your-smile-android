package patient.com.yoursmile.fragment;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import patient.com.yoursmile.R;
import patient.com.yoursmile.activity.HomeActivity;
import patient.com.yoursmile.helper.Blur;
import patient.com.yoursmile.helper.CircleTransform;
import patient.com.yoursmile.helper.Logger;
import patient.com.yoursmile.helper.PatientConstant;
import patient.com.yoursmile.helper.PatientHelper;
import patient.com.yoursmile.model.PatientModel;
import patient.com.yoursmile.model.ResponseModel;
import patient.com.yoursmile.network.GenericNetworkInterface;
import patient.com.yoursmile.network.GetMyProfileAsyncTask;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MyProfileFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link MyProfileFragment#newInstance} factory method to
 * create an instance of this fragment.
 */

public class MyProfileFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    public PatientModel patientModel;
    @BindView(R.id.txtDescription)
    TextView txtDescription;
    @BindView(R.id.txtContact)
    TextView txtContact;
    @BindView(R.id.txtEmail)
    TextView txtEmail;
    @BindView(R.id.txtAddress)
    TextView txtAddress;
    @BindView(R.id.txtName)
    TextView txtName;
    @BindView(R.id.imgBanner)
    ImageView imgBanner;
    @BindView(R.id.imgProfile)
    ImageView imgProfile;
    @BindView(R.id.layProfile)
    LinearLayout layProfile;
    Target target = new Target() {
        @Override
        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
            if (bitmap != null) {
                ByteArrayOutputStream out = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.PNG, 70, out);
                Bitmap decoded = BitmapFactory.decodeStream(new ByteArrayInputStream(out.toByteArray()));
                try {
                    if (getActivity() != null) {
                        Bitmap bitmap1 = Blur.fastblur(getActivity(), decoded, 1);
                        imgBanner.setImageBitmap(bitmap1);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onBitmapFailed(Drawable errorDrawable) {
        }

        @Override
        public void onPrepareLoad(Drawable placeHolderDrawable) {
        }
    };
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public MyProfileFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment MyProfileFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MyProfileFragment newInstance(String param1, String param2) {
        MyProfileFragment fragment = new MyProfileFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_my_profile, container, false);
        ButterKnife.bind(this, view);
        getProfile();
        return view;
    }


    private void getProfile() {
        new GetMyProfileAsyncTask(getActivity(), new GenericNetworkInterface() {
            @Override
            public void networkResponse(ResponseModel responseModel) {
                if (responseModel != null && responseModel.getStatus() == 1) {
                    if (responseModel.getObject() != null && (responseModel.getObject() instanceof PatientModel)) {
                        patientModel = (PatientModel) responseModel.getObject();
                        setProfile(patientModel);
                    }
                } else if (responseModel != null) {
                    PatientHelper.getInstance().showExitToast(getActivity(), responseModel.getMessage());
                }
            }
        }).execute(PatientConstant.PROFILE);
    }

    public void setProfile(PatientModel model) {
        if (model != null) {
            Activity activity = getActivity();
            if (isAdded() && activity instanceof HomeActivity) {
                ((HomeActivity) activity).showEditProfile();
            }

            layProfile.setVisibility(View.VISIBLE);
            patientModel = model;
            txtName.setText(patientModel.getName());
            txtEmail.setText(patientModel.getEmail());
            txtAddress.setText(patientModel.getAddress());
            txtContact.setText(patientModel.getContact());
            txtDescription.setText(patientModel.getDescription());
            imgBanner.setImageBitmap(null);
            if (getActivity() != null)
                imgBanner.setBackgroundColor(getActivity().getResources().getColor(R.color.banner_bg));
            imgProfile.setImageResource(R.drawable.profileplaceholder);

            if (patientModel.getProfileimage() != null && patientModel.getProfileimage().length() > 0) {
                //TODO: here replace the base image url with updated url, to download image
                String imagePath = patientModel.getProfileimage();
                Logger.error("before image path", imagePath);
                if (imagePath.contains("http://dentist.yoursmiledirect.com"))
                    imagePath = imagePath.replace("http://dentist.yoursmiledirect.com",
                            (PatientConstant.PROFILE_IMAGE_BASE_URL + "doctors/public"));

                Logger.error("after image path", imagePath);

                if (getActivity() != null)
                    Picasso.with(getActivity()).load(imagePath)
                            .placeholder(R.drawable.profileplaceholder)
                            .transform(new CircleTransform()).into(imgProfile);
                if (getActivity() != null)
                    Picasso.with(getActivity()).load(imagePath)
                            .into(target);
            }
        }
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();

    }


    @OnClick(R.id.imgProfile)
    public void clickOnProfile() {
        if (isAdded() && patientModel != null) {
            Activity activity = getActivity();
            if (activity != null && activity instanceof HomeActivity) {
                ((HomeActivity) activity).showImage(patientModel.getProfileimage());
            }
        }
    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }
}