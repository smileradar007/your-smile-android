package patient.com.yoursmile.fragment;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import org.json.JSONObject;

import patient.com.yoursmile.helper.PatientConstant;
import patient.com.yoursmile.helper.PatientHelper;
import patient.com.yoursmile.helper.SharedPref;
import patient.com.yoursmile.model.ResponseModel;
import patient.com.yoursmile.model.SettingModel;
import patient.com.yoursmile.network.GenericNetworkInterface;
import patient.com.yoursmile.network.GetSettingAsyncTask;
import patient.com.yoursmile.network.SetSettingAsyncTask;
import patient.com.yoursmile.activity.ChangePassActivity;
import patient.com.yoursmile.R;
import patient.com.yoursmile.activity.TutorialCartoonActivity;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link SettingFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link SettingFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SettingFragment extends Fragment {


    private int push=1,sound=1;
    private ProgressBar loadingProgressBar;

    private ImageView soundToggleButton,pushToggleButton;
    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment SettingFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static SettingFragment newInstance(String param1, String param2) {
        SettingFragment fragment = new SettingFragment();
        return fragment;
    }

    public SettingFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    private void callService()
    {
        loadingProgressBar.setVisibility(View.VISIBLE);
        new GetSettingAsyncTask(getActivity(), new GenericNetworkInterface() {
            @Override
            public void networkResponse(ResponseModel responseModel) {
                loadingProgressBar.setVisibility(View.INVISIBLE);
                if(responseModel!=null && responseModel.getStatus()==1)
                {
                    if(responseModel.getObject()!=null && responseModel.getObject() instanceof SettingModel)
                    {
                        SettingModel settingModel = (SettingModel) responseModel.getObject();
                        setSetting(settingModel);
                    }
                }
                else if(responseModel!=null){
                    PatientHelper.getInstance().showExitToast(getActivity(),responseModel.getMessage());
                }
            }
        }).execute(PatientConstant.SETING + "usertype/" + PatientConstant.USERTYPE);
    }

   /* public static void setTint(ProgressBar progressBar, int color) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            ColorStateList stateList = ColorStateList.valueOf(color);
            progressBar.setProgressTintList(stateList);
            progressBar.setSecondaryProgressTintList(stateList);
            progressBar.setIndeterminateTintList(stateList);
        } else {
            PorterDuff.Mode mode = PorterDuff.Mode.SRC_IN;
            if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.GINGERBREAD_MR1) {
                mode = PorterDuff.Mode.MULTIPLY;
            }
            if (progressBar.getIndeterminateDrawable() != null)
                progressBar.getIndeterminateDrawable().setColorFilter(color, mode);
            if (progressBar.getProgressDrawable() != null)
                progressBar.getProgressDrawable().setColorFilter(color, mode);
        }
    }*/


    private void setSettingJson( int push, int sound)
    {

        //{"sound":"1","pushNotification":"0","appType":"2","userId":"1"}
        try {
            JSONObject settingJson = new JSONObject();
            settingJson.put("sound",sound);
            settingJson.put("pushNotification",push);
            settingJson.put("appType","1");
            settingJson.put("userId",pref.getPatientID());


            new SetSettingAsyncTask(getActivity(), new GenericNetworkInterface() {
                @Override
                public void networkResponse(ResponseModel responseModel) {
                    if (responseModel != null && responseModel.getStatus() == 1) {
                        if(responseModel.getObject()!=null && responseModel.getObject() instanceof SettingModel)
                        {
                            SettingModel settingModel = (SettingModel) responseModel.getObject();
                            setSetting(settingModel);
                        }
                    }
                }
            }, settingJson).execute();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void setSetting(SettingModel settingModel)
    {
        soundToggleButton.setImageBitmap(null);
        pushToggleButton.setImageBitmap(null);

        Drawable onDrawable = getResources().getDrawable(R.drawable.automated_switch_on);
        Drawable offDrawable = getResources().getDrawable(R.drawable.automated_switch_off);

        if(settingModel.getSound()==1) {
            sound = 1;
            soundToggleButton.setImageDrawable(onDrawable);
        }
        else{
            sound = 0;
            soundToggleButton.setImageDrawable(offDrawable);
        }


        if(settingModel.getPushNotification()==1)
        {
            push =1;
            pushToggleButton.setImageDrawable(onDrawable);
        }
        else{
            push=0;
            pushToggleButton.setImageDrawable(offDrawable);
        }
    }

    private View view;
    private SharedPref pref;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, final Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_setting, container, false);

        pref = SharedPref.getInstance(getActivity());
        loadingProgressBar = (ProgressBar)view.findViewById(R.id.pbLoading);

        soundToggleButton = (ImageView) view.findViewById(R.id.tgSound);
        pushToggleButton  = (ImageView) view.findViewById(R.id.tgPush);

        soundToggleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sound == 1) {
                    sound = 0;
                } else {
                    sound = 1;
                }
                setSettingJson(push, sound);
            }
        });

        pushToggleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (push == 1) {
                    push = 0;
                } else {
                    push = 1;
                }
                setSettingJson(push, sound);
            }
        });

        view.findViewById(R.id.lay3).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), TutorialCartoonActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });

        if(pref.getLoginType().equalsIgnoreCase(PatientConstant.S_LOGIN))
        {
            view.findViewById(R.id.lay4).setVisibility(View.VISIBLE);

            view.findViewById(R.id.lay4).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getActivity(), ChangePassActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                }
            });
        }


        callService();
        return  view;
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();

    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

}
