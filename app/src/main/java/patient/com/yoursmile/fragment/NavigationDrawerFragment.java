package patient.com.yoursmile.fragment;

import android.app.Activity;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import patient.com.yoursmile.activity.HomeActivity;
import patient.com.yoursmile.R;
import patient.com.yoursmile.adapter.SideMenuListAdapter;
import patient.com.yoursmile.helper.PatientConstant;
import patient.com.yoursmile.helper.SharedPref;
import patient.com.yoursmile.model.DoctorProfileModel;
import patient.com.yoursmile.model.MenuModel;
import patient.com.yoursmile.model.ResponseModel;
import patient.com.yoursmile.network.GenericNetworkInterface;
import patient.com.yoursmile.network.GetDoctorProfileAsynctask;

/**
 * Fragment used for managing interactions for and presentation of a navigation drawer.
 * See the <a href="https://developer.android.com/design/patterns/navigation-drawer.html#Interaction">
 * design guidelines</a> for a complete explanation of the behaviors implemented here.
 */
public class NavigationDrawerFragment extends Fragment {

    /**
     * Remember the position of the selected item.
     */
    private static final String STATE_SELECTED_POSITION = "selected_navigation_drawer_position";

    /**
     * Per the design guidelines, you should show the drawer on launch until the user manually
     * expands it. This shared preference tracks this.
     */
    private static final String PREF_USER_LEARNED_DRAWER = "navigation_drawer_learned";

    /**
     * A pointer to the current callbacks instance (the Activity).
     */
    private NavigationDrawerCallbacks mCallbacks;
    /**
     * Helper component that ties the action bar to the navigation drawer.
     */
    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerLayout mDrawerLayout;
    private View mFragmentContainerView;
    private int mCurrentSelectedPosition = 4;
    private boolean mFromSavedInstanceState;
    private boolean mUserLearnedDrawer;
    public SideMenuListAdapter sideMenuListAdapter;
    private SharedPref pref;
    private Activity mActivity;
    public NavigationDrawerFragment() {
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        List<MenuModel> menuModelList = new ArrayList<MenuModel>();
        menuModelList.add(new MenuModel(R.drawable.myprofile,R.string.my_profile));
        menuModelList.add(new MenuModel(R.drawable.home,R.string.doctor_info));
        menuModelList.add(new MenuModel(R.drawable.notification,R.string.notifications));
        menuModelList.add(new MenuModel(R.drawable.album,R.string.album));
        menuModelList.add(new MenuModel(R.drawable.photorequest,R.string.photo_request));
        menuModelList.add(new MenuModel(R.drawable.share_icon,R.string.treatment_outcome));
        menuModelList.add(new MenuModel(R.drawable.setting,R.string.settings));
        menuModelList.add(new MenuModel(R.drawable.logout,R.string.log_out));
        sideMenuListAdapter = new SideMenuListAdapter(getActivity(),menuModelList);

        // Read in the flag indicating whether or not the user has demonstrated awareness of the
        // drawer. See PREF_USER_LEARNED_DRAWER for details.
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getActivity());
        mUserLearnedDrawer = sp.getBoolean(PREF_USER_LEARNED_DRAWER, false);

        if (savedInstanceState != null) {
            mCurrentSelectedPosition = savedInstanceState.getInt(STATE_SELECTED_POSITION);
            mFromSavedInstanceState = true;
        }

        // Select either the default item (0) or the last selected item.
        selectItem(mCurrentSelectedPosition);
    }

    public void menuAdapterNotify(){
        if(sideMenuListAdapter!=null){
            sideMenuListAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        // Indicate that this fragment would like to influence the set of actions in the action bar.
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        ListView mDrawerListView = (ListView) inflater.inflate(R.layout.fragment_navigation_drawer, container, false);
        mDrawerListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selectItem(position);
            }
        });
        mDrawerListView.setAdapter(sideMenuListAdapter);
        mDrawerListView.setItemChecked(mCurrentSelectedPosition, true);
        pref = SharedPref.getInstance(getActivity());
        return mDrawerListView;
    }

    public boolean isDrawerOpen() {
        return mDrawerLayout != null && mDrawerLayout.isDrawerOpen(mFragmentContainerView);
    }


    public void handleMenuButton() {
        if(isDrawerOpen())
        {
            mDrawerLayout.closeDrawers();
        }
        else{
            mDrawerLayout.openDrawer(mFragmentContainerView);
        }

    }

    /**
     * Users of this fragment must call this method to set up the navigation drawer interactions.
     *
     * @param fragmentId   The android:id of this fragment in its activity's layout.
     * @param drawerLayout The DrawerLayout containing this fragment's UI.
     */
    public void setUp(int fragmentId, DrawerLayout drawerLayout) {
        mFragmentContainerView = getActivity().findViewById(fragmentId);
        mDrawerLayout = drawerLayout;

        // set a custom shadow that overlays the main content when the drawer opens
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
        // set up the drawer's list view with items and click listener

        // ActionBar actionBar = getActionBar();
        // actionBar.setDisplayHomeAsUpEnabled(true);
        // actionBar.setHomeButtonEnabled(true);

        // ActionBarDrawerToggle ties together the the proper interactions
        // between the navigation drawer and the action bar app icon.
        mDrawerToggle = new ActionBarDrawerToggle(
                getActivity(),                    /* host Activity */
                mDrawerLayout,                    /* DrawerLayout object */
                R.drawable.ic_drawer,             /* nav drawer image to replace 'Up' caret */
                R.string.navigation_drawer_open,  /* "open drawer" description for accessibility */
                R.string.navigation_drawer_close  /* "close drawer" description for accessibility */
        ) {
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                if (!isAdded()) {
                    return;
                }

                getActivity().supportInvalidateOptionsMenu(); // calls onPrepareOptionsMenu()
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);


                if (!isAdded()) {
                    return;
                }

             if(pref.getDoctorID()!=null && pref.getDoctorID().length()>0){
                 getDoctorProfile();
             }


                if (!mUserLearnedDrawer) {
                    // The user manually opened the drawer; store this flag to prevent auto-showing
                    // the navigation drawer automatically in the future.
                    mUserLearnedDrawer = true;
                    SharedPreferences sp = PreferenceManager
                            .getDefaultSharedPreferences(getActivity());
                    sp.edit().putBoolean(PREF_USER_LEARNED_DRAWER, true).apply();
                }

                getActivity().supportInvalidateOptionsMenu(); // calls onPrepareOptionsMenu()
            }
        };

        // If the user hasn't 'learned' about the drawer, open it to introduce them to the drawer,
        // per the navigation drawer design guidelines.
        if (!mUserLearnedDrawer && !mFromSavedInstanceState) {
            mDrawerLayout.openDrawer(mFragmentContainerView);
        }

        // Defer code dependent on restoration of previous instance state.
        mDrawerLayout.post(new Runnable() {
            @Override
            public void run() {
                mDrawerToggle.syncState();
            }
        });

        mDrawerLayout.setDrawerListener(mDrawerToggle);
    }

    private void getDoctorProfile()
    {
        new GetDoctorProfileAsynctask(getActivity(), new GenericNetworkInterface() {
            @Override
            public void networkResponse(ResponseModel responseModel) {
                if(responseModel!=null && responseModel.getStatus()==1)
                {
                    if(responseModel.getOther1()==1)
                    {
                        if(responseModel.getObject()!=null && responseModel.getObject() instanceof DoctorProfileModel)
                        {
                            DoctorProfileModel doctorProfileModel = (DoctorProfileModel)responseModel.getObject();
                            if (doctorProfileModel.getMorph() != null && doctorProfileModel.getMorph().length() > 0) {
                                sideMenuListAdapter.setTreatmentBoolean(true);
                            }else{
                                sideMenuListAdapter.setTreatmentBoolean(false);
                            }

                            if (doctorProfileModel.getMorph() != null && doctorProfileModel.getMorph().length() > 0) {

                                Bundle bundle = new Bundle();
                                String share = "Hey, Check it out... ";
                                if(doctorProfileModel.getWebsiteurl()!=null && doctorProfileModel.getWebsiteurl().length()>0)
                                    share = share+doctorProfileModel.getWebsiteurl();
                                else if (doctorProfileModel.getFbpageurl()!=null && doctorProfileModel.getFbpageurl().length()>0)
                                    share = share+doctorProfileModel.getFbpageurl();
                                bundle.putString("Text",share);
                                bundle.putString("Morph", doctorProfileModel.getMorph());

                                if(mActivity!=null && mActivity instanceof HomeActivity)
                                ((HomeActivity)mActivity).setSideTreatment(bundle);

                            }else{
                                if(mActivity!=null && mActivity instanceof HomeActivity)
                                ((HomeActivity)getActivity()).setSideTreatment(null);
                            }
                        }
                    }
                }
            }
        },false).execute(PatientConstant.BASE_URL + "patient.json/id/" + pref.getDoctorID());
    }

    private void selectItem(int position) {
        mCurrentSelectedPosition = position;
    /*    if (mDrawerListView != null) {
            mDrawerListView.setItemChecked(position, true);
        }*/
        if (mDrawerLayout != null) {
            mDrawerLayout.closeDrawer(mFragmentContainerView);
        }
        if (mCallbacks != null) {
            mCallbacks.onNavigationDrawerItemSelected(position);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = activity;
        try {
            mCallbacks = (NavigationDrawerCallbacks) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException("Activity must implement NavigationDrawerCallbacks.");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mActivity = null;
        mCallbacks = null;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(STATE_SELECTED_POSITION, mCurrentSelectedPosition);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Forward the new configuration the drawer toggle component.
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    /**
     * Callbacks interface that all activities using this fragment must implement.
     */
    public static interface NavigationDrawerCallbacks {
        /**
         * Called when an item in the navigation drawer is selected.
         */
        void onNavigationDrawerItemSelected(int position);
    }
}
