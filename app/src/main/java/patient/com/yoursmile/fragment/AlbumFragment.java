package patient.com.yoursmile.fragment;

import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import patient.com.yoursmile.R;
import patient.com.yoursmile.activity.HomeActivity;
import patient.com.yoursmile.adapter.AlbumHistoryModelAdapter;
import patient.com.yoursmile.helper.PatientConstant;
import patient.com.yoursmile.helper.SharedPref;
import patient.com.yoursmile.model.AlbumHistoryModel;
import patient.com.yoursmile.model.PagerLimitModel;
import patient.com.yoursmile.model.ResponseModel;
import patient.com.yoursmile.network.GenericNetworkInterface;
import patient.com.yoursmile.network.GetAlbumHistoryAsync;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link AlbumFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link AlbumFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AlbumFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.txtNoItem)
    TextView txtNoItem;
    @BindView(R.id.rvAlbum)
    RecyclerView recyclerView;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private SharedPref pref;
    private ArrayList<AlbumHistoryModel> mList = new ArrayList<>();
    private AlbumHistoryModelAdapter mAdapter;
    private SwipeRefreshLayout mSwipeRefreshLayout;

    public AlbumFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment AlbumFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static AlbumFragment newInstance(String param1, String param2) {
        AlbumFragment fragment = new AlbumFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pref = SharedPref.getInstance(getActivity());
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_album, container, false);
        ButterKnife.bind(this, view);
        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipeRefreshLayout);
        mAdapter = new AlbumHistoryModelAdapter(getActivity(), mList, (position, type) -> {
            if (mList != null && position < mList.size()) {
                int number = 0;
                if (position > 0) {
                    number = position * 3;
                }

                if (type == 1)//left
                {
                    number += 1;
                    ((HomeActivity) getActivity()).openViewPager(number, getImageArrayList());
                } else if (type == 2)//center
                {
                    number += 2;
                    ((HomeActivity) getActivity()).openViewPager(number, getImageArrayList());
                } else if (type == 3) // right//
                {
                    number += 3;
                    ((HomeActivity) getActivity()).openViewPager(number, getImageArrayList());
                }
            }
        });
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(mAdapter);
        getAlbumHistory(true);

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                                                     @Override
                                                     public void onRefresh() {
                                                         getAlbumHistory(false);
                                                     }
                                                 }
        );

        return view;
    }

    private ArrayList<PagerLimitModel> getImageArrayList() {
        ArrayList<PagerLimitModel> list = new ArrayList<>();
        for (AlbumHistoryModel historyModel : mList) {
            list.add(new PagerLimitModel(historyModel.getLeftImage(), historyModel.getCreatedDate()));
            list.add(new PagerLimitModel(historyModel.getCenterImage(), historyModel.getCreatedDate()));
            list.add(new PagerLimitModel(historyModel.getRightImage(), historyModel.getCreatedDate()));
        }
        return list;
    }


    private void getAlbumHistory(boolean value) {
        // http://api.sawbros.com/main/album.json/filter/album-list/patientId/1/doctorId/1/offset/0
        String urlString = PatientConstant.BASE_URL + "album.json/filter/album-list/patientId/" + pref.getPatientID() + "/doctorId/" + pref.getDoctorID() + "/offset/0";
        new GetAlbumHistoryAsync(getActivity(), value, new GenericNetworkInterface() {
            @Override
            public void networkResponse(ResponseModel responseModel) {
                mSwipeRefreshLayout.setRefreshing(false);
                mList.clear();
                if (responseModel != null && responseModel.getStatus() == 1) {
                    mList.clear();
                    List<Object> list = responseModel.getObjectList();
                    if (list != null && list.size() > 0) {
                        txtNoItem.setVisibility(View.INVISIBLE);
                        for (Object object : list) {
                            if (object instanceof AlbumHistoryModel)
                                mList.add((AlbumHistoryModel) object);
                        }
                    } else {
                        txtNoItem.setVisibility(View.VISIBLE);
                    }
                    if (mAdapter != null)
                        mAdapter.notifyDataSetChanged();
                } else {
                    txtNoItem.setVisibility(View.VISIBLE);
                }
            }
        }).execute(urlString);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
