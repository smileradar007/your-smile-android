package patient.com.yoursmile.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import patient.com.yoursmile.helper.TouchImageView;
import patient.com.yoursmile.model.PagerLimitModel;
import patient.com.yoursmile.R;

/**
 * Created by vineet on 17/12/15.
 */
public class MenuFragment  extends Fragment {
    private View mMenuView;
    private int mPosition;
    private ArrayList<PagerLimitModel> mMenuLinksList;
    private ProgressBar mProgressbar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle != null) {
            if (bundle.containsKey("pos"))
                mPosition = getArguments() != null ? getArguments().getInt("pos") : 1;
            if (bundle.containsKey("List"))
                mMenuLinksList = (ArrayList<PagerLimitModel>) bundle.getSerializable("List");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mMenuView = inflater.inflate(R.layout.fragment_view_menu, container, false);
        initializeNormalVariables();
        return mMenuView;
    }

    private void initializeNormalVariables() {
        mProgressbar = (ProgressBar) mMenuView.findViewById(R.id.pb);
        final TouchImageView menuIV = (TouchImageView) mMenuView.findViewById(R.id.iv_menu_image);
        final TextView noImageTV = (TextView) mMenuView.findViewById(R.id.tv_no_image);
        final TextView timeStampTextView = (TextView) mMenuView.findViewById(R.id.tv_timestamp);

        if (mMenuLinksList != null && mMenuLinksList.size() > 0) {
            PagerLimitModel pagerLimitModel = mMenuLinksList.get(mPosition);

            final String imgPath = pagerLimitModel.getImageURl();
            timeStampTextView.setText(pagerLimitModel.getTimeStamp());

            if (imgPath.length() > 0) {
                menuIV.setVisibility(View.VISIBLE);
                noImageTV.setVisibility(View.GONE);

                Picasso.with(getActivity())
                        .load(imgPath)
                        .resizeDimen(R.dimen.adpter_album_history_image_slider, R.dimen.adpter_album_history_image_slider)
                        .into(menuIV, new Callback() {
                            @Override
                            public void onSuccess() {
                                mProgressbar.setVisibility(View.INVISIBLE);
                            }

                            @Override
                            public void onError() {
                                Picasso.with(getActivity())
                                        .load(imgPath)
                                        .resizeDimen(R.dimen.adpter_album_history_image_slider, R.dimen.adpter_album_history_image_slider)
                                        .into(menuIV, new Callback() {
                                            @Override
                                            public void onSuccess() {
                                                mProgressbar.setVisibility(View.INVISIBLE);
                                            }

                                            @Override
                                            public void onError() {
                                                // TODO Auto-generated method stub
                                                mProgressbar.setVisibility(View.INVISIBLE);
                                                noImageTV.setVisibility(View.VISIBLE);
                                            }
                                        });
                            }
                        });
            } else {
                menuIV.setVisibility(View.INVISIBLE);
                noImageTV.setVisibility(View.VISIBLE);
            }
        } else {
            menuIV.setVisibility(View.INVISIBLE);
            noImageTV.setVisibility(View.VISIBLE);
        }
    }
}
