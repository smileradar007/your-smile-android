package patient.com.yoursmile.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import patient.com.yoursmile.adapter.EndlessRecyclerOnScrollListener;
import patient.com.yoursmile.adapter.RecyclerViewAdapter;
import patient.com.yoursmile.helper.PatientConstant;
import patient.com.yoursmile.helper.SharedPref;
import patient.com.yoursmile.model.NotificationModel;
import patient.com.yoursmile.model.ResponseModel;
import patient.com.yoursmile.network.DeleteNotificationAsynctask;
import patient.com.yoursmile.network.GenericNetworkInterface;
import patient.com.yoursmile.network.GetNotificationListAsynctask;
import patient.com.yoursmile.activity.AlbumActivity;
import patient.com.yoursmile.activity.HomeActivity;
import patient.com.yoursmile.R;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link NotificationFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link NotificationFragment#newInstance} factory method to
 * create an instance of this fragment.
 */

public class NotificationFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private RecyclerView recyclerView;
    private boolean isRunning=false;
    private RecyclerView.Adapter mAdapter;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private ArrayList<NotificationModel> mDataSet;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private SharedPref pref;
    @BindView(R.id.txtNoItem)
    TextView txtNoItem;
    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment NotificationFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static NotificationFragment newInstance(String param1, String param2) {
        NotificationFragment fragment = new NotificationFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public NotificationFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pref = SharedPref.getInstance(getActivity());
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_notification, container, false);
        ButterKnife.bind(this, view);
        recyclerView = (RecyclerView) view.findViewById(R.id.rvNotification);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        mSwipeRefreshLayout = (SwipeRefreshLayout)view.findViewById(R.id.swipeRefreshLayout);

        // Adapter:
//        String[] adapterData = new String[]{"Alabama", "Alaska", "Arizona", "Arkansas", "California", "Colorado", "Connecticut", "Delaware", "Florida", "Georgia", "Hawaii", "Idaho", "Illinois", "Indiana", "Iowa", "Kansas", "Kentucky", "Louisiana", "Maine", "Maryland", "Massachusetts", "Michigan", "Minnesota", "Mississippi", "Missouri", "Montana", "Nebraska", "Nevada", "New Hampshire", "New Jersey", "New Mexico", "New York", "North Carolina", "North Dakota", "Ohio", "Oklahoma", "Oregon", "Pennsylvania", "Rhode Island", "South Carolina", "South Dakota", "Tennessee", "Texas", "Utah", "Vermont", "Virginia", "Washington", "West Virginia", "Wisconsin", "Wyoming"};
//        mDataSet = new ArrayList<String>(Arrays.asList(adapterData));
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        mDataSet = new ArrayList<NotificationModel>();
        mAdapter = new RecyclerViewAdapter(getActivity(), mDataSet,notificationInterface);
        recyclerView.setAdapter(mAdapter);

        /* Listeners */
        recyclerView.setOnScrollListener(new EndlessRecyclerOnScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int current_page) {

            }
        });
        getNotificationList(0,true);

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                                                     @Override
                                                     public void onRefresh() {
                                                         getNotificationList(0,false);
                                                     }
                                                 }
        );
        return view;
    }

    private void getNotificationList(int value,boolean show){
        String urlString = PatientConstant.NOTIFICATION+"id/"+pref.getPatientID()+"/offset/0/usertype/"+PatientConstant.USERTYPE;
        new GetNotificationListAsynctask(getActivity(),show, new GenericNetworkInterface() {
            @Override
            public void networkResponse(ResponseModel responseModel) {
                mSwipeRefreshLayout.setRefreshing(false);
                mDataSet.clear();
                if(responseModel!=null && responseModel.getStatus()==1)
                {
                    List<Object> list = responseModel.getObjectList();
                    if(list!=null && list.size()>0){
                        txtNoItem.setVisibility(View.INVISIBLE);
                        for(Object object : list) {
                            NotificationModel notificationModel = (NotificationModel) object;
                            mDataSet.add(notificationModel);
                        }
                    }
                    else{
                        txtNoItem.setVisibility(View.VISIBLE);
                    }
                }
                mAdapter.notifyDataSetChanged();
            }
        }).execute(urlString);
    }

    RecyclerViewAdapter.NotificationInterface notificationInterface = new RecyclerViewAdapter.NotificationInterface() {
        @Override
        public void notificationItemClick(NotificationModel notificationModel) {
            if(notificationModel!=null) {
                String albumType = notificationModel.getType();
                if(albumType.equalsIgnoreCase(PatientConstant.NOTIFICATION_TREATMENT_COMP) || albumType.equalsIgnoreCase(PatientConstant.NOTIFICATION_MORPH_COMP)){
                    Activity activity = getActivity();
                    if(activity instanceof HomeActivity)
                    {
                        ((HomeActivity)activity).onNavigationDrawerItemSelected(1);
                    }
                }
                else if(albumType.equalsIgnoreCase(PatientConstant.NOTIFICATION_ACCEPTED)
                        ||albumType.equalsIgnoreCase(PatientConstant.NOTIFICATION_NEWALBUM)
                        ||albumType.equalsIgnoreCase(PatientConstant.NOTIFICATION_SOS)){
              /*      Intent intent = new Intent(getActivity(), AlbumActivity.class);
                    startActivity(intent);*/
                    Activity activity = getActivity();
                    if(activity instanceof HomeActivity)
                        ((HomeActivity)activity).onNavigationDrawerItemSelected(4);
                }
                else if(albumType.equalsIgnoreCase(PatientConstant.NOTIFICATION_REJECTED)) {
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("ID", notificationModel.getObjectID());
                    Intent intent = new Intent(getActivity(), AlbumActivity.class);
                    intent.putExtras(bundle);
                    startActivity(intent);
                }
            }
        }

        @Override
        public void notificationItemLongClick(final NotificationModel notificationModel) {
            AlertDialog.Builder builder1 = new AlertDialog.Builder(getActivity());
            builder1.setMessage("Are you sure want to delete this notification?");
            builder1.setCancelable(true);
            builder1.setPositiveButton("Yes",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            new DeleteNotificationAsynctask(getActivity(), new GenericNetworkInterface() {
                                @Override
                                public void networkResponse(ResponseModel responseModel) {
                                    if(responseModel!=null && responseModel.getStatus()==1){
                                        if(isAdded()){
                                            if(mDataSet!=null && mDataSet.size()>0){
                                                mDataSet.remove(notificationModel);
                                                mAdapter.notifyDataSetChanged();
                                            }
                                        }
                                    }
                                }
                            }).execute(PatientConstant.NOTIFICATION+"id/"+notificationModel.getNotificationID()+"/usertype/"+PatientConstant.USERTYPE);
                            dialog.cancel();
                        }
                    });
            builder1.setNegativeButton("No",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });

            AlertDialog alert11 = builder1.create();
            alert11.show();

        }

        @Override
        public void gotoBottomScreen() {

        }
    };

    /**
     * Substitute for our onScrollListener for RecyclerView
     */
    RecyclerView.OnScrollListener onScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            // Could hide open views here if you wanted. //
        }
    };

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }
}
