package patient.com.yoursmile.fragment;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.squareup.picasso.Transformation;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import patient.com.yoursmile.R;
import patient.com.yoursmile.activity.HomeActivity;
import patient.com.yoursmile.activity.MrophActivity;
import patient.com.yoursmile.helper.Blur;
import patient.com.yoursmile.helper.PatientConstant;
import patient.com.yoursmile.helper.SharedPref;
import patient.com.yoursmile.model.DoctorProfileModel;
import patient.com.yoursmile.network.GetDoctorProfileAsynctask;
import patient.com.yoursmile.utils.Utils;
import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.OnNeverAskAgain;
import permissions.dispatcher.RuntimePermissions;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link DashboardFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link DashboardFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
@RuntimePermissions
public class DashboardFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.txtEPhone)
    TextView txtEPhone;
    @BindView(R.id.txtMessage)
    TextView txtMessage;
    @BindView(R.id.txtName)
    TextView txtName;
    @BindView(R.id.txtPractice)
    TextView txtPractice;
    @BindView(R.id.txtDegree)
    TextView txtDegree;
    @BindView(R.id.txtAddress)
    TextView txtAddress;
    @BindView(R.id.txtEmail)
    TextView txtEmail;
    @BindView(R.id.txtPhone)
    TextView txtPhone;
    @BindView(R.id.txtWeb)
    TextView txtWeb;
    @BindView(R.id.txtBio)
    TextView txtBio;
    @BindView(R.id.imgProfile)
    ImageView profileImageView;
    @BindView(R.id.imgBanner)
    ImageView bannerImageView;
    @BindView(R.id.layProfile)
    ScrollView layProfile;
    @BindView(R.id.btnSubmit)
    Button btnSubmit;
    @BindView(R.id.imgFb)
    ImageView imgFB;
    @BindView(R.id.imgTw)
    ImageView imgTw;
    @BindView(R.id.imgInsta)
    ImageView imgInsta;
    Target target = new Target() {
        @Override
        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
            if (bitmap != null) {
                try {
                    Bitmap bitmap1 = Blur.fastblur(getActivity(), bitmap, 1);
                    bannerImageView.setImageBitmap(bitmap1);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onBitmapFailed(Drawable errorDrawable) {
        }

        @Override
        public void onPrepareLoad(Drawable placeHolderDrawable) {
        }
    };
    private SharedPref pref;
    private DoctorProfileModel doctorProfileModel;

    public DashboardFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment DashboardFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static DashboardFragment newInstance(String param1, String param2) {
        DashboardFragment fragment = new DashboardFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pref = SharedPref.getInstance(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_dashboard, container, false);
        ButterKnife.bind(this, view);
        getDoctorProfile();
        return view;
    }

    private void getDoctorProfile() {
        new GetDoctorProfileAsynctask(getActivity(), responseModel -> {
            if (responseModel != null && responseModel.getStatus() == 1) {
                if (responseModel.getOther1() == 1) {
                    txtMessage.setVisibility(View.GONE);
                    layProfile.setVisibility(View.VISIBLE);
                    if (responseModel.getObject() != null && responseModel.getObject() instanceof DoctorProfileModel) {
                        doctorProfileModel = (DoctorProfileModel) responseModel.getObject();
                        if (doctorProfileModel.getProfileimage() != null && doctorProfileModel.getProfileimage().length() > 0) {
                            Picasso.with(getActivity())
                                    .load(doctorProfileModel.getProfileimage()).fit()
                                    .placeholder(R.drawable.profileplaceholder)
                                    .transform(new CircleTransform()).into(profileImageView);
                            Picasso.with(getActivity()).load(doctorProfileModel.getProfileimage()).into(target);
                        }
                        txtName.setText(doctorProfileModel.getName());
                        txtEPhone.setText(doctorProfileModel.getEmmergency_telephone());
//                            txtQualification.setText(doctorProfileModel.getPractice_name());
                        txtPractice.setText(doctorProfileModel.getPractice_name());
//                        txtDegree.setText(doctorProfileModel.getQualification());
                        txtAddress.setText(doctorProfileModel.getAddress());
                        txtEmail.setText(doctorProfileModel.getEmail());
                        txtPhone.setText(doctorProfileModel.getPractice_telephone());
//                        txtWeb.setText(doctorProfileModel.getWebsiteurl());
                        txtWeb.setText("www.yoursmiledirect.com");
                        txtBio.setText(doctorProfileModel.getDescription());
                        if (doctorProfileModel.getMorph() != null && doctorProfileModel.getMorph().length() > 0) {
                            btnSubmit.setVisibility(View.VISIBLE);

                            Bundle bundle = new Bundle();
                            String share = "Hey, Check it out... ";
                            if (doctorProfileModel.getWebsiteurl() != null && doctorProfileModel.getWebsiteurl().length() > 0)
                                share = share + doctorProfileModel.getWebsiteurl();
                            else if (doctorProfileModel.getFbpageurl() != null && doctorProfileModel.getFbpageurl().length() > 0)
                                share = share + doctorProfileModel.getFbpageurl();
                            bundle.putString("Text", share);
                            bundle.putString("Morph", doctorProfileModel.getMorph());

                            ((HomeActivity) getActivity()).setSideTreatment(bundle);

                        } else {
                            btnSubmit.setVisibility(View.GONE);
                            ((HomeActivity) getActivity()).setSideTreatment(null);
                        }
                    }
                } else {
                    txtMessage.setVisibility(View.VISIBLE);
                    layProfile.setVisibility(View.GONE);
                    txtMessage.setText(responseModel.getMessage());
                }
            }
        }, true).execute(PatientConstant.BASE_URL + "patient.json/id/" + pref.getDoctorID());
    }

    @OnClick(R.id.imgFb)
    public void openFB() {
//        if (doctorProfileModel != null && doctorProfileModel.getFbpageurl().length() > 0) {

//            String facebookUrl = "https://www.facebook.com/" + doctorProfileModel.getFbpageurl();
        String facebookUrl = "https://www.facebook.com/yoursmiledirect/";
        try {
            int versionCode = getActivity().getPackageManager().getPackageInfo("com.facebook.katana", 0).versionCode;
            if (versionCode >= 3002850) {
                Uri uri = Uri.parse("fb://facewebmodal/f?href=" + facebookUrl);
                startActivity(new Intent(Intent.ACTION_VIEW, uri));
                ;
            } else {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(facebookUrl)));
            }
        } catch (PackageManager.NameNotFoundException e) {
            // Facebook is not installed. Open the browser
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(facebookUrl)));
        }
//        }
    }

    @OnClick(R.id.imgTw)
    public void openTw() {
//        if (doctorProfileModel != null && doctorProfileModel.getTwhandle().length() > 0) {

        try {
//                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("twitter://user?screen_name=" + doctorProfileModel.getTwhandle())));
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://twitter.com/yoursmiledirect?lang=en")));
        } catch (Exception e) {
//                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://twitter.com/#!/" + doctorProfileModel.getTwhandle())));
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://twitter.com/yoursmiledirect?lang=en")));
        }
//        }
    }

    @OnClick(R.id.imgInsta)
    public void openInsta() {
        String url = "https://instagram.com/yoursmiledirect/";
        try {
            Uri uri = Uri.parse(url);
//            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
            Intent intent = getActivity().getPackageManager().getLaunchIntentForPackage("com.instagram.android");
            intent.setPackage("com.instagram.android");
            intent.setComponent(new ComponentName("com.instagram.android", "com.instagram.android.activity.UrlHandlerActivity"));
            intent.setData(uri);
            startActivity(intent);

        } catch (ActivityNotFoundException e) {
            e.printStackTrace();
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
//            Toast.makeText(getContext(), "Something went wrong, please try again.", Toast.LENGTH_SHORT).show();
        }

    }

    @OnClick(R.id.txtAddress)
    public void openAddress() {
        if (doctorProfileModel != null && doctorProfileModel.getAddress().length() > 0) {
            Double latitude = 0.0, longitude = 0.0;
            try {
                latitude = Double.parseDouble(doctorProfileModel.getLatitude());
                longitude = Double.parseDouble(doctorProfileModel.getLongitude());
            } catch (Exception e) {
            }

            String label = doctorProfileModel.getAddress();
            String uriBegin = "geo:" + latitude + "," + longitude;
            String query = latitude + "," + longitude + "(" + label + ")";
            String encodedQuery = Uri.encode(query);
            String uriString = uriBegin + "?q=" + encodedQuery + "&z=16";
            Uri uri = Uri.parse(uriString);
            Intent intent = new Intent(android.content.Intent.ACTION_VIEW, uri);
            startActivity(intent);
        }
    }

    @OnClick(R.id.imgProfile)
    public void clickOnProfile() {
        if (isAdded() && doctorProfileModel != null) {
            Activity activity = getActivity();
            if (activity instanceof HomeActivity) {
                ((HomeActivity) activity).showImage(doctorProfileModel.getProfileimage());
            }
        }
    }

    @OnClick(R.id.txtEmail)
    public void openMailComposer() {
        if (doctorProfileModel != null) {
            if (doctorProfileModel.getEmail() != null && doctorProfileModel.getEmail().length() > 0) {
                Intent intent = new Intent(Intent.ACTION_SEND);
                String[] recipients = {doctorProfileModel.getEmail()};
                intent.putExtra(Intent.EXTRA_EMAIL, recipients);
                intent.putExtra(Intent.EXTRA_SUBJECT, "Rx Tracker");
                intent.putExtra(Intent.EXTRA_TEXT, "Patient Problem");
                intent.setType("text/html");
                startActivity(intent);
            }
        }
    }

    @OnClick(R.id.txtEPhone)
    public void onEmergencyCallClicked() {
        DashboardFragmentPermissionsDispatcher.callEmergencyNumberWithCheck(this);
    }

    @NeedsPermission(Manifest.permission.CALL_PHONE)
    public void callEmergencyNumber() {
        if (doctorProfileModel != null) {
            if (doctorProfileModel.getEmmergency_telephone() != null && doctorProfileModel.getEmmergency_telephone().length() > 0) {
                Intent intent = new Intent(Intent.ACTION_CALL);
                intent.setData(Uri.parse("tel:" + doctorProfileModel.getEmmergency_telephone()));
                if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                getActivity().startActivity(intent);
            }
        }
    }

    @OnClick(R.id.txtPhone)
    public void onPhoneClicked() {
        DashboardFragmentPermissionsDispatcher.callPracticeNumberWithCheck(this);
    }

    @NeedsPermission(Manifest.permission.CALL_PHONE)
    public void callPracticeNumber() {
        if (doctorProfileModel != null) {
            if (doctorProfileModel.getPractice_telephone() != null && doctorProfileModel.getPractice_telephone().length() > 0) {
                Intent intent = new Intent(Intent.ACTION_CALL);
                intent.setData(Uri.parse("tel:" + doctorProfileModel.getPractice_telephone()));
                if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                getActivity().startActivity(intent);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        DashboardFragmentPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults);
    }

    @OnNeverAskAgain(Manifest.permission.CALL_PHONE)
    void onCallNeverAskAgainTapped() {
        openAppSettingsScreen();
    }

    private void openAppSettingsScreen() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("Please allow Smile Radar access to make calls.").setPositiveButton("Settings", (dialogInterface, i) -> Utils.goToSettings(getContext())).setNegativeButton("Not Now", (dialogInterface, i) -> dialogInterface.cancel()).show();
    }

    @OnClick(R.id.btnSubmit)
    public void callMorph() {
        if (doctorProfileModel != null) {
            if (doctorProfileModel.getMorph() != null && doctorProfileModel.getMorph().length() > 0) {
                Intent intent = new Intent(getActivity(), MrophActivity.class);
                Bundle bundle = new Bundle();
                String share = "Hey, Check it out... ";
                if (doctorProfileModel.getWebsiteurl() != null && doctorProfileModel.getWebsiteurl().length() > 0)
                    share = share + doctorProfileModel.getWebsiteurl();
                else if (doctorProfileModel.getFbpageurl() != null && doctorProfileModel.getFbpageurl().length() > 0)
                    share = share + doctorProfileModel.getFbpageurl();
                bundle.putString("Text", share);
                bundle.putString("Morph", doctorProfileModel.getMorph());
                intent.putExtras(bundle);
                getActivity().startActivity(intent);
            }
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

    class CircleTransform implements Transformation {
        @Override
        public Bitmap transform(Bitmap source) {
            int size = Math.min(source.getWidth(), source.getHeight());

            int x = (source.getWidth() - size) / 2;
            int y = (source.getHeight() - size) / 2;

            Bitmap squaredBitmap = Bitmap.createBitmap(source, x, y, size, size);
            if (squaredBitmap != source) {
                source.recycle();
            }
            Bitmap bitmap = Bitmap.createBitmap(size, size, source.getConfig());
            Canvas canvas = new Canvas(bitmap);
            Paint paint = new Paint();
            BitmapShader shader = new BitmapShader(squaredBitmap, BitmapShader.TileMode.CLAMP, BitmapShader.TileMode.CLAMP);
            paint.setShader(shader);
            paint.setAntiAlias(true);
            float r = size / 2f;
            canvas.drawCircle(r, r, r, paint);
            squaredBitmap.recycle();
            return bitmap;
        }

        @Override
        public String key() {
            return "circle";
        }
    }
}
