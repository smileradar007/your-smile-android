package patient.com.yoursmile.fragment;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import patient.com.yoursmile.UIHelper.CustomDateTimePicker;
import patient.com.yoursmile.activity.HomeActivity;
import patient.com.yoursmile.activity.PatientApplication;
import patient.com.yoursmile.R;
import patient.com.yoursmile.helper.PatientConstant;
import patient.com.yoursmile.helper.PatientHelper;
import patient.com.yoursmile.helper.SharedPref;
import patient.com.yoursmile.model.PhotoRequestModel;
import patient.com.yoursmile.network.GetPhotoReqAsync;
import patient.com.yoursmile.utils.CustomTaskService;
import patient.com.yoursmile.utils.GcmNetworkManagerUtil;
import patient.com.yoursmile.utils.Utils;
import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.OnNeverAskAgain;
import permissions.dispatcher.RuntimePermissions;

import static android.app.Activity.RESULT_OK;


@RuntimePermissions
public class PhotoRequestFragment extends Fragment implements CustomDateTimePicker.ICustomDateTimeListener {
    @BindView(R.id.btnSubmit)
    Button btnPhoto;
    @BindView(R.id.imgBanner)
    ImageView imgBanner;
    @BindView(R.id.tvClickHere)
    TextView tvClickHere;
    @BindView(R.id.txt1)
    TextView txtDaysRemain;
    @BindView(R.id.txt2)
    TextView txtDaysToGo;
    @BindView(R.id.txt3)
    TextView txtTime;
    @BindView(R.id.btnReminder)
    Button btnReminder;
    // TODO: Rename parameter arguments, choose names that match
    private SharedPref pref;
    private int mDays = 0;
    private Animation animation;
    private Calendar selectedCalendar = Calendar.getInstance();
    private Tracker mTracker;
    private String mUserName, mScreenName = "Photo request home screen";


    public PhotoRequestFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment PhotoRequestFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static PhotoRequestFragment newInstance(String param1, String param2) {
        PhotoRequestFragment fragment = new PhotoRequestFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pref = SharedPref.getInstance(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_photo_request, container, false);
        ButterKnife.bind(this, view);
        getPhotoRequest();
        disableBottomButton(true, 0);
        initializeAnalytics();
        animation = new ScaleAnimation(.9f, .8f, .9f, .8f
                , Animation.RELATIVE_TO_SELF, .5f, Animation.RELATIVE_TO_SELF, .5f);
        animation.setFillAfter(true);
        animation.setDuration(1000);
        animation.setRepeatMode(Animation.REVERSE);
        animation.setRepeatCount(Animation.INFINITE);
//        pref.setIsFirstTimeClickingPhoto(true);
        return view;
    }

    private void initializeAnalytics() {
        mUserName = pref.getPatientID();
        // Obtain the shared Tracker instance.
        PatientApplication application = (PatientApplication) getActivity().getApplication();
        mTracker = application.getDefaultTracker();
        Log.i("TakePhotoActivity", "Setting screen name: " + mScreenName + ",Id:" + mUserName);
        mTracker.setScreenName("User:" + mUserName + ", Screen Name:" + mScreenName);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    private void showCustomDialog() {
        CustomDateTimePicker custom = new CustomDateTimePicker(getActivity(), this);
        custom.set24HourFormat(false);
        custom.setDate(selectedCalendar);
        custom.showDialog();
    }

    private void getPhotoRequest() {
        String urlString = PatientConstant.BASE_URL+"patient.json/filter/" + pref.getDoctorID();
        Log.e("GetPhotoRequest Url", urlString);
        new GetPhotoReqAsync(getActivity(), responseModel -> {
            if (responseModel != null && responseModel.getStatus() == 1) {
                if (responseModel.getObject() != null && responseModel.getObject() instanceof PhotoRequestModel) {
                    disableBottomButton(false, 0);
                    PhotoRequestModel photoRequestModel = (PhotoRequestModel) responseModel.getObject();
                    mDays = photoRequestModel.getCounter();
                    Log.e("SOS in response", "" + photoRequestModel.getSOS());
                    txtDaysRemain.setText(Html.fromHtml("<b>" + mDays + " Days</b>"));

                    long time = pref.getReminderTime();
                    if (time > 0)
                        setReminderText(new Date(time));

                    if (photoRequestModel.getTreetment() == 0) {
                        if (photoRequestModel.getSOS() == 1 || photoRequestModel.getAlbumRequired() == 1) {
                            disableBottomButton(false, photoRequestModel.getSOS());
                        } else {
                            disableBottomButton(true, photoRequestModel.getSOS());
                        }
                    } else {
                        disableBottomButton(true, photoRequestModel.getSOS());
                    }
                }
            }
        }).execute(urlString);
    }

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onSet(Dialog dialog, Calendar calendarSelected, Date dateSelected, int year, String monthFullName, String monthShortName, int monthNumber, int date, String weekDayFullName, String weekDayShortName, int hour24, int hour12, int min, int sec, String AM_PM) {
        Calendar calendar = Calendar.getInstance();

        if (calendarSelected.getTimeInMillis() < calendar.getTimeInMillis()) {
            Toast.makeText(getActivity(), "Please select future date", Toast.LENGTH_SHORT).show();
        } else {
            PatientHelper.pushAppointmentsToCalender(getActivity(), getString(R.string.app_name), "Photo Request Reminder", "", 0, calendarSelected.getTimeInMillis(), true, false);
            pref.setReminderTime(calendarSelected.getTimeInMillis());
            setReminderText(dateSelected);
            int seconds = Utils.getSecondsDifference(Calendar.getInstance().getTimeInMillis(), dateSelected.getTime());

            GcmNetworkManagerUtil.scheduleOneTimeTask(getContext(), CustomTaskService.TAG_TASK_REMINDER_NOTIFCATION, seconds - 300, seconds);

            dialog.dismiss();
            selectedCalendar.setTime(calendarSelected.getTime());
        }
    }

    private void setReminderText(Date dateSelected) {
        SimpleDateFormat format = (SimpleDateFormat) DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.SHORT);
        String setDate = format.format(dateSelected);
        if (setDate != null) {
            txtTime.setText(setDate);
        }
    }

    @Override
    public void onCancel() {

    }

    @OnClick(R.id.btnReminder)
    public void setReminder() {
        PhotoRequestFragmentPermissionsDispatcher.showCalendarWithCheck(this);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        PhotoRequestFragmentPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults);
    }

    @NeedsPermission(Manifest.permission.WRITE_CALENDAR)
    public void showCalendar() {
        // Show Date dialog
        showCustomDialog();
    }

    @OnNeverAskAgain(Manifest.permission.WRITE_CALENDAR)
    void onCalendarNeverAskAgain() {
        openAppSettingsScreen();
    }

    @OnClick(R.id.imgBanner)
    public void onTakePhoto() {
        onClickOnPhoto();
    }

    private void openAppSettingsScreen() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("To set up a reminder, allow Smile Radar to access calendar.").setPositiveButton("Settings", (dialogInterface, i) -> Utils.goToSettings(getContext())).setNegativeButton("Not Now", (dialogInterface, i) -> dialogInterface.cancel()).show();
    }

    @OnClick(R.id.btnSubmit)
    public void onClickOnPhoto() {
        Activity activity = getActivity();
        if (activity != null && activity instanceof HomeActivity) {
            ((HomeActivity) activity).callTakePhoto();
        }
        tvClickHere.setVisibility(View.GONE);
        pref.setIsFirstTimeClickingPhoto(false);
    }

    private void disableBottomButton(boolean value, int sos) {
        if (sos == 1) {
            txtDaysRemain.setVisibility(View.GONE);
            txtDaysToGo.setVisibility(View.GONE);
        } else {
            txtDaysRemain.setVisibility(View.VISIBLE);
            txtDaysToGo.setVisibility(View.VISIBLE);
        }
        if (value) {
            tvClickHere.setVisibility(View.GONE);
            playButtonAnimation(false);
            imgBanner.setEnabled(false);
            btnPhoto.setEnabled(false);
            btnPhoto.setAlpha(.3f);
            btnReminder.setEnabled(false);
            btnReminder.setAlpha(.3f);
            pref.setMenuRed(false);
        } else {
            if (pref.isFirstTimeClickingPhoto())
                tvClickHere.setVisibility(View.VISIBLE);
            playButtonAnimation(true);
            pref.setMenuRed(true);
            imgBanner.setEnabled(true);
            btnPhoto.setAlpha(1.0f);
            btnPhoto.setEnabled(true);
            btnReminder.setAlpha(1.0f);
            btnReminder.setEnabled(true);
        }

        Activity activity = getActivity();
        if (activity != null && activity instanceof HomeActivity) {
            ((HomeActivity) activity).rearrangeMenu();
        }
    }

    private void playButtonAnimation(boolean playAnimation) {
        if (playAnimation) {
            Log.e("imgBanner", "starting animation");
            imgBanner.startAnimation(animation);
        } else {
            Log.e("imgBanner", "stop animation");
            if (animation != null)
                imgBanner.getAnimation().cancel();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == 1001) {
                disableBottomButton(true, 0);
                pref.setTimeStamp("");
                txtTime.setText(Html.fromHtml("<b>_:_ AM</b>"));

            }
        }
    }
}
