package patient.com.yoursmile.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import patient.com.yoursmile.R;

/**
 * Created by ankur on 24/2/16.
 */

public class CartoonFragment extends Fragment {

    @BindView(R.id.layBanner)
    RelativeLayout layBanner;
    @BindView(R.id.btnDone)
    Button btnDone;
    private View view;
    private int mPosition;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_cartoon, container, false);
        ButterKnife.bind(this, view);
        switch (mPosition) {
            case 0:
                layBanner.setBackgroundResource(R.drawable.side);
                btnDone.setVisibility(View.GONE);
                break;
            case 1:
                layBanner.setBackgroundResource(R.drawable.theefourth);
                btnDone.setVisibility(View.GONE);
                break;
            case 2:
                layBanner.setBackgroundResource(R.drawable.front);
                btnDone.setVisibility(View.GONE);
                break;
            case 3:
                layBanner.setBackgroundResource(R.drawable.degree);
                btnDone.setVisibility(View.VISIBLE);
                break;
        }

        btnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Activity activity = getActivity();
                if (activity != null) {
                    activity.finish();
                }
            }
        });
        return view;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle != null && bundle.containsKey("Position")) {
            mPosition = bundle.getInt("Position");
        }
    }
}
