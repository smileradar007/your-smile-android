package patient.com.yoursmile.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.view.Menu;
import android.view.MenuItem;

import patient.com.yoursmile.R;
import patient.com.yoursmile.helper.SharedPref;

public class SplashActivity extends Activity {

    private SharedPref sharedPref;
    private android.os.Handler handler = handler = new android.os.Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (sharedPref.isFirstTime()) {
//                sharedPref.setFirstTime(false);
//                Intent intent = new Intent(SplashActivity.this, TutorialActivity.class);
                Intent intent = new Intent(SplashActivity.this, VideoTutorialActivity.class);
                startActivity(intent);
                finish();
            } else if (sharedPref.isLoggedIn()) {
                if (sharedPref.getDoctorID().length() > 0) {
                    Intent intent = new Intent(SplashActivity.this, HomeActivity.class);
                    startActivity(intent);
                    finish();
                } else {
                    Intent intent = new Intent(SplashActivity.this, ScanQRCodeActivity.class);
                    startActivity(intent);
                    finish();
                }
            } else {
                Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

//        printSHA();

        sharedPref = SharedPref.getInstance(SplashActivity.this);

        new Thread() {
            public void run() {
                try {
                    // Do some work here
                    sleep(2000);
                } catch (Exception e) {
                }
                handler.sendEmptyMessage(0);
            }
        }.start();
    }

    /* private void printSHA() {
         PackageInfo info;
         try {

             info = getPackageManager().getPackageInfo(
                     "patient.com.yoursmile.patient", PackageManager.GET_SIGNATURES);

             for (Signature signature : info.signatures) {
                 MessageDigest md;
                 md = MessageDigest.getInstance("SHA");
                 md.update(signature.toByteArray());
                 String something = new String(Base64.encode(md.digest(), 0));
                 Log.e("Hash key", something);
                 System.out.println("Hash key" + something);
             }

         } catch (PackageManager.NameNotFoundException e1) {
             Log.e("name not found", e1.toString());
         } catch (NoSuchAlgorithmException e) {
             Log.e("no such an algorithm", e.toString());
         } catch (Exception e) {
             Log.e("exception", e.toString());
         }

     }*/
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_splash, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
