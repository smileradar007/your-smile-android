package patient.com.yoursmile.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import patient.com.yoursmile.R;
import patient.com.yoursmile.helper.Logger;
import patient.com.yoursmile.helper.PatientConstant;
import patient.com.yoursmile.helper.PatientHelper;
import patient.com.yoursmile.helper.SharedPref;
import patient.com.yoursmile.model.LoginModel;
import patient.com.yoursmile.model.ResponseModel;
import patient.com.yoursmile.network.ForgotPasswordAsync;
import patient.com.yoursmile.network.GenericNetworkInterface;
import patient.com.yoursmile.network.LoginNetworkInterface;
import patient.com.yoursmile.network.SmileLoginAsyn;

public class Smile_login_Activity extends Activity {

    @BindView(R.id.imgBack)
    ImageView imgBack;

    @BindView(R.id.edEmail)
    EditText edMail;

    @BindView(R.id.edPassword)
    EditText edPassword;

    @BindView(R.id.txtSignup)
    TextView txtSignup;

    @BindView(R.id.txtChangePass)
    TextView txtForgotPassword;

    @BindView(R.id.btnSubmit)
    Button btnSubmit;


    private SharedPref sharedPref;
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 101;
    private static final String PROPERTY_REG_ID = "REG_ID";
    private static final String PROPERTY_APP_VERSION = "App Version";
    private static final String TAG = Smile_login_Activity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_smile_login_);
        ButterKnife.bind(this);
        sharedPref = SharedPref.getInstance(Smile_login_Activity.this);
        setupForPush();
        setupForPush();
        setupForPush();
    }

    @OnClick(R.id.imgBack)
    public void clickOnBack(){
        finish();
    }
    @OnClick(R.id.txtSignup)
    public void clickOnCreateAccount(){
        Intent intent = new Intent(Smile_login_Activity.this,SignUpActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivityForResult(intent, 888);
    }

    @OnClick(R.id.txtChangePass)
    public void clickOnForgotPass(){
        String mailId = edMail.getText().toString().trim();

        if(mailId.length()==0) {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(Smile_login_Activity.this);
            alertDialog.setMessage("Please enter your email and click on Forgot Password. You will receive password on your email id.");
            alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                }
            });
            alertDialog.show();
          /*  Toast.makeText(Smile_login_Activity.this, "Please enter email ID", Toast.LENGTH_SHORT).show();*/
        }
        else if (!PatientHelper.getInstance().emailValidate(mailId)){
            Toast.makeText(Smile_login_Activity.this,"Please enter valid email address",Toast.LENGTH_SHORT).show();
        }else {
            String url = PatientConstant.BASE_URL+"password.json/email/"+mailId+"/appType/1";
            new ForgotPasswordAsync(Smile_login_Activity.this, new GenericNetworkInterface() {
                @Override
                public void networkResponse(ResponseModel responseModel) {
                    if(responseModel!=null){
                        String message = responseModel.getMessage();
                        if(message!=null){
                            AlertDialog.Builder alertDialog = new AlertDialog.Builder(Smile_login_Activity.this);
                            alertDialog.setMessage(message);
                            alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            });
                            alertDialog.show();
                        }
//                            Toast.makeText(Smile_login_Activity.this,message,Toast.LENGTH_SHORT).show();
                    }
                }
            }).execute(url);
        }
    }

    @OnClick(R.id.btnSubmit)
    public void onClickOnSubmit(){
        String mailId = edMail.getText().toString().trim();
        String passString = edPassword.getText().toString().trim();

        if(mailId.length()==0) {
            Toast.makeText(Smile_login_Activity.this, "Please enter email ID", Toast.LENGTH_SHORT).show();
        }
        else if (!PatientHelper.getInstance().emailValidate(mailId)){
            Toast.makeText(Smile_login_Activity.this,"Please enter valid email address",Toast.LENGTH_SHORT).show();
        }else if(passString.length()==0){
            Toast.makeText(Smile_login_Activity.this, "Please enter password", Toast.LENGTH_SHORT).show();
        } else if (passString.length()<8){
            edPassword.setText("");
            Toast.makeText(Smile_login_Activity.this,"Please enter password at least 8 characters",Toast.LENGTH_SHORT).show();
        }else{
            // Application code
            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("deviceToken",regid);
                jsonObject.put("email",mailId);
                jsonObject.put("password",passString);
                jsonObject.put("loginOrSignup", "1");
                jsonObject.put("signupType","1");
                jsonObject.put("socialLoginMedium", "3");
                jsonObject.put("platform", PatientConstant.PLATFORM);


                new SmileLoginAsyn(Smile_login_Activity.this, networkInterface, jsonObject).execute();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private LoginNetworkInterface networkInterface = new LoginNetworkInterface(){
        @Override
        public void facebookLoginResponse(ResponseModel response) {
        }

        @Override
        public void SmileLoginResponse(ResponseModel response) {

            if(response!=null && response.getStatus()==1)
            {
                if(response.getObject()!=null && response.getObject() instanceof LoginModel) {
                    LoginModel loginModel = (LoginModel) response.getObject();
//                    Logger.error("Patient Id", loginModel.getId() + "we");
                    sharedPref.setLoggedIn(true);
                    sharedPref.setLoginType(PatientConstant.S_LOGIN);
                    sharedPref.setPatientID(loginModel.getId());
                    sharedPref.setAccessToken(loginModel.getAccessToken());
                    String doctorID = loginModel.getDoctorId();
                    String doctorName = loginModel.getDoctorname();

                    if (doctorID != null && !doctorID.equalsIgnoreCase("null"))
                        sharedPref.setDoctorID(doctorID);

                    if (doctorName != null && !doctorName.equalsIgnoreCase("null"))
                        sharedPref.setDoctorName(doctorName);

                    setResult(RESULT_OK);

                    Intent send = new Intent(Smile_login_Activity.this, ScanQRCodeActivity.class);
                    startActivity(send);
                    finish();
                }
            }
            else if(response!=null && response.getStatus()==2)
            {
                if(response.getObject()!=null && response.getObject() instanceof LoginModel) {
                    LoginModel loginModel = (LoginModel) response.getObject();
                    Logger.error("Patient Id", loginModel.getId() + "we");
                    sharedPref.setLoggedIn(true);
                    sharedPref.setLoginType(PatientConstant.S_LOGIN);
                    sharedPref.setPatientID(loginModel.getId());
                    sharedPref.setAccessToken(loginModel.getAccessToken());
                    String doctorID = loginModel.getDoctorId();
                    String doctorName = loginModel.getDoctorname();

                    if (doctorID != null && !doctorID.equalsIgnoreCase("null"))
                        sharedPref.setDoctorID(doctorID);

                    if (doctorName != null && !doctorName.equalsIgnoreCase("null"))
                        sharedPref.setDoctorName(doctorName);

                    setResult(RESULT_OK);

                    Intent send = new Intent(Smile_login_Activity.this, ScanQRCodeActivity.class);
                    startActivity(send);
                    finish();
                }
            }
            else if(response!=null && response.getMessage()!=null)
            {
                PatientHelper.getInstance().showExitToast(Smile_login_Activity.this,response.getMessage());
            }
            else{
                PatientHelper.getInstance().showExitToast(Smile_login_Activity.this,"Network Error!");
            }

        }

        @Override
        public void gPlusLoginResponse(ResponseModel response) {

        }
    };



    private GoogleCloudMessaging gcm;
    private String regid=null;
    public void setupForPush() {
        if (checkPlayServices()) {
            gcm = GoogleCloudMessaging.getInstance(this);
            regid = getRegistrationId(Smile_login_Activity.this);
            if (regid.isEmpty())
            {
                registerInBackground();
            }
        } else {
            Log.i(TAG, "No valid Google Play Services APK found.");
        }
    }
    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Log.i(TAG, "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }


    /**
     * Gets the current registration ID for application on GCM service.
     * <p>
     * If result is empty, the app needs to register.
     *
     * @return registration ID, or empty string if there is no existing
     *         registration ID.
     */
    private String getRegistrationId(Context context) {
        final SharedPreferences prefs = getGCMPreferences();
        String registrationId = prefs.getString(PROPERTY_REG_ID, "");
        if (registrationId.isEmpty()) {
            Log.i(TAG, "Registration not found.");
            return "";
        }
        // Check if app was updated; if so, it must clear the registration ID
        // since the existing regID is not guaranteed to work with the new
        // app version.
        int registeredVersion = prefs.getInt(PROPERTY_APP_VERSION, Integer.MIN_VALUE);
        int currentVersion = getAppVersion(context);
        if (registeredVersion != currentVersion) {
            Log.i(TAG, "App version changed.");
            return "";
        }
        return registrationId;
    }


    /**
     * @return Application's {@code SharedPreferences}.
     */
    private SharedPreferences getGCMPreferences() {
        // This sample app persists the registration ID in shared preferences, but
        // how you store the regID in your app is up to you.
        return getSharedPreferences("GCM_PREF",
                Context.MODE_PRIVATE);
    }

    private void clearGCMPref() {
        SharedPreferences.Editor editor = getGCMPreferences().edit();
        editor.clear();
        editor.commit();
    }

    /**
     * @return Application's version code from the {@code PackageManager}.
     */
    private static int getAppVersion(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            // should never happen
            throw new RuntimeException("Could not get package name: " + e);
        }
    }

    /**
     * Registers the application with GCM servers asynchronously.
     * <p>
     * Stores the registration ID and app versionCode in the application's
     * shared preferences.
     */
    private void registerInBackground() {
        new GCMUpdateAsync().execute();
    }

    class GCMUpdateAsync extends AsyncTask<Void, Void, String> {
        @Override
        protected String doInBackground(Void... params) {
            String msg = "";
            try {
                if (gcm == null)
                {
                    gcm = GoogleCloudMessaging.getInstance(Smile_login_Activity.this);
                }
                regid = gcm.register(PatientConstant.SENDER_ID);
                msg = "Device registered, registration ID=" + regid;

                // You should send the registration ID to your server over HTTP,
                // so it can use GCM/HTTP or CCS to send messages to your app.
                // The request to your server should be authenticated if your app
                // is using accounts.
                sendRegistrationIdToBackend();

                // For this demo: we don't need to send it because the device
                // will send upstream messages to a server that echo back the
                // message using the 'from' address in the message.

                // Persist the regID - no need to register again.
                storeRegistrationId(Smile_login_Activity.this, regid);
            } catch (IOException ex) {
                msg = "Error :" + ex.getMessage();
                // If there is an error, don't just keep trying to register.
                // Require the user to click a button again, or perform
                // exponential back-off.
            }
            return msg;
        }
        @Override
        protected void onPostExecute(String msg) {
            //	 mDisplay.append(msg + "\n");
            //				 sendPush();
        }
    }

    /**
     * Sends the registration ID to your server over HTTP, so it can use GCM/HTTP
     * or CCS to send messages to your app. Not needed for this demo since the
     * device sends upstream messages to a server that echoes back the message
     * using the 'from' address in the message.
     */
    private void sendRegistrationIdToBackend() {

    }

    /**
     * Stores the registration ID and app versionCode in the application's
     * {@code SharedPreferences}.
     *
     * @param context application's context.
     * @param regId registration ID
     */
    private void storeRegistrationId(Context context, String regId) {
        final SharedPreferences prefs = getGCMPreferences();
        int appVersion = getAppVersion(context);
        Log.i(TAG, "Saving regId on app version " + appVersion);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(PROPERTY_REG_ID, regId);
        editor.commit();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==888){
            if (resultCode == RESULT_OK) {
                setResult(RESULT_OK);
                finish();
            }
        }
    }
}
