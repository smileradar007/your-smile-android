package patient.com.yoursmile.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import patient.com.yoursmile.R;
import patient.com.yoursmile.helper.Blur;
import patient.com.yoursmile.helper.CircleTransform;
import patient.com.yoursmile.helper.PatientConstant;
import patient.com.yoursmile.helper.PatientHelper;
import patient.com.yoursmile.helper.SharedPref;
import patient.com.yoursmile.model.PatientModel;
import patient.com.yoursmile.model.ResponseModel;
import patient.com.yoursmile.network.GenericNetworkInterface;
import patient.com.yoursmile.network.SetEditProfileAsyncTask;
import patient.com.yoursmile.utils.FileUtils;
import patient.com.yoursmile.utils.Utils;
import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.OnNeverAskAgain;
import permissions.dispatcher.RuntimePermissions;

@RuntimePermissions
public class MyProfileActivity extends Activity {

    @BindView(R.id.edDescription)
    EditText edDescription;
    @BindView(R.id.edContact)
    EditText edContact;
    @BindView(R.id.edEmail)
    EditText edEmail;
    @BindView(R.id.edAddress)
    EditText edAddress;
    @BindView(R.id.edName)
    EditText edName;
    @BindView(R.id.imgBanner)
    ImageView imgBanner;
    @BindView(R.id.imgProfile)
    ImageView imgProfile;
    @BindView(R.id.imgEdit)
    ImageView imgEdit;
    @BindView(R.id.btnSubmit)
    Button btnSubmit;
    @BindView(R.id.imgBack)
    ImageView imgBack;

    private String imagePath = "";
    private SharedPref pre;

    private static int REQUEST_CAMERA = 101, SELECT_FILE = 102;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_profile);
        ButterKnife.bind(this);
        pre = SharedPref.getInstance(MyProfileActivity.this);

        imgBanner.setVisibility(View.INVISIBLE);

        PatientModel patientModel = (PatientModel) getIntent().getSerializableExtra("Model");
        setProfile(patientModel);
    }

    private void setProfile(PatientModel patientModel)
    {
        if(patientModel!=null)
        {
            edName.setText(patientModel.getName());
            if(patientModel.getEmail()!=null && patientModel.getEmail().trim().length()>0) {
                edEmail.setText(patientModel.getEmail());
                edEmail.setEnabled(false);
                edEmail.setFocusable(false);
                edEmail.setClickable(false);
            }
            edAddress.setText(patientModel.getAddress());
            edContact.setText(patientModel.getContact());
            edDescription.setText(patientModel.getDescription());

            String leftStringUrl = patientModel.getProfileimage();
            if(leftStringUrl.contains("www.smile-radar.com"))
                leftStringUrl = PatientHelper.getNewImageString(leftStringUrl);
            else
                leftStringUrl = PatientHelper.getImageString(leftStringUrl);

            if(patientModel.getProfileimage()!=null && patientModel.getProfileimage().length()>0) {
                Picasso.with(this).load(leftStringUrl).fit()
                        .transform(new CircleTransform()).into(imgProfile);
                Picasso.with(this).load(patientModel.getProfileimage()).into(target);
            }
        }
    }

    public static void saveToFile(String filename,Bitmap bmp) {
        try {
            FileOutputStream out = new FileOutputStream(filename);
            bmp.compress(Bitmap.CompressFormat.PNG, 100, out);
            out.flush();
            out.close();
        } catch(Exception e) {

        }
    }

    Target target = new Target() {
        @Override
        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
            if(bitmap!=null) {
                imagePath = PatientHelper.createNewFile(MyProfileActivity.this);
                try {
                    Bitmap bitmap1 = Blur.fastblur(MyProfileActivity.this, bitmap, 1);
                    saveToFile(imagePath, bitmap1);
                    imgBanner.setImageBitmap(bitmap1);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }
        @Override
        public void onBitmapFailed(Drawable errorDrawable) {
        }
        @Override
        public void onPrepareLoad(Drawable placeHolderDrawable) {
        }
    };

    @OnClick(R.id.imgEdit)
    public void clickOnEditImage() {
        MyProfileActivityPermissionsDispatcher.showDialogForCameraWithCheck(this);
    }

    @NeedsPermission({android.Manifest.permission.CAMERA, android.Manifest.permission.WRITE_EXTERNAL_STORAGE})
    public void showDialogForCamera() {
        final CharSequence[] items = {"Take Photo", "Choose from Gallery","Remove existing", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(MyProfileActivity.this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo")) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(intent, REQUEST_CAMERA);
                } else if (items[item].equals("Choose from Gallery")) {
                    Intent intent = new Intent(
                            Intent.ACTION_PICK,
                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/*");
                    startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }else if (items[item].equals("Remove existing")) {
                    dialog.dismiss();
                    imagePath = "";
                    imgProfile.setImageResource(R.drawable.profileplaceholder);
                    imgBanner.setBackgroundColor(getResources().getColor(R.color.banner_bg));

                }
            }
        });
        builder.show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        MyProfileActivityPermissionsDispatcher.onRequestPermissionsResult(this,requestCode,grantResults);
    }

    @OnNeverAskAgain({android.Manifest.permission.CAMERA, android.Manifest.permission.WRITE_EXTERNAL_STORAGE})
    void onCameraNeverAskAgain() {
        openAppSettingsScreen();
    }

    private void openAppSettingsScreen() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MyProfileActivity.this);
        builder.setMessage("To upload a photo, allow Smile Radar to access camera and storage.").setPositiveButton("Settings", (dialogInterface, i) -> Utils.goToSettings(MyProfileActivity.this)).setNegativeButton("Not Now", (dialogInterface, i) -> MyProfileActivity.this.finish()).show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_CAMERA) {
                Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
                if(thumbnail!=null) {
                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                    thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);

                    File destination = new File(Environment.getExternalStorageDirectory(), System.currentTimeMillis() + ".jpg");
                    FileOutputStream fo;
                    try {
                        destination.createNewFile();
                        fo = new FileOutputStream(destination);
                        fo.write(bytes.toByteArray());
                        fo.close();
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    imagePath = destination.getAbsolutePath();
                    Picasso.with(MyProfileActivity.this).load(destination).fit()
                            .transform(new CircleTransform()).into(imgProfile);

                    Bitmap bitmap1= Blur.fastblur(MyProfileActivity.this, thumbnail, 1);
                    imgBanner.setImageBitmap(bitmap1);

                }
            } else if (requestCode == SELECT_FILE) {
                Uri selectedImageUri = data.getData();
                String selectedImagePath = FileUtils.getFilePathFromUri(this,selectedImageUri);
                Bitmap bm;
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeFile(selectedImagePath, options);
                final int REQUIRED_SIZE = 200;
                int scale = 1;
                while (options.outWidth / scale / 2 >= REQUIRED_SIZE
                        && options.outHeight / scale / 2 >= REQUIRED_SIZE)
                    scale *= 2;
                options.inSampleSize = scale;
                options.inJustDecodeBounds = false;
                bm = BitmapFactory.decodeFile(selectedImagePath, options);
                imagePath = selectedImagePath;
                Picasso.with(MyProfileActivity.this).load("file://"+selectedImagePath).fit()
                        .transform(new CircleTransform()).into(imgProfile);

                Bitmap bitmap1= Blur.fastblur(MyProfileActivity.this, bm, 1);
                imgBanner.setImageBitmap(bitmap1);
            }
        }
    }


    @OnClick(R.id.btnSubmit)
    public void setProfile()
    {
          String name = edName.getText().toString().trim();
          String address = edAddress.getText().toString().trim();
          String emailString = edEmail.getText().toString().trim();
          String contactString = edContact.getText().toString().trim();
          String desString =edDescription.getText().toString().trim();

        if(name.length()==0)
        {

            PatientHelper.getInstance().showExitToast(MyProfileActivity.this,"Please enter name");
        }else {

            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("patientId", pre.getPatientID());
                jsonObject.put("action", PatientConstant.ACTION);
                jsonObject.put("userName", name);
                jsonObject.put("userAddress", address);
                jsonObject.put("userEmailid", emailString);
                jsonObject.put("userContact", contactString);
                jsonObject.put("userDescription", desString);
                jsonObject.put("file",imagePath);

                new SetEditProfileAsyncTask(MyProfileActivity.this, new GenericNetworkInterface() {
                    @Override
                    public void networkResponse(ResponseModel responseModel) {
                        if(responseModel!=null && responseModel.getStatus()==1)
                        {
                            if(responseModel.getObject()!=null && responseModel.getObject() instanceof  PatientModel){
                             PatientModel model = (PatientModel) responseModel.getObject();
                                Intent intent = new Intent();
                                intent.putExtra("Model",model);
                                setResult(RESULT_OK,intent);
                                finish();
                            }
                        }
                    }
                },jsonObject).execute(PatientConstant.PATIENT_URL);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @OnClick(R.id.imgBack)
    public void closeActivity(){
        finish();
    }
}