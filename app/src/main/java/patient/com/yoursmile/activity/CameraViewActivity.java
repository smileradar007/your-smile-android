package patient.com.yoursmile.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.support.annotation.DrawableRes;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatDelegate;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.flurgle.camerakit.CameraKit;
import com.flurgle.camerakit.CameraListener;
import com.flurgle.camerakit.CameraView;
import com.yalantis.ucrop.UCrop;

import java.io.File;

import butterknife.BindView;
import butterknife.OnClick;
import patient.com.yoursmile.R;
import patient.com.yoursmile.utils.FileUtils;

/**
 * Created by mohitesh on 18/10/16.
 */

public class CameraViewActivity extends BaseActivity {

    @BindView(R.id.camera)
    CameraView camera;
    @BindView(R.id.button_capture)
    FloatingActionButton buttonCapture;
    @BindView(R.id.imgOverLay)
    ImageView imgOverLay;

    private Handler mBackgroundHandler;

    private static final String LEFT = "left", CENTER = "Center", RIGHT = "right";
    private String type;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        setUpOverlayImage();
    }

    private void setUpOverlayImage() {
        if (getIntent().hasExtra("image_type")) {
            type = getIntent().getStringExtra("image_type");
        }

        imgOverLay.setImageDrawable(ContextCompat.getDrawable(this, getOverlayDrawable()));
    }

    @Override
    protected int getMainLayout() {
        return R.layout.activity_camera_view;
    }

    @Override
    public void initializePresenter() {

    }

    @Override
    protected void onResume() {
        super.onResume();
        camera.start();
    }

    @Override
    protected void onPause() {
        camera.stop();
        super.onPause();
    }

    @OnClick(R.id.button_capture)
    public void onClick(View button) {
        buttonCapture.setEnabled(false);
        camera.setCameraListener(new CameraListener() {
            @Override
            public void onPictureTaken(byte[] jpeg) {
                super.onPictureTaken(jpeg);
                buttonCapture.setEnabled(true);

                getBackgroundHandler().post(() -> {
                    final File file = FileUtils.writeDataToTempFile(CameraViewActivity.this, jpeg);

                    new Handler(Looper.getMainLooper()).post(() -> {
                        UCrop.Options options = new UCrop.Options();
                        options.setStatusBarColor(ContextCompat.getColor(CameraViewActivity.this,R.color.colorPrimaryDark));
                        options.setToolbarColor(ContextCompat.getColor(CameraViewActivity.this,R.color.colorPrimary));
                        options.setOverlayDrawable(getOverlayDrawable());
                        options.setShowCropGrid(false);

                        UCrop.of(Uri.fromFile(file), Uri.fromFile(new File(FileUtils.getImageFileName())))
                                .useSourceImageAspectRatio()
                                .withMaxResultSize(3264, 1836)
                                .withOptions(options)
                                .start(CameraViewActivity.this);
                    });
                });
            }
        });
        camera.captureImage();
    }

    @OnClick(R.id.imageVw_switch)
    public void onSwitchClick() {
        switch (camera.toggleFacing()) {
            case CameraKit.Constants.FACING_BACK:
                Toast.makeText(this, "Switched to back camera!", Toast.LENGTH_SHORT).show();
                break;

            case CameraKit.Constants.FACING_FRONT:
                Toast.makeText(this, "Switched to front camera!", Toast.LENGTH_SHORT).show();
                break;
        }

    }

    @OnClick(R.id.imageVw_flash)
    public void onFlashClick(ImageView imageView) {
        switch (camera.toggleFlash()) {
            case CameraKit.Constants.FLASH_ON:
                imageView.setImageResource(R.drawable.ic_flash_on);
                Toast.makeText(this, "Flash on!", Toast.LENGTH_SHORT).show();
                break;

            case CameraKit.Constants.FLASH_OFF:
                imageView.setImageResource(R.drawable.ic_flash_off);
                Toast.makeText(this, "Flash off!", Toast.LENGTH_SHORT).show();
                break;

            case CameraKit.Constants.FLASH_AUTO:
                imageView.setImageResource(R.drawable.ic_flash_auto);
                Toast.makeText(this, "Flash auto!", Toast.LENGTH_SHORT).show();
                break;
        }
    }

    private Handler getBackgroundHandler() {
        if (mBackgroundHandler == null) {
            HandlerThread thread = new HandlerThread("background");
            thread.start();
            mBackgroundHandler = new Handler(thread.getLooper());
        }
        return mBackgroundHandler;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case UCrop.REQUEST_CROP:
                    final Uri resultUri = UCrop.getOutput(data);
                    String imagePath = FileUtils.getFilePathFromUri(CameraViewActivity.this, resultUri);
                    if (!TextUtils.isEmpty(imagePath)) {
                        Intent intent = new Intent();
                        intent.putExtra("imagePath", imagePath);
                        setResult(RESULT_OK, intent);
                    }
                    finish();
                    break;
            }
        }
    }

    private
    @DrawableRes
    int getOverlayDrawable() {
        if (!TextUtils.isEmpty(type)) {
            if (type.equalsIgnoreCase(CENTER)) {
                return R.drawable.frontup;
            } else if (type.equalsIgnoreCase(RIGHT)) {
                return R.drawable.rightup;
            } else {
                return R.drawable.leftup;
            }
        }
        return R.drawable.frontup;
    }
}
