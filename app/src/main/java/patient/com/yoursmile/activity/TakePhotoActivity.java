package patient.com.yoursmile.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import patient.com.yoursmile.R;
import patient.com.yoursmile.helper.PatientConstant;
import patient.com.yoursmile.helper.SharedPref;
import patient.com.yoursmile.network.SetAlbumAsyncTask;
import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.RuntimePermissions;

@RuntimePermissions
public class TakePhotoActivity extends BaseActivity {

    private static final int REQUEST_CODE_TAKE_PICTURE = 101;
    private static final String LEFT = "left", CENTER = "Center", RIGHT = "right";
    //    private static GoogleAnalytics sAnalytics;
    private static Tracker mTracker;
    @BindView(R.id.imgBack)
    ImageView imgBack;
    @BindView(R.id.txtHeader)
    TextView txtHeader;
    @BindView(R.id.btnSend)
    Button btnSend;
    @BindView(R.id.layHeader)
    RelativeLayout layHeader;
    @BindView(R.id.imageVw_front)
    ImageView imageVwFront;
    @BindView(R.id.frmLyt_front)
    FrameLayout frmLytFront;
    @BindView(R.id.imageVw_left)
    ImageView imageVwLeft;
    @BindView(R.id.frmLyt_left)
    FrameLayout frmLytLeft;
    @BindView(R.id.imageVw_right)
    ImageView imageVwRight;
    @BindView(R.id.frmLyt_right)
    FrameLayout frmLytRight;
    private String selectedImage = null;
    private String leftString = null, centerString = null, rightString = null, imagePath;
    private SharedPref pre;
    private String mScreenName = "Take Photo Screen";
    private String mUserName;
    private FirebaseAnalytics mFirebaseAnalytics;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_take_photo_new);
        ButterKnife.bind(this);
        pre = SharedPref.getInstance(TakePhotoActivity.this);
        mUserName = pre.getPatientID();

// Obtain the FirebaseAnalytics instance.
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, mUserName);
//        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, name);
        bundle.putString(FirebaseAnalytics.Param.SOURCE, mScreenName);
        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);


        // Obtain the shared Tracker instance.
        PatientApplication application = (PatientApplication) getApplication();
        mTracker = application.getDefaultTracker();
        Log.i("TakePhotoActivity", "Setting screen name: " + mScreenName + ",Id:" + mUserName);
        mTracker.setScreenName("User:" + mUserName + ", Screen Name:" + mScreenName);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());


        showAlertDialog();
    }

    @Override
    protected int getMainLayout() {
        return -1;
    }

    @Override
    public void initializePresenter() {

    }

    private void showAlertDialog() {

        if (!pre.isAlertONTF()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(TakePhotoActivity.this);
            builder.setTitle(getString(R.string.app_name));
            builder.setMessage("Please have your Cheek Retractor ready for your photo.");
            //Button One : Yes
            builder.setPositiveButton("OK", (dialog, which) -> {
                Intent intent = new Intent(TakePhotoActivity.this, TutorialCartoonActivity.class);
                startActivity(intent);
            });
            //Button Two : No
            builder.setNegativeButton("Cancel", (dialog, which) -> onBackPressed());
            //Button Three : Neutral
            builder.setNeutralButton("Don't show again", (dialog, which) -> {
                pre.setAlertONTF(true);
                Intent intent = new Intent(TakePhotoActivity.this, TutorialCartoonActivity.class);
                startActivity(intent);
            });
            AlertDialog diag = builder.create();
            diag.show();
        }
    }

    @OnClick(R.id.imgBack)
    public void closeActivity() {
        onBackPressed();
    }

    @OnClick({R.id.frmLyt_front, R.id.frmLyt_left, R.id.frmLyt_right})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.frmLyt_front:
                selectImage(CENTER);
                break;
            case R.id.frmLyt_left:
                selectImage(LEFT);
                break;
            case R.id.frmLyt_right:
                selectImage(RIGHT);
                break;
        }
    }

    private void selectImage(final String type) {
        selectedImage = type;
        final CharSequence[] items = {"Take Photo", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(TakePhotoActivity.this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, (dialog, item) -> {
            if (items[item].equals("Take Photo")) {
                TakePhotoActivityPermissionsDispatcher.openCameraWithCheck(TakePhotoActivity.this, type);
            } else if (items[item].equals("Cancel")) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        TakePhotoActivityPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults);
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {

                case REQUEST_CODE_TAKE_PICTURE:
                    Bundle bundle = data.getExtras();
                    if (bundle != null && bundle.containsKey("imagePath")) {
                        imagePath = bundle.getString("imagePath");
                    }

                    if (imagePath != null) {
                        if (selectedImage.equalsIgnoreCase(LEFT)) {
                            leftString = imagePath;
                            Picasso.with(TakePhotoActivity.this).load(new File(imagePath)).fit().into(imageVwLeft);
                        } else if (selectedImage.equalsIgnoreCase(CENTER)) {
                            centerString = imagePath;
                            Picasso.with(TakePhotoActivity.this).load(new File(imagePath)).fit().into(imageVwFront);
                        } else if (selectedImage.equalsIgnoreCase(RIGHT)) {
                            rightString = imagePath;
                            Picasso.with(TakePhotoActivity.this).load(new File(imagePath)).fit().into(imageVwRight);
                        }

                        checkAllImageSelected();
                    }
                    break;
            }
        }
    }

    @NeedsPermission({Manifest.permission.CAMERA, Manifest.permission.RECORD_AUDIO, Manifest.permission.WRITE_EXTERNAL_STORAGE})
    public void openCamera(String type) {
        Bundle bundle = new Bundle();
        bundle.putString("image_type", type);
        Intent intent = new Intent(TakePhotoActivity.this, CameraViewActivity.class);
        intent.putExtras(bundle);
        startActivityForResult(intent, REQUEST_CODE_TAKE_PICTURE);
    }


    private boolean checkAllImageSelected() {
        if (leftString != null && centerString != null && rightString != null) {
            btnSend.setVisibility(View.VISIBLE);
            return true;
        }
        btnSend.setVisibility(View.GONE);
        return false;
    }

    @OnClick(R.id.btnSend)
    public void uploadImage() {
        btnSend.setVisibility(View.INVISIBLE);
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("patientId", pre.getPatientID());
            jsonObject.put("doctorId", pre.getDoctorID());
            jsonObject.put("left", leftString);
            jsonObject.put("center", centerString);
            jsonObject.put("right", rightString);

            new SetAlbumAsyncTask(TakePhotoActivity.this, responseModel -> {
                if (responseModel != null && responseModel.getStatus() == 1) {
                    setResult(RESULT_OK);
                    onBackPressed();
                } else if (responseModel != null && responseModel.getMessage() != null) {
                    Toast.makeText(TakePhotoActivity.this, responseModel.getMessage(), Toast.LENGTH_SHORT).show();
                    btnSend.setVisibility(View.VISIBLE);
                } else {
                    btnSend.setVisibility(View.VISIBLE);
                }
            }, jsonObject).execute(PatientConstant.ALBUM);

        } catch (JSONException e) {
            btnSend.setVisibility(View.VISIBLE);
            e.printStackTrace();
        }
    }


}