package patient.com.yoursmile.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;

import com.viewpagerindicator.CirclePageIndicator;
import com.viewpagerindicator.PageIndicator;

import butterknife.BindView;
import butterknife.ButterKnife;
import patient.com.yoursmile.R;
import patient.com.yoursmile.fragment.CartoonFragment;
import patient.com.yoursmile.fragment.YoutubeFragment;

public class TutorialCartoonActivity extends FragmentActivity {
    @BindView(R.id.pager)
    ViewPager mTutorialsPager;
    PageIndicator mindicator;
    private ScreenSlidePagerAdapter mPagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tutorial);
        ButterKnife.bind(this);
        initialize();
    }

    public void skipTutorialPage(){
        Intent intent = new Intent(TutorialCartoonActivity.this,LoginActivity.class);
        startActivity(intent);
        finish();
    }

    private void initialize()
    {

        mPagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager());

        mTutorialsPager.setAdapter(mPagerAdapter);

        CirclePageIndicator indicator = (CirclePageIndicator) findViewById(R.id.indicator);
        mindicator = indicator;
        mindicator.setViewPager(mTutorialsPager);
        final float density = getResources().getDisplayMetrics().density;
        indicator.setRadius(4 * density);
        indicator.setPageColor(getResources().getColor(R.color.white));
        indicator.setFillColor(getResources().getColor(R.color.colorAccent));
    }

    private class ScreenSlidePagerAdapter extends FragmentPagerAdapter {
        public ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            Fragment fragment = null;
            Bundle bundle = new Bundle();

           /*if(position==0) {
               fragment = new YoutubeFragment();
                bundle.putInt("pos", position);
                fragment.setArguments(bundle);
            }else{*/
                fragment = new CartoonFragment();
                bundle.putInt("Position", position);
                fragment.setArguments(bundle);
//            }
            return fragment;
        }

        @Override
        public int getCount() {
            return 4;
        }

        @Override
        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }
    }
}
