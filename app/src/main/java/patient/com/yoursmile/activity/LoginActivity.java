package patient.com.yoursmile.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.gcm.GoogleCloudMessaging;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Arrays;

import hugo.weaving.DebugLog;
import patient.com.yoursmile.R;
import patient.com.yoursmile.helper.Logger;
import patient.com.yoursmile.helper.PatientConstant;
import patient.com.yoursmile.helper.PatientHelper;
import patient.com.yoursmile.helper.SharedPref;
import patient.com.yoursmile.model.LoginModel;
import patient.com.yoursmile.model.ResponseModel;
import patient.com.yoursmile.network.LoginAsnctask;
import patient.com.yoursmile.network.LoginNetworkInterface;

public class LoginActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener, View.OnClickListener {

    /* Request code used to invoke sign in user interactions. */
    private static final int RC_SIGN_IN = 9001;
    private static final String KEY_IN_RESOLUTION = "is_in_resolution";
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 101;
    private static final String PROPERTY_REG_ID = "REG_ID";
    private static final String PROPERTY_APP_VERSION = "App Version";
    private static final String TAG = LoginActivity.class.getSimpleName();
    /* Client used to interact with Google APIs. */
    private GoogleApiClient mGoogleApiClient;
    /**
     * Determines if the client is in a resolution state, and
     * waiting for resolution intent to return.
     */
    private boolean mIsInResolution;
    private CallbackManager callbackManager;
    private SharedPref sharedPref;
    private GoogleCloudMessaging gcm;
    private String regid = null;
    private LoginNetworkInterface networkInterface = new LoginNetworkInterface() {
        @Override
        public void facebookLoginResponse(ResponseModel response) {
            LoginModel loginModel = (LoginModel) response.getObject();
            Logger.error("FBLogin response", "in interface: Email:" + loginModel.getEmail() + ",Name:" + loginModel.getName() + ",ID:" + loginModel.getId() + ",Token:" + loginModel.getAccessToken());
            Logger.error("status code", loginModel.getStatus() + "" +
                    "");
            if (response != null && response.getStatus() == 1) {
                if (response.getObject() != null && response.getObject() instanceof LoginModel) {
//                    LoginModel loginModel = (LoginModel) response.getObject();
                    Logger.error(TAG, "FB response:" + response.toString());
                    Logger.error(TAG, "Setting AccessToken:" + loginModel.getAccessToken());
//                    Logger.error(TAG, "Patient ID:" + loginModel.getId());

                    sharedPref.setLoggedIn(true);
                    sharedPref.setLoginType(PatientConstant.FB_LOGIN);
                    sharedPref.setPatientID(loginModel.getId());
                    sharedPref.setAccessToken(loginModel.getAccessToken());
                    sharedPref.setDoctorID(loginModel.getDoctorId());
                    sharedPref.setDoctorName(loginModel.getDoctorname());
                    Intent send = new Intent(LoginActivity.this, ScanQRCodeActivity.class);
                    startActivity(send);
                    finish();
                }

            }
            if (response != null && response.getStatus() == 2) {
                if (response.getObject() != null && response.getObject() instanceof LoginModel) {
//                    LoginModel loginModel = (LoginModel) response.getObject();
                    sharedPref.setLoggedIn(true);
//                    Logger.error(TAG, "Patient ID:" + loginModel.getId());
                    sharedPref.setLoginType(PatientConstant.FB_LOGIN);
                    sharedPref.setPatientID(loginModel.getId());
                    sharedPref.setAccessToken(loginModel.getAccessToken());
                    sharedPref.setDoctorID(loginModel.getDoctorId());
                    sharedPref.setDoctorName(loginModel.getDoctorname());
                    Intent send = new Intent(LoginActivity.this, ScanQRCodeActivity.class);
                    startActivity(send);
                    finish();
                }
            } else if (response != null && response.getMessage() != null) {
                setupForPush();
                PatientHelper.getInstance().showExitToast(LoginActivity.this, response.getMessage());
            } else {
                PatientHelper.getInstance().showExitToast(LoginActivity.this, getString(R.string.network_err));
            }
        }

        @Override
        public void SmileLoginResponse(ResponseModel response) {

        }

        @Override
        public void gPlusLoginResponse(ResponseModel response) {
            if (response != null && response.getStatus() == 1) {
                if (response.getObject() != null && response.getObject() instanceof LoginModel) {
                    LoginModel loginModel = (LoginModel) response.getObject();
                    Logger.error("Patient Id", loginModel.getId() + "we");
                    sharedPref.setLoggedIn(true);
                    sharedPref.setLoginType(PatientConstant.G_LOGIN);
                    sharedPref.setPatientID(loginModel.getId());
                    sharedPref.setAccessToken(loginModel.getAccessToken());
                    String doctorID = loginModel.getDoctorId();
                    String doctorName = loginModel.getDoctorname();

                    if (doctorID != null && !doctorID.equalsIgnoreCase("null"))
                        sharedPref.setDoctorID(doctorID);

                    if (doctorName != null && !doctorName.equalsIgnoreCase("null"))
                        sharedPref.setDoctorName(doctorName);

                    Intent send = new Intent(LoginActivity.this, ScanQRCodeActivity.class);
                    startActivity(send);
                    finish();
                }
            }
            if (response != null && response.getStatus() == 2) {
                if (response.getObject() != null && response.getObject() instanceof LoginModel) {
                    LoginModel loginModel = (LoginModel) response.getObject();
                    Logger.error("Patient Id", loginModel.getId() + "we");
                    sharedPref.setLoggedIn(true);
                    sharedPref.setLoginType(PatientConstant.G_LOGIN);
                    sharedPref.setPatientID(loginModel.getId());
                    sharedPref.setAccessToken(loginModel.getAccessToken());

                    String doctorID = loginModel.getDoctorId();
                    String doctorName = loginModel.getDoctorname();

                    if (doctorID != null && !doctorID.equalsIgnoreCase("null"))
                        sharedPref.setDoctorID(doctorID);

                    if (doctorName != null && !doctorName.equalsIgnoreCase("null"))
                        sharedPref.setDoctorName(doctorName);

                    Intent send = new Intent(LoginActivity.this, ScanQRCodeActivity.class);
                    startActivity(send);
                    finish();
                }
            } else if (response != null && response.getMessage() != null) {
                setupForPush();
                PatientHelper.getInstance().showExitToast(LoginActivity.this, response.getMessage());
            } else {
                PatientHelper.getInstance().showExitToast(LoginActivity.this, getString(R.string.network_err));
            }

        }
    };
    /**
     * Facebook Methods
     */
    private FacebookCallback<LoginResult> callback = new FacebookCallback<LoginResult>() {
        @Override
        public void onSuccess(LoginResult loginResult) {
            GraphRequest request = GraphRequest.newMeRequest(
                    loginResult.getAccessToken(),
                    (object, response) -> {
                        // Application code
                        Logger.error("LoginActivity", "FB login success response:" + object.toString());
                        try {
                            JSONObject jsonObject = new JSONObject();
                            jsonObject.put("signupType", "1");
                            jsonObject.put("socialLoginMedium", "1");
                            jsonObject.put("socialId", object.get("id"));
                            jsonObject.put("platform", PatientConstant.PLATFORM);
                            jsonObject.put("name", object.optString("name"));
                            jsonObject.put("deviceToken", regid);
                            jsonObject.put("address", "");
                            jsonObject.put("email", object.optString("email"));
                            jsonObject.put("contact", "");
                            jsonObject.put("profileimage", "");
                            jsonObject.put("description", "");

                            new LoginAsnctask(LoginActivity.this, networkInterface, jsonObject, 1).execute();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    });
            Bundle parameters = new Bundle();
            parameters.putString("fields", "id,name,email,gender, birthday");
            request.setParameters(parameters);
            request.executeAsync();
        }

        @Override
        public void onCancel() {

        }

        @Override
        public void onError(FacebookException e) {

        }
    };

    /**
     * @return Application's version code from the {@code PackageManager}.
     */
    private static int getAppVersion(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            // should never happen
            throw new RuntimeException("Could not get package name: " + e);
        }
    }

    /**
     * Called when the activity is starting. Restores the activity state.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        sharedPref = SharedPref.getInstance(LoginActivity.this);
//        android_id = Secure.getString(getContentResolver(), Secure.ANDROID_ID);

        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();

        if (savedInstanceState != null) {
            mIsInResolution = savedInstanceState.getBoolean(KEY_IN_RESOLUTION, false);
        }

        setUpForGoogleSignIn();
        setupForPush();

        Button loginButton = (Button) findViewById(R.id.login_button);
        LoginManager.getInstance().registerCallback(callbackManager, callback);
        loginButton.setOnClickListener(this);
        findViewById(R.id.login_button_smile).setOnClickListener(this);
        findViewById(R.id.sign_in_button).setOnClickListener(this);
//        findViewById(R.id.tvDocApp).setOnClickListener(this);
    }

    private void setUpForGoogleSignIn() {
        // [START configure_signin
        // Configure sign-in to request the user's ID, email address, and basic
        // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        // [END configure_signin]

        // [START build_client]
        // Build a GoogleApiClient with access to the Google Sign-In API and the
        // options specified by gso.
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
        // [END build_client]

    }

    /**
     * Saves the resolution state.
     */
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(KEY_IN_RESOLUTION, mIsInResolution);
    }

    /**
     * Called when {@code mGoogleApiClient} is trying to connect but failed.
     * Handle {@code result.getResolution()} if there is a resolution
     * available.
     */
    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
    }

    private void onSignInClicked() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.login_button_smile:
                Intent intent = new Intent(LoginActivity.this, Smile_login_Activity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivityForResult(intent, 999);
                break;
            case R.id.sign_in_button:
                onSignInClicked();
                break;

            case R.id.login_button:
                LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "email")/*Collections.singletonList("public_profile")*/);
                break;

            /*case R.id.tvDocApp:
                final String appPackageName = "com.smileradar.doc";
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                }

                break;*/
        }
    }

    @DebugLog
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG, "onActivityResult:" + requestCode + ":" + resultCode + ":" + data);
        callbackManager.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case RC_SIGN_IN:
                // If the error resolution was not successful we should not resolve further.
                GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
                handleSignInResult(result);
                break;
            case 999:
                if (resultCode == RESULT_OK) {
                    finish();
                }
                break;
        }
    }

    private void handleSignInResult(GoogleSignInResult result) {
        Log.d(TAG, "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();
            if (acct != null) {
                try {

                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("signupType", "1");
                    jsonObject.put("socialLoginMedium", "2");
                    jsonObject.put("socialId", acct.getId());
                    jsonObject.put("platform", PatientConstant.PLATFORM);
                    jsonObject.put("name", acct.getDisplayName());
                    jsonObject.put("deviceToken", regid);
                    jsonObject.put("address", "");
                    jsonObject.put("email", acct.getEmail());
                    jsonObject.put("contact", "");
                    jsonObject.put("description", "");
                    jsonObject.put("profileimage", "");

                    new LoginAsnctask(LoginActivity.this, networkInterface, jsonObject, 2).execute();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void setupForPush() {
        if (checkPlayServices()) {
            gcm = GoogleCloudMessaging.getInstance(this);
            regid = getRegistrationId(LoginActivity.this);
            if (regid.isEmpty()) {
                registerInBackground();
            }
        } else {
            Log.i(TAG, "No valid Google Play Services APK found.");
        }
    }

    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Log.i(TAG, "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }

    /**
     * Gets the current registration ID for application on GCM service.
     * <p>
     * If result is empty, the app needs to register.
     *
     * @return registration ID, or empty string if there is no existing
     * registration ID.
     */
    private String getRegistrationId(Context context) {
        final SharedPreferences prefs = getGCMPreferences();
        String registrationId = prefs.getString(PROPERTY_REG_ID, "");
        if (registrationId.isEmpty()) {
            Log.i(TAG, "Registration not found.");
            return "";
        }
        // Check if app was updated; if so, it must clear the registration ID
        // since the existing regID is not guaranteed to work with the new
        // app version.
        int registeredVersion = prefs.getInt(PROPERTY_APP_VERSION, Integer.MIN_VALUE);
        int currentVersion = getAppVersion(context);
        if (registeredVersion != currentVersion) {
            Log.i(TAG, "App version changed.");
            return "";
        }
        return registrationId;
    }

    /**
     * @return Application's {@code SharedPreferences}.
     */
    private SharedPreferences getGCMPreferences() {
        // This sample app persists the registration ID in shared preferences, but
        // how you store the regID in your app is up to you.
        return getSharedPreferences("GCM_PREF", Context.MODE_PRIVATE);
    }

    /**
     * Registers the application with GCM servers asynchronously.
     * <p>
     * Stores the registration ID and app versionCode in the application's
     * shared preferences.
     */
    private void registerInBackground() {
        new GCMUpdateAsync().execute();
    }

    /**
     * Sends the registration ID to your server over HTTP, so it can use GCM/HTTP
     * or CCS to send messages to your app. Not needed for this demo since the
     * device sends upstream messages to a server that echoes back the message
     * using the 'from' address in the message.
     */
    private void sendRegistrationIdToBackend() {

    }

    /**
     * Stores the registration ID and app versionCode in the application's
     * {@code SharedPreferences}.
     *
     * @param context application's context.
     * @param regId   registration ID
     */
    private void storeRegistrationId(Context context, String regId) {
        final SharedPreferences prefs = getGCMPreferences();
        int appVersion = getAppVersion(context);
        Log.i(TAG, "Saving regId on app version " + appVersion);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(PROPERTY_REG_ID, regId);
        editor.apply();
    }

    class GCMUpdateAsync extends AsyncTask<Void, Void, String> {
        @Override
        protected String doInBackground(Void... params) {
            String msg = "";
            try {
                if (gcm == null) {
                    gcm = GoogleCloudMessaging.getInstance(LoginActivity.this);
                }
                regid = gcm.register(PatientConstant.SENDER_ID);
                msg = "Device registered, registration ID=" + regid;

                // You should send the registration ID to your server over HTTP,
                // so it can use GCM/HTTP or CCS to send messages to your app.
                // The request to your server should be authenticated if your app
                // is using accounts.
                sendRegistrationIdToBackend();

                // For this demo: we don't need to send it because the device
                // will send upstream messages to a server that echo back the
                // message using the 'from' address in the message.

                // Persist the regID - no need to register again.
                storeRegistrationId(LoginActivity.this, regid);
            } catch (IOException ex) {
                msg = "Error :" + ex.getMessage();
                // If there is an error, don't just keep trying to register.
                // Require the user to click a button again, or perform
                // exponential back-off.
            }
            return msg;
        }

        @Override
        protected void onPostExecute(String msg) {
            //	 mDisplay.append(msg + "\n");
            //				 sendPush();
        }
    }
}

