package patient.com.yoursmile.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;

import com.viewpagerindicator.CirclePageIndicator;
import com.viewpagerindicator.PageIndicator;

import butterknife.BindView;
import butterknife.ButterKnife;
import patient.com.yoursmile.R;
import patient.com.yoursmile.fragment.TutorialFragment1;
import patient.com.yoursmile.fragment.TutorialFragment2;
import patient.com.yoursmile.fragment.TutorialFragment3;

public class TutorialActivity extends FragmentActivity {
    @BindView(R.id.pager)
    ViewPager mTutorialsPager;
    PageIndicator mindicator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tutorial);
        ButterKnife.bind(this);
        initialize();

    }

    public void skipTutorialPage(){
        Intent intent = new Intent(TutorialActivity.this,LoginActivity.class);
        startActivity(intent);
        finish();
    }

    private void initialize()
    {

        ScreenSlidePagerAdapter mPagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager());

        mTutorialsPager.setAdapter(mPagerAdapter);

        CirclePageIndicator indicator = (CirclePageIndicator) findViewById(R.id.indicator);
        mindicator = indicator;
        mindicator.setViewPager(mTutorialsPager);
        final float density = getResources().getDisplayMetrics().density;
        indicator.setRadius(4 * density);
        indicator.setPageColor(getResources().getColor(R.color.white));
        indicator.setFillColor(getResources().getColor(R.color.colorAccent));

       /* findViewById(R.id.sign_in_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TutorialActivity.this, HomeActivity.class);
                startActivity(intent);
                finish();
            }
        });*/
    }

    private class ScreenSlidePagerAdapter extends FragmentPagerAdapter {
        public ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            Fragment fragment = null;
            Bundle bundle = new Bundle();
            switch (position){
                case 0:
                    fragment = new TutorialFragment1();
                    bundle.putInt("pos", position);
                    fragment.setArguments(bundle);
                    break;

                case 1:
                    fragment = new TutorialFragment2();
                    bundle.putInt("pos", position);
                    fragment.setArguments(bundle);
                    break;

                case 2:
                    fragment = new TutorialFragment3();
                    bundle.putInt("pos", position);
                    fragment.setArguments(bundle);
                    break;

                default:
                    fragment = new TutorialFragment1();
                    bundle.putInt("pos", position);
                    fragment.setArguments(bundle);
                    break;
            }

            return fragment;
        }

        @Override
        public int getCount() {
            return 3;
        }

        @Override
        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }
    }
}
