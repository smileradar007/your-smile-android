package patient.com.yoursmile.activity;

import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Display;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.io.File;

import butterknife.BindView;
import butterknife.OnClick;
import patient.com.yoursmile.R;
import patient.com.yoursmile.helper.Logger;
import patient.com.yoursmile.helper.PatientConstant;
import patient.com.yoursmile.helper.PatientHelper;
import patient.com.yoursmile.model.AlbumModel;
import patient.com.yoursmile.network.AlbumEditAsynctask;
import patient.com.yoursmile.network.GetAlbumAsynctask;

public class AlbumActivity extends BaseActivity {

    public static final int REQUEST_CODE_TAKE_PICTURE = 0x2;
    private static final String LEFT = "left", CENTER = "Center", RIGHT = "right";
    @BindView(R.id.btnSend)
    Button btnSend;
    @BindView(R.id.btnLeft)
    Button btnLeft;
    @BindView(R.id.btnRight)
    Button btnRight;
    @BindView(R.id.btnCenter)
    Button btnCenter;
    @BindView(R.id.imgLeft)
    ImageView imgLeft;
    @BindView(R.id.imgCenter)
    ImageView imgCenter;
    @BindView(R.id.imgRight)
    ImageView imgRight;
    @BindView(R.id.imgBack)
    ImageView imgBack;
    @BindView(R.id.txtLeftReject)
    TextView txtLeftReject;
    @BindView(R.id.txtCenterReject)
    TextView txtCenterReject;
    @BindView(R.id.txtRightReject)
    TextView txtRightReject;
    @BindView(R.id.txtLeftLable)
    TextView txtLeftLable;
    @BindView(R.id.txtCenterLable)
    TextView txtCenterLable;
    @BindView(R.id.txtRightLable)
    TextView txtRightLable;
    @BindView(R.id.lay_center)
    RelativeLayout centerLayout;
    @BindView(R.id.lay_left)
    RelativeLayout leftLayout;
    @BindView(R.id.lay_right)
    RelativeLayout rightLayout;
    private String albumType = "albumRejected", albumID = null;
    private boolean leftImageAccepted, centerImageAccepted, rightImageAccepted;
    private String selectedImage = null;

    private String leftString = null, centerString = null, rightString = null, imagePath;

    //    private NotificationModel notificationModel;
    private AlbumModel albumModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;

        Logger.error("Height ", ((width / 2) + ""));

        RelativeLayout.LayoutParams parms1 = new RelativeLayout.LayoutParams(width, (width / 2) - 70);
        centerLayout.setLayoutParams(parms1);
        RelativeLayout.LayoutParams parms2 = new RelativeLayout.LayoutParams(width, (width / 2) - 70);
        leftLayout.setLayoutParams(parms2);
        RelativeLayout.LayoutParams parms3 = new RelativeLayout.LayoutParams(width, (width / 2) - 70);
        rightLayout.setLayoutParams(parms3);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null && bundle.containsKey("ID")) {
            txtLeftReject.setVisibility(View.VISIBLE);
            txtRightReject.setVisibility(View.VISIBLE);
            txtCenterReject.setVisibility(View.VISIBLE);
            albumID = bundle.getString("ID");
            getAlbumModel();
        }
    }

    @Override
    protected int getMainLayout() {
        return R.layout.activity_take_photo;
    }

    @Override
    public void initializePresenter() {

    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null && bundle.containsKey("ID")) {
            txtLeftReject.setVisibility(View.VISIBLE);
            txtRightReject.setVisibility(View.VISIBLE);
            txtCenterReject.setVisibility(View.VISIBLE);
            albumID = bundle.getString("ID");
            getAlbumModel();
        }
    }

    @OnClick(R.id.btnLeft)
    public void clickOnLeft() {
        if (!leftImageAccepted)
            selectImage(LEFT);
    }

    @OnClick(R.id.btnCenter)
    public void clickOnCenter() {
        if (!centerImageAccepted)
            selectImage(CENTER);
    }

    @OnClick(R.id.btnRight)
    public void clickOnRight() {
        if (!rightImageAccepted)
            selectImage(RIGHT);
    }

    @OnClick(R.id.imgLeft)
    public void clickOnLeftImage() {
        if (!leftImageAccepted)
            selectImage(LEFT);
    }

    @OnClick(R.id.imgCenter)
    public void clickOnCenterImage() {
        if (!centerImageAccepted)
            selectImage(CENTER);
    }

    @OnClick(R.id.imgRight)
    public void clickOnRightImage() {
        if (!rightImageAccepted)
            selectImage(RIGHT);
    }


    private void selectImage(final String type) {
        selectedImage = type;

        //"Choose from Gallery",
        final CharSequence[] items = {"Take Photo", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(AlbumActivity.this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, (dialog, item) -> {
            if (items[item].equals("Take Photo")) {
                openCamera(type);
            } else if (items[item].equals("Cancel")) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    private void openCamera(String type) {
        Bundle bundle = new Bundle();
        bundle.putString("image_type", type);
        Intent intent = new Intent(AlbumActivity.this, CameraViewActivity.class);
        intent.putExtras(bundle);
        startActivityForResult(intent, REQUEST_CODE_TAKE_PICTURE);
    }


    private void getAlbumModel() {
        new GetAlbumAsynctask(AlbumActivity.this, responseModel -> {
            if (responseModel != null && responseModel.getStatus() == 1) {
                if (responseModel.getObject() != null && responseModel.getObject() instanceof AlbumModel) {
                    albumModel = (AlbumModel) responseModel.getObject();
                    setAlbumModel();
                }
            } else if (responseModel != null) {
                PatientHelper.getInstance().showExitToast(AlbumActivity.this, responseModel.getMessage());
            }
        }).execute(PatientConstant.BASE_URL + "album.json/id/" + albumID);
    }


    private void setAlbumModel() {
        if (albumType != null) {
            if (albumType.equalsIgnoreCase("albumRejected")) {
                //TODO Here album values going to set on UI
                if (albumModel != null) {

                    if (albumModel.getCenterImageStatus().equalsIgnoreCase("1")) {
                        String url = albumModel.getCenterImage();
                        if (url.contains("http://dentist.yoursmiledirect.com"))
                            url = url.replace("http://dentist.yoursmiledirect.com", PatientConstant.PROFILE_IMAGE_BASE_URL + "doctors/public");

                        Logger.error("center image", url);
                        Picasso.with(AlbumActivity.this).load(url).fit()
                                .into(imgCenter);
                        btnCenter.setVisibility(View.INVISIBLE);
                        centerImageAccepted = true;
                        txtCenterReject.setText(getString(R.string.accepted));
                    } else if (albumModel.getCenterImageStatus().equalsIgnoreCase("2")) {
                        txtCenterReject.setText(getString(R.string.rejected));
                        txtCenterLable.setText(getString(R.string.front_teeth_photo));
                        Picasso.with(AlbumActivity.this).load(R.drawable.ic_front_banner).fit()
                                .into(imgCenter);
                    }


                    if (albumModel.getLeftImageStatus().equalsIgnoreCase("1")) {
                        String url = albumModel.getLeftImage();
                        if (url.contains("http://dentist.yoursmiledirect.com"))
                            url = url.replace("http://dentist.yoursmiledirect.com", PatientConstant.PROFILE_IMAGE_BASE_URL + "doctors/public");
                        Logger.error("left image", url);
                        Picasso.with(AlbumActivity.this).load(url).fit()
                                .into(imgLeft);
                        leftImageAccepted = true;
                        txtLeftReject.setText(getString(R.string.accepted));
                        btnLeft.setVisibility(View.INVISIBLE);
                    } else if (albumModel.getLeftImageStatus().equalsIgnoreCase("2")) {
                        txtLeftReject.setText(getString(R.string.rejected));
                        txtLeftLable.setText(getString(R.string.left_teeth_photo));
                        Picasso.with(AlbumActivity.this).load(R.drawable.ic_left_banner).fit()
                                .into(imgLeft);
                    }

                    if (albumModel.getRightImageStaus().equalsIgnoreCase("1")) {
                        String url = albumModel.getRightImage();
                        if (url.contains("http://dentist.yoursmiledirect.com"))
                            url = url.replace("http://dentist.yoursmiledirect.com", PatientConstant.PROFILE_IMAGE_BASE_URL + "doctors/public");
                        Logger.error("right image", url);
                        Picasso.with(AlbumActivity.this).load(url).fit()
                                .into(imgRight);
                        rightImageAccepted = true;
                        btnRight.setVisibility(View.INVISIBLE);
                        txtRightReject.setText(getString(R.string.accepted));
                    } else if (albumModel.getRightImageStaus().equalsIgnoreCase("2")) {
                        txtRightReject.setText(getString(R.string.rejected));
                        txtRightLable.setText(getString(R.string.right_teeth_photo));
                        Picasso.with(AlbumActivity.this).load(R.drawable.ic_right_banner).fit()
                                .into(imgRight);
                    }
                }
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {

                case REQUEST_CODE_TAKE_PICTURE:
                    Bundle bundle = data.getExtras();
                    if (bundle != null && bundle.containsKey("imagePath")) {
                        imagePath = bundle.getString("imagePath");
                    }

                    if (imagePath != null) {
                        if (selectedImage.equalsIgnoreCase(LEFT)) {
                            leftString = imagePath;
                            Picasso.with(AlbumActivity.this).load(new File(imagePath)).fit().into(imgLeft);
                        } else if (selectedImage.equalsIgnoreCase(CENTER)) {
                            centerString = imagePath;
                            Picasso.with(AlbumActivity.this).load(new File(imagePath)).fit().into(imgCenter);
                        } else if (selectedImage.equalsIgnoreCase(RIGHT)) {
                            rightString = imagePath;
                            Picasso.with(AlbumActivity.this).load(new File(imagePath)).fit().into(imgRight);
                        }

                        checkAllImageSelected();
                    }
                    break;

            }
        }
    }

    private boolean checkAllImageSelected() {
        if (albumType != null) {
            if (albumType.equalsIgnoreCase("accountAccepted")) {
                if (leftString != null && centerString != null && rightString != null) {
                    btnSend.setVisibility(View.VISIBLE);
                    return true;
                }
            } else if (albumType.equalsIgnoreCase("albumRejected")) {
                if (checkRejectedAlbumImageUpload()) {
                    btnSend.setVisibility(View.VISIBLE);
                    return true;
                }
            }
        }
        btnSend.setVisibility(View.GONE);
        return false;
    }


    private boolean checkRejectedAlbumImageUpload() {
        int a = 0, b = 0, c = 0;
        if (!centerImageAccepted) {
            if (!TextUtils.isEmpty(centerString))
                a = 0;
            else
                a = 1;
        }

        if (!leftImageAccepted) {
            if (!TextUtils.isEmpty(leftString))
                b = 0;
            else
                b = 1;
        }

        if (!rightImageAccepted) {
            if (!TextUtils.isEmpty(rightString))
                c = 0;
            else
                c = 1;
        }
        return (a == 0 && b == 0 && c == 0);
    }


    @OnClick(R.id.btnSend)
    public void uploadImage() {
        btnSend.setVisibility(View.INVISIBLE);

        if (albumType != null && albumID != null) {
            if (albumType.equalsIgnoreCase("accountAccepted")) {

                new AlbumEditAsynctask(AlbumActivity.this, responseModel -> {
                    if (responseModel != null && responseModel.getStatus() == 1) {
                        setResult(RESULT_OK);
                        finish();
                    } else {
                        btnSend.setVisibility(View.VISIBLE);
                    }
                }, albumID, leftString, centerString, rightString).execute(PatientConstant.ALBUM);

            } else if (albumType.equalsIgnoreCase("albumRejected")) {


                if (leftImageAccepted)
                    leftString = null;

                if (centerImageAccepted)
                    centerString = null;

                if (rightImageAccepted)
                    rightString = null;


                new AlbumEditAsynctask(AlbumActivity.this, responseModel -> {
                    if (responseModel != null && responseModel.getStatus() == 1) {
                        setResult(RESULT_OK);
                        finish();
                    } else if (responseModel != null && responseModel.getMessage() != null) {
                        Toast.makeText(AlbumActivity.this, responseModel.getMessage(), Toast.LENGTH_SHORT).show();
                        btnSend.setVisibility(View.VISIBLE);
                    } else {
                        btnSend.setVisibility(View.VISIBLE);
                    }
                }, albumID, leftString, centerString, rightString).execute(PatientConstant.ALBUM);

            } else {
                btnSend.setVisibility(View.VISIBLE);
            }
        }
    }

    @OnClick(R.id.imgBack)
    public void closeActivity() {
        finish();
    }
}