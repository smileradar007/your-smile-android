package patient.com.yoursmile.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import butterknife.ButterKnife;

/**
 * Created by mohitesh on 02/07/16.
 */

public abstract class BaseActivity extends AppCompatActivity {

    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getMainLayout() != -1) {
            setContentView(getMainLayout());
            ButterKnife.bind(this);
        }


        initializePresenter();
    }

    protected abstract int getMainLayout();


    public abstract void initializePresenter();

    @Override
    public void onBackPressed() {
        try {
            if (((PatientApplication) getApplicationContext()).getCreated() == 1) {
                finish();
                if (!this.getClass().getSimpleName().equalsIgnoreCase("HomeActivity")) {
                    Intent intent = new Intent(this, HomeActivity.class);
                    intent.putExtra("overRideAnimations", true);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                }
            } else {
                super.onBackPressed();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    @Override
    protected void onStop() {
        super.onStop();
    }
}
