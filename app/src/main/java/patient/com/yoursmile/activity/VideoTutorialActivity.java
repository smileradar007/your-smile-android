package patient.com.yoursmile.activity;

import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.MediaController;
import android.widget.VideoView;

import patient.com.yoursmile.R;
import patient.com.yoursmile.helper.SharedPref;

public class VideoTutorialActivity extends AppCompatActivity {

    private VideoView tutorialVideoView;
    private SharedPref sharedPref;

    private MediaController mediaController;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_tutorial);

        tutorialVideoView = (VideoView) findViewById(R.id.tutorialVideoView);
        sharedPref = SharedPref.getInstance(VideoTutorialActivity.this);
        //Creating MediaController
        mediaController = new MediaController(this);

        Button button = (Button) findViewById(R.id.btnSkipVideo);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tutorialVideoView.isPlaying())
                    tutorialVideoView.stopPlayback();
                navigateUserToActivity();
            }
        });


    }

    @Override
    protected void onResume() {
        super.onResume();

        mediaController.setAnchorView(tutorialVideoView);
        //specify the location of media file
        Uri video = Uri.parse("android.resource://" + getPackageName() + "/"
                + R.raw.splash_video);

        tutorialVideoView.setVideoURI(video);
        tutorialVideoView.start();
        tutorialVideoView.setZOrderOnTop(true);
        tutorialVideoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                mp.stop();

                navigateUserToActivity();
            }
        });
    }

    private void navigateUserToActivity() {
        if (sharedPref.isFirstTime()) {
            sharedPref.setFirstTime(false);
            Intent intent = new Intent(VideoTutorialActivity.this, TutorialActivity.class);
            startActivity(intent);
            finish();
        } else if (sharedPref.isLoggedIn()) {
            sharedPref.setFirstTime(false);
            if (sharedPref.getDoctorID().length() > 0) {
                Intent intent = new Intent(VideoTutorialActivity.this, HomeActivity.class);
                startActivity(intent);
                finish();
            } else {
                Intent intent = new Intent(VideoTutorialActivity.this, ScanQRCodeActivity.class);
                startActivity(intent);
                finish();
            }
        } else {
            Intent intent = new Intent(VideoTutorialActivity.this, LoginActivity.class);
            startActivity(intent);
            finish();
        }
    }
}
