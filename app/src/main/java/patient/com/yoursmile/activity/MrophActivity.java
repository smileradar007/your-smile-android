package patient.com.yoursmile.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import patient.com.yoursmile.R;
import patient.com.yoursmile.helper.PatientHelper;

public class MrophActivity extends Activity {

    @BindView(R.id.imgBack)
    ImageView imgBack;

    @BindView(R.id.imgShare)
    ImageView imgShare;

    @BindView(R.id.imgMorph)
    ImageView imgMorph;

    private String fileshareString = "";
    private String message = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mroph);
        ButterKnife.bind(this);

        Bundle bundle = getIntent().getExtras();
        if(bundle!=null && bundle.containsKey("Morph")){

            String imageURL = bundle.getString("Morph");
            message =  bundle.getString("Text");

            if(imageURL!=null && imageURL.length()>0){
                Picasso.with(this)
                        .load(imageURL)
                        .resizeDimen(R.dimen.adpter_album_history_image_slider, R.dimen.adpter_album_history_image_slider)
                        .into(new Target() {
                            @Override
                            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                                if(bitmap!=null){
                                    imgMorph.setImageBitmap(bitmap);
                                    fileshareString = PatientHelper.createNewFile(MrophActivity.this);
                                    FileOutputStream outStream = null;
                                    try {
                                        outStream = new FileOutputStream(fileshareString);
                                        bitmap.compress(Bitmap.CompressFormat.PNG, 100, outStream);
                                    } catch (FileNotFoundException e) {
                                        e.printStackTrace();
                                    }
                                }

                            }
                            @Override
                            public void onBitmapFailed(Drawable errorDrawable) {
                            }
                            @Override
                            public void onPrepareLoad(Drawable placeHolderDrawable) {
                            }
                        });
            }
        }
    }



    @OnClick(R.id.imgBack)
    public void closeAcitvity()
    {
        finish();
    }


    @OnClick(R.id.imgShare)
    public void shareAcitvity()
    {
        Intent shareIntent = new Intent();
        shareIntent.setAction(Intent.ACTION_SEND);
        shareIntent.putExtra(Intent.EXTRA_TEXT, message);
        shareIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(new File(fileshareString)));  //optional//use this when you want to send an image
        shareIntent.setType("image/jpeg");
        shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        startActivity(Intent.createChooser(shareIntent, "send"));
    }
}
