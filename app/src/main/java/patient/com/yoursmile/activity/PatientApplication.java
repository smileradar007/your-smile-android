package patient.com.yoursmile.activity;

import android.app.Activity;
import android.app.Application;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;

import org.acra.ACRA;
import org.acra.ReportingInteractionMode;
import org.acra.annotation.ReportsCrashes;

import patient.com.yoursmile.R;
import patient.com.yoursmile.utils.Foreground;

/**
 * Created by vineet on 10/1/16.
 */
@ReportsCrashes(formKey = "", // will not be used
        mailTo = "hsandeepkesharwani0708@gmail.com", // my email here
        mode = ReportingInteractionMode.TOAST,
        resToastText = R.string.crash_toast_text)
public class PatientApplication extends Application {
    private static GoogleAnalytics sAnalytics;
    private static Tracker sTracker;
    private int created;

    @Override
    public void onCreate() {
        super.onCreate();
        ACRA.init(this);
        sAnalytics = GoogleAnalytics.getInstance(this);
        registerActivityLifecycleCallbacks(new MyActivityLifecycleCallbacks());
        registerActivityLifecycleCallbacks(new Foreground());
    }

    /**
     * Gets the default {@link Tracker} for this {@link Application}.
     * @return tracker
     */
    synchronized public Tracker getDefaultTracker() {
        // To enable debug logging use: adb shell setprop log.tag.GAv4 DEBUG
        if (sTracker == null) {
            sTracker = sAnalytics.newTracker(R.xml.global_tracker);
        }

        return sTracker;
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    public int getCreated() {
        return created;
    }

    public void setCreated(int created) {
        this.created = created;
    }

    private class MyActivityLifecycleCallbacks implements ActivityLifecycleCallbacks {

        @Override
        public void onActivityCreated(Activity activity, Bundle arg1) {
            created++;

        }

        @Override
        public void onActivityDestroyed(Activity activity) {
            created--;
        }

        @Override
        public void onActivityPaused(Activity arg0) {
        }

        @Override
        public void onActivityResumed(Activity arg0) {
        }

        @Override
        public void onActivitySaveInstanceState(Activity arg0, Bundle arg1) {

        }

        @Override
        public void onActivityStarted(Activity arg0) {

        }

        @Override
        public void onActivityStopped(Activity arg0) {


        }

    }
}
