package patient.com.yoursmile.activity;

import android.app.Activity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import patient.com.yoursmile.R;
import patient.com.yoursmile.model.ResponseModel;
import patient.com.yoursmile.network.ChangePasswordAsync;
import patient.com.yoursmile.network.GenericNetworkInterface;

import static patient.com.yoursmile.helper.PatientConstant.BASE_URL;

public class ChangePassActivity extends Activity {

    String urlString = BASE_URL+"/password.json";

    @BindView(R.id.edPassword)
    EditText edPassword;
    @BindView(R.id.edNewPassword)
    EditText edNewPassword;
    @BindView(R.id.edConPassword)
    EditText edConPassword;
    @BindView(R.id.btnSubmit)
    Button btnSubmit;
    @BindView(R.id.imgBack)
    ImageView imgBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_pass);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.btnSubmit)
    public void onClickSubmit(){
        String  currentPassword  =  edPassword.getText().toString().trim();
        String  newPassword      =  edNewPassword.getText().toString().trim();
        String  confPassword     =  edConPassword.getText().toString().trim();

        if(currentPassword.length()==0){
            Toast.makeText(ChangePassActivity.this,"Please enter current password",Toast.LENGTH_SHORT).show();
        }else  if(newPassword.length()==0){
            Toast.makeText(ChangePassActivity.this,"Please enter new password",Toast.LENGTH_SHORT).show();
        }else  if(confPassword.length()==0){
            Toast.makeText(ChangePassActivity.this,"Please enter confirm password",Toast.LENGTH_SHORT).show();
        }
        else if (currentPassword.length()<8){
            edPassword.setText("");
            Toast.makeText(ChangePassActivity.this,"Please enter current password at least 8 characters",Toast.LENGTH_SHORT).show();
        }
        else if (newPassword.length()<8){
            edNewPassword.setText("");
            edConPassword.setText("");
            Toast.makeText(ChangePassActivity.this,"Please enter new password at least 8 characters",Toast.LENGTH_SHORT).show();
        }
        else if (confPassword.length()<8){
            edNewPassword.setText("");
            edConPassword.setText("");
            Toast.makeText(ChangePassActivity.this,"Please enter confirm password at least 8 characters",Toast.LENGTH_SHORT).show();
        }
        else  if(!newPassword.equals(confPassword))
        {
            edNewPassword.setText("");
            edConPassword.setText("");
            Toast.makeText(ChangePassActivity.this,"New password and confirm password doesn't match",Toast.LENGTH_SHORT).show();
        }else{
            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("appType","1");
                jsonObject.put("newPassword",newPassword);
                jsonObject.put("oldPassword",currentPassword);

                new ChangePasswordAsync(ChangePassActivity.this, new GenericNetworkInterface() {
                    @Override
                    public void networkResponse(ResponseModel responseModel) {
                        if(responseModel!=null){
                            String message = responseModel.getMessage();
                            if(message!=null)
                                Toast.makeText(ChangePassActivity.this,message,Toast.LENGTH_SHORT).show();
                            if(responseModel.getStatus()== 1){
                                finish();
                            }
                        }
                    }
                }).execute(jsonObject.toString(),urlString);
            }catch (JSONException e){
                e.printStackTrace();
            }
        }
    }

    @OnClick(R.id.imgBack)
    public void onClickFinish(){
        finish();
    }
}
