package patient.com.yoursmile.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.StyleSpan;
import android.text.style.UnderlineSpan;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.journeyapps.barcodescanner.CaptureActivity;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.OnClick;
import patient.com.yoursmile.R;
import patient.com.yoursmile.helper.PatientHelper;
import patient.com.yoursmile.helper.SharedPref;
import patient.com.yoursmile.model.ResponseModel;
import patient.com.yoursmile.network.LinkToDoctorAsynctask;
import patient.com.yoursmile.network.LoginNetworkInterface;
import patient.com.yoursmile.utils.Utils;
import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.OnNeverAskAgain;
import permissions.dispatcher.RuntimePermissions;

@RuntimePermissions
public class ScanQRCodeActivity extends BaseActivity {

    @BindView(R.id.btnSubmit)
    Button btnSubmit;
    @BindView(R.id.btnScan)
    Button btnScan;
    @BindView(R.id.edQR)
    EditText edQR;
    @BindView(R.id.txtDoctorLink)
    TextView txtDoctorLink;
    private SharedPref sharedPref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        sharedPref = SharedPref.getInstance(ScanQRCodeActivity.this);


        ProgressDialog mProgressDialog = new ProgressDialog(ScanQRCodeActivity.this);
        mProgressDialog.setTitle(getString(R.string.app_name));
        mProgressDialog.setMessage("Please wait...");

        if (sharedPref.getDoctorID().length() > 0 && !sharedPref.getDoctorName().equalsIgnoreCase("null")) {
            txtDoctorLink.setVisibility(View.VISIBLE);
            final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD);
            SpannableString content = new SpannableString("Continue with Doctor " + sharedPref.getDoctorName());

            content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
            int start = content.length() - sharedPref.getDoctorName().length();
            content.setSpan(bss, start - 1, content.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
            txtDoctorLink.setText(content);
        }
    }

    @Override
    protected int getMainLayout() {
        return R.layout.activity_scan_qrcode;
    }

    @Override
    public void initializePresenter() {

    }

    @OnClick(R.id.btnSubmit)
    public void clickOnSubmit() {
        String qrString = edQR.getText().toString().trim();

        if (qrString.length() == 0) {
            PatientHelper.getInstance().showExitToast(ScanQRCodeActivity.this, "Please enter Doctor ID.");
        } else {
            qrString = qrString.trim();
            hitLinktoDoctor(qrString, true);
        }
    }

    @OnClick(R.id.btnScan)
    public void clickOnScan() {
        ScanQRCodeActivityPermissionsDispatcher.openScanActivityWithCheck(this);
    }

    @NeedsPermission({android.Manifest.permission.CAMERA, android.Manifest.permission.WRITE_EXTERNAL_STORAGE})
    public void openScanActivity() {
        Intent intent = new Intent(ScanQRCodeActivity.this, CaptureActivity.class);
        intent.putExtra("SCAN_MODE", "QR_CODE_MODE");
        startActivityForResult(intent, 0);
    }

    @OnNeverAskAgain({android.Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE})
    void onCameraNeverAskAgain() {
        openAppSettingsScreen();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        ScanQRCodeActivityPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults);
    }

    private void openAppSettingsScreen() {
        AlertDialog.Builder builder = new AlertDialog.Builder(ScanQRCodeActivity.this);
        builder.setMessage("To scan QR code, allow Smile Radar to access camera and storage.").setPositiveButton("Settings", (dialogInterface, i) -> Utils.goToSettings(ScanQRCodeActivity.this)).setNegativeButton("Not Now", (dialogInterface, i) -> dialogInterface.cancel()).show();
    }

    private void hitLinktoDoctor(final String doctorID, final boolean convertion) {
        try {
            String base = doctorID;
            if (convertion) {
                byte[] base64 = Base64.decode(doctorID, Base64.DEFAULT);
                base = new String(base64, "UTF8");
            }
            final String value = base;

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("action", "1");
            jsonObject.put("doctorId", base);
            jsonObject.put("patientId", sharedPref.getPatientID());
            Log.e("Scan QR Params", jsonObject.toString());

            new LinkToDoctorAsynctask(ScanQRCodeActivity.this, new LoginNetworkInterface() {
                @Override
                public void gPlusLoginResponse(ResponseModel response) {
                    if (response != null && response.getStatus() == 1) {
                        if (convertion) {
                            sharedPref.setDoctorID(value);
                        } else {
                            sharedPref.setDoctorID(doctorID);
                        }
                        Intent send = new Intent(ScanQRCodeActivity.this, HomeActivity.class);
                        startActivity(send);
                        finish();
                    }
                    if (response != null && response.getStatus() == 2) {
                        if (convertion) {
                            sharedPref.setDoctorID(value);
                        } else {
                            sharedPref.setDoctorID(doctorID);
                        }
                        Intent send = new Intent(ScanQRCodeActivity.this, HomeActivity.class);
                        startActivity(send);
                        finish();
                    } else if (response != null) {
                        PatientHelper.getInstance().showExitToast(ScanQRCodeActivity.this, response.getMessage());
                    } else {
                        PatientHelper.getInstance().showExitToast(ScanQRCodeActivity.this, "Network Error!");
                    }
                }

                @Override
                public void facebookLoginResponse(ResponseModel response) {
                }

                @Override
                public void SmileLoginResponse(ResponseModel response) {
                }
            }, jsonObject).execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (requestCode == 0) {
            if (resultCode == RESULT_OK) {
                // Handle successful scan
                String capturedQrValue = intent.getStringExtra("SCAN_RESULT");
                if (capturedQrValue != null)
                    hitLinktoDoctor(capturedQrValue, true);
            }
        }
    }

    @OnClick(R.id.txtDoctorLink)
    public void clickOntxtDoctorLink() {
        if (sharedPref.getDoctorID().length() > 0 && sharedPref.getDoctorName().length() > 0) {
            hitLinktoDoctor(sharedPref.getDoctorID(), false);
        }
    }
}
