package patient.com.yoursmile.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;
import patient.com.yoursmile.R;
import patient.com.yoursmile.adapter.SliderAdapter;
import patient.com.yoursmile.fragment.AlbumFragment;
import patient.com.yoursmile.fragment.DashboardFragment;
import patient.com.yoursmile.fragment.MyProfileFragment;
import patient.com.yoursmile.fragment.NavigationDrawerFragment;
import patient.com.yoursmile.fragment.NotificationFragment;
import patient.com.yoursmile.fragment.PhotoRequestFragment;
import patient.com.yoursmile.fragment.SettingFragment;
import patient.com.yoursmile.helper.Logger;
import patient.com.yoursmile.helper.PatientConstant;
import patient.com.yoursmile.helper.SharedPref;
import patient.com.yoursmile.model.PagerLimitModel;
import patient.com.yoursmile.model.PatientModel;

public class HomeActivity extends BaseActivity implements NavigationDrawerFragment.NavigationDrawerCallbacks, ViewPager.OnPageChangeListener, View.OnClickListener {

    @BindView(R.id.imgMenu)
    ImageView imgMenu;
    @BindView(R.id.txtHeader)
    TextView txtHeader;
    @BindView(R.id.btnEdit)
    Button btnEdit;
    @BindView(R.id.layHeader)
    RelativeLayout layHeader;
    @BindView(R.id.container)
    FrameLayout container;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;
    @BindView(R.id.imgBig)
    ImageView imgBig;
    @BindView(R.id.imgBigCross)
    ImageView imgBigCross;
    @BindView(R.id.layImage)
    RelativeLayout layImage;
    @BindView(R.id.pager)
    ViewPager mPager;
    NavigationDrawerFragment mNavigationDrawerFragment;
    private ProgressDialog mProgressDialog;
    private SharedPref sharedPref;
    private boolean isViewPagerVisible = false;
    private Bundle morphBundle;
    private SharedPref pref;
    private FirebaseAnalytics mFirebaseAnalytics;
    private String mScreenName = "Home Screen";


    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private FragmentManager fragmentManager;


    private SliderAdapter mPagerAdapter;
    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // TODO Auto-generated method stub
            final String action = intent.getAction();
            sendAction(action);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ArrayList<PagerLimitModel> pagerList = new ArrayList<>();
        pref = SharedPref.getInstance(HomeActivity.this);
        String mUserName = pref.getPatientID();

// Obtain the FirebaseAnalytics instance.
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
// Obtain the FirebaseAnalytics instance.
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        Bundle bundle1 = new Bundle();
        bundle1.putString(FirebaseAnalytics.Param.ITEM_ID, mUserName);
//        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, name);
        bundle1.putString(FirebaseAnalytics.Param.SOURCE, mScreenName);
        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle1);


        mPager.setOffscreenPageLimit(0);
        mPagerAdapter = new SliderAdapter(getSupportFragmentManager(), pagerList);
        mPager.setAdapter(mPagerAdapter);
        mPager.setCurrentItem(0, false);
        mPager.addOnPageChangeListener(this);


        sharedPref = SharedPref.getInstance(HomeActivity.this);

        mProgressDialog = new ProgressDialog(HomeActivity.this);
        mProgressDialog.setTitle(getString(R.string.app_name));
        mProgressDialog.setMessage("Please wait...");

        FacebookSdk.sdkInitialize(getApplicationContext());


        mNavigationDrawerFragment = (NavigationDrawerFragment) getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(R.id.navigation_drawer, drawerLayout);

        imgMenu.setOnClickListener(this);
        imgBigCross.setOnClickListener(this);

        onNavigationDrawerItemSelected(4);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null && bundle.containsKey("Action")) {
            String action = bundle.getString("Action");
            sendAction(action);
        }
    }

    @Override
    protected int getMainLayout() {
        return R.layout.activity_home;
    }

    @Override
    public void initializePresenter() {

    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null && bundle.containsKey("Action")) {
            String action = bundle.getString("Action");
            sendAction(action);
        }
    }

    public void openViewPager(int position, ArrayList<PagerLimitModel> List) {
        mPagerAdapter = new SliderAdapter(getSupportFragmentManager(), List);
        mPager.setAdapter(mPagerAdapter);
        mPager.setVisibility(View.VISIBLE);
        isViewPagerVisible = true;

        if (List != null && List.size() > position) {
            mPager.setCurrentItem(position);
        } else {
            mPager.setCurrentItem(0);
        }
    }

    private void closeViewPager() {
        isViewPagerVisible = false;
        mPager.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onBackPressed() {
        if (isViewPagerVisible)
            closeViewPager();
        else
            super.onBackPressed();
    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        // update the main content by replacing fragments

        if (btnEdit != null)
            btnEdit.setVisibility(View.GONE);
        switch (position) {
            case 0: // My Profile
                if (txtHeader != null) {
                    MyProfileFragment myProfileFragment = new MyProfileFragment();
                    txtHeader.setText(getString(R.string.my_profile));
                    fragmentManager = getSupportFragmentManager();
                    fragmentManager.beginTransaction()
                            .replace(R.id.container, myProfileFragment)
                            .commit();
                }
                break;

            case 1: // Doctor Profile
                if (txtHeader != null) {
                    DashboardFragment dashboardFragment = new DashboardFragment();
                    fragmentManager = getSupportFragmentManager();
                    txtHeader.setText(getString(R.string.doctor_info));
                    fragmentManager.beginTransaction()
                            .replace(R.id.container, dashboardFragment)
                            .commit();
                }
                break;

            case 2: // Notifications
                NotificationFragment notificationFragment = new NotificationFragment();
                txtHeader.setText(getString(R.string.notifications));
                fragmentManager = getSupportFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.container, notificationFragment)
                        .commit();
                break;

            case 3: // Album Screen
                AlbumFragment albumFragment = new AlbumFragment();
                txtHeader.setText(getString(R.string.album));
                fragmentManager = getSupportFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.container, albumFragment)
                        .commit();
                break;

            case 4: // Photo Request
                if (txtHeader != null) {
                    txtHeader.setText(getString(R.string.photo_request));
                    PhotoRequestFragment photoRequestFragment = new PhotoRequestFragment();
                    fragmentManager = getSupportFragmentManager();
                    fragmentManager.beginTransaction()
                            .replace(R.id.container, photoRequestFragment)
                            .commit();
                }
                break;

            case 5:
                if (morphBundle != null) {
                    Intent intent = new Intent(HomeActivity.this, MrophActivity.class);
                    intent.putExtras(morphBundle);
                    startActivity(intent);
                }
                break;

            case 6: // Settings
                txtHeader.setText(getString(R.string.settings));
                SettingFragment settingFragment = new SettingFragment();
                fragmentManager = getSupportFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.container, settingFragment)
                        .commit();
                break;

            case 7: // Log Out
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(HomeActivity.this);
                alertDialog.setTitle("LOG OUT ");
                alertDialog.setMessage("Are you sure want to log out ?");
                alertDialog.setCancelable(false);

                alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        mProgressDialog.show();

                        if (pref.getLoginType().equalsIgnoreCase(PatientConstant.G_LOGIN)) {
                            logout();

                        } else if (pref.getLoginType().equalsIgnoreCase(PatientConstant.FB_LOGIN)) {
                            LoginManager.getInstance().logOut();
                            mProgressDialog.dismiss();
                            logout();
                        } else {
                            mProgressDialog.dismiss();
                            logout();
                        }
                    }
                });

                alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                alertDialog.show();
                break;
        }
    }

    public void showEditProfile() {
        if (btnEdit != null)
            btnEdit.setVisibility(View.VISIBLE);
    }

    private void logout() {
        sharedPref.setLoggedIn(false);
        sharedPref.setPrefClear();
        mProgressDialog.dismiss();
        Intent intent = new Intent(HomeActivity.this, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);
        finish();
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @OnClick(R.id.btnEdit)
    public void onButtonEditClick() {
        Fragment fragment = fragmentManager.findFragmentById(R.id.container);
        if (fragment != null && fragment instanceof MyProfileFragment) {
            if (((MyProfileFragment) fragment).patientModel != null) {
                Intent intent = new Intent(HomeActivity.this, MyProfileActivity.class);
                intent.putExtra("Model", ((MyProfileFragment) fragment).patientModel);
                startActivityForResult(intent, 1002);
            }
        }
    }

    public void showImage(String imageURl) {
        layImage.setVisibility(View.VISIBLE);
        if (imageURl != null && imageURl.length() > 0) {
            Logger.error("Img", imageURl);
            Picasso.with(HomeActivity.this)
                    .load(imageURl)
                    .into(imgBig);
        }
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.imgBigCross:
                layImage.setVisibility(View.GONE);
                imgBig.setImageBitmap(null);
                break;

            case R.id.imgMenu:
                if (mNavigationDrawerFragment != null)
                    mNavigationDrawerFragment.handleMenuButton();
                break;

        }
    }

    public void setSideTreatment(Bundle bundle) {
        morphBundle = bundle;
        if (mNavigationDrawerFragment != null) {
            if (bundle != null) {
                mNavigationDrawerFragment.sideMenuListAdapter.setTreatmentBoolean(true);
            } else {
                mNavigationDrawerFragment.sideMenuListAdapter.setTreatmentBoolean(false);
            }
        }
    }

    public void callTakePhoto() {
        //TODO: send analytics action here
        Intent intent = new Intent(HomeActivity.this, TakePhotoActivity.class);
        startActivityForResult(intent, 1001);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == 1001) {
                Fragment fragment = fragmentManager.findFragmentById(R.id.container);
                if (fragment != null && fragment instanceof PhotoRequestFragment) {
                    fragment.onActivityResult(requestCode, resultCode, data);
                }
            }
            if (requestCode == 1002) {
                if (data != null) {
                    PatientModel model = (PatientModel) data.getSerializableExtra("Model");
                    Fragment fragment = fragmentManager.findFragmentById(R.id.container);
                    if (fragment != null && fragment instanceof MyProfileFragment) {
                        ((MyProfileFragment) fragment).setProfile(model);
                    }
                }
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        IntentFilter filter = new IntentFilter();
        filter.addAction(PatientConstant.NOTIFICATION_ACCEPTED);
        filter.addAction(PatientConstant.NOTIFICATION_REJECTED);
        filter.addAction(PatientConstant.NOTIFICATION_SOS);
        filter.addAction(PatientConstant.NOTIFICATION_NEWALBUM);
        filter.addAction(PatientConstant.NOTIFICATION_TREATMENT_COMP);
        filter.addAction(PatientConstant.NOTIFICATION_MORPH_COMP);
        LocalBroadcastManager.getInstance(HomeActivity.this).registerReceiver(receiver, filter);
    }

    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(HomeActivity.this).unregisterReceiver(receiver);
    }

    public void sendAction(String action) {
        if (action.equalsIgnoreCase(PatientConstant.NOTIFICATION_ACCEPTED)) {
            Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.container);
            if (fragment instanceof PhotoRequestFragment) {
                pref.setTimeStamp("");
                onNavigationDrawerItemSelected(4);
            } else if (fragment instanceof DashboardFragment) {
                onNavigationDrawerItemSelected(0);
            }
        } else if (action.equalsIgnoreCase(PatientConstant.NOTIFICATION_SOS)) {
            pref.setTimeStamp("");
            Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.container);
            if (fragment instanceof PhotoRequestFragment) {
                onNavigationDrawerItemSelected(4);
            }
        } else if (action.equalsIgnoreCase(PatientConstant.NOTIFICATION_NEWALBUM)) {
            pref.setTimeStamp("");
            Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.container);
            if (fragment instanceof PhotoRequestFragment) {
                onNavigationDrawerItemSelected(4);
            }
        } else if (action.equalsIgnoreCase(PatientConstant.NOTIFICATION_TREATMENT_COMP)) {
            Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.container);
            onNavigationDrawerItemSelected(0);

        } else if (action.equalsIgnoreCase(PatientConstant.NOTIFICATION_MORPH_COMP)) {
            Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.container);
            if (fragment instanceof PhotoRequestFragment) {
                onNavigationDrawerItemSelected(0);
            }
        }
    }

    ;

    public void rearrangeMenu() {
        mNavigationDrawerFragment.menuAdapterNotify();
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            return inflater.inflate(R.layout.fragment_home, container, false);
        }
    }
}
