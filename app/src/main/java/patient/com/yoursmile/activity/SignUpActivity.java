package patient.com.yoursmile.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import patient.com.yoursmile.R;
import patient.com.yoursmile.helper.Blur;
import patient.com.yoursmile.helper.CircleTransform;
import patient.com.yoursmile.helper.Logger;
import patient.com.yoursmile.helper.PatientConstant;
import patient.com.yoursmile.helper.PatientHelper;
import patient.com.yoursmile.helper.SharedPref;
import patient.com.yoursmile.model.LoginModel;
import patient.com.yoursmile.model.ResponseModel;
import patient.com.yoursmile.network.LoginNetworkInterface;
import patient.com.yoursmile.network.SignUpAsync;

public class SignUpActivity extends Activity {

    private static final String TAG = SignUpActivity.class.getSimpleName();

    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 101;
    private static final String PROPERTY_REG_ID = "REG_ID";
    private static final String PROPERTY_APP_VERSION = "App Version";
    private static int REQUEST_CAMERA = 101, SELECT_FILE = 102;

    @BindView(R.id.edDescription)
    EditText edDescription;
    @BindView(R.id.edConPassword)
    EditText edConPassword;
    @BindView(R.id.edPassword)
    EditText edPassword;
    @BindView(R.id.edContact)
    EditText edContact;
    @BindView(R.id.edEmail)
    EditText edEmail;
    @BindView(R.id.edAddress)
    EditText edAddress;
    @BindView(R.id.edName)
    EditText edName;
    @BindView(R.id.imgBanner)
    ImageView imgBanner;
    @BindView(R.id.imgProfile)
    ImageView imgProfile;
    @BindView(R.id.imgEdit)
    ImageView imgEdit;
    @BindView(R.id.btnSubmit)
    Button btnSubmit;
    @BindView(R.id.imgBack)
    ImageView imgBack;

    private String imagePath = "";
    private SharedPref sharedPref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup_);
        ButterKnife.bind(this);
        sharedPref = SharedPref.getInstance(SignUpActivity.this);
        setupForPush();

        imgBanner.setVisibility(View.INVISIBLE);

    }

    public void saveToFile(String filename,Bitmap bmp) {
        try {
            FileOutputStream out = new FileOutputStream(filename);
            bmp.compress(Bitmap.CompressFormat.PNG, 100, out);
            out.flush();
            out.close();
        } catch(Exception e) {}
    }

    Target target = new Target() {
        @Override
        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
            if(bitmap!=null) {
                imagePath = PatientHelper.createNewFile(SignUpActivity.this);
                try {
                    Bitmap bitmap1 = Blur.fastblur(SignUpActivity.this, bitmap, 1);
                    saveToFile(imagePath, bitmap1);
                    imgBanner.setImageBitmap(bitmap1);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }
        @Override
        public void onBitmapFailed(Drawable errorDrawable) {
        }
        @Override
        public void onPrepareLoad(Drawable placeHolderDrawable) {
        }
    };

    @OnClick(R.id.imgEdit)
    public void clickOnEditImage()
    {
        CharSequence[] demoItems;

        if(imagePath!=null && imagePath.length()>0)
            demoItems = new CharSequence[]{"Take Photo", "Choose from Gallery", "Remove existing", "Cancel"};
        else
            demoItems = new CharSequence[]{"Take Photo", "Choose from Gallery", "Cancel"};

        final CharSequence[] items = demoItems;

        AlertDialog.Builder builder = new AlertDialog.Builder(SignUpActivity.this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo")) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(intent, REQUEST_CAMERA);
                } else if (items[item].equals("Choose from Gallery")) {
                    Intent intent = new Intent(
                            Intent.ACTION_PICK,
                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/*");
                    startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }else if (items[item].equals("Remove existing")) {
                    dialog.dismiss();
                    imagePath = "";
                    imgProfile.setImageResource(R.drawable.profileplaceholder);
                    imgBanner.setBackgroundColor(getResources().getColor(R.color.banner_bg));

                }
            }
        });
        builder.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_CAMERA) {
                Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
                if(thumbnail!=null) {
                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                    thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);

                    File destination = new File(Environment.getExternalStorageDirectory(), System.currentTimeMillis() + ".jpg");
                    FileOutputStream fo;
                    try {
                        destination.createNewFile();
                        fo = new FileOutputStream(destination);
                        fo.write(bytes.toByteArray());
                        fo.close();
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    imagePath = destination.getAbsolutePath();
                    Picasso.with(SignUpActivity.this).load(destination).fit()
                            .transform(new CircleTransform()).into(imgProfile);

                    Bitmap bitmap1= Blur.fastblur(SignUpActivity.this, thumbnail, 1);
                    imgBanner.setImageBitmap(bitmap1);

                }
            } else if (requestCode == SELECT_FILE) {
                Uri selectedImageUri = data.getData();
                String[] projection = {MediaStore.MediaColumns.DATA};
                Cursor cursor = managedQuery(selectedImageUri, projection, null, null,null);
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
                cursor.moveToFirst();
                String selectedImagePath = cursor.getString(column_index);
                Bitmap bm;
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeFile(selectedImagePath, options);
                final int REQUIRED_SIZE = 200;
                int scale = 1;
                while (options.outWidth / scale / 2 >= REQUIRED_SIZE
                        && options.outHeight / scale / 2 >= REQUIRED_SIZE)
                    scale *= 2;
                options.inSampleSize = scale;
                options.inJustDecodeBounds = false;
                bm = BitmapFactory.decodeFile(selectedImagePath, options);
                imagePath = selectedImagePath;
                Picasso.with(SignUpActivity.this).load("file://"+selectedImagePath).fit()
                        .transform(new CircleTransform()).into(imgProfile);

                Bitmap bitmap1= Blur.fastblur(SignUpActivity.this, bm, 1);
                imgBanner.setImageBitmap(bitmap1);
            }
        }
    }


    @OnClick(R.id.btnSubmit)
    public void setProfile()
    {
        String name = edName.getText().toString().trim();
        String address = edAddress.getText().toString().trim();
        String emailString = edEmail.getText().toString().trim();
        String contactString = edContact.getText().toString().trim();
        String passString =edPassword.getText().toString().trim();
        String passCString =edConPassword.getText().toString().trim();
//        String desString = edDescription.getText().toString().trim();



        if(name.length()==0)
        {
            PatientHelper.getInstance().showExitToast(SignUpActivity.this,"Please enter name");
        }
        else if(emailString.length()==0){
            PatientHelper.getInstance().showExitToast(SignUpActivity.this,"Please enter email");
        }else if(!PatientHelper.getInstance().emailValidate(emailString)){
            PatientHelper.getInstance().showExitToast(SignUpActivity.this,"Please enter valid email");
        }
        else if(passString.length()==0){
            PatientHelper.getInstance().showExitToast(SignUpActivity.this,"Please enter password");
        }
        else if(passString.length()<8){
            PatientHelper.getInstance().showExitToast(SignUpActivity.this,"Please enter password at least 8 characters");
        }
        else if(passCString.length()==0){
            PatientHelper.getInstance().showExitToast(SignUpActivity.this,"Please enter confirm password");
        }     else if(passCString.length()<8){
            PatientHelper.getInstance().showExitToast(SignUpActivity.this,"Please enter confirm password at least 8 characters");
        }
        else if(!passString.equals(passCString)){
            edPassword.setText("");
            edConPassword.setText("");
            PatientHelper.getInstance().showExitToast(SignUpActivity.this, "New password and confirm password doesn't match");
        }
        else if(contactString.length()>0 && contactString.length()<7){
            PatientHelper.getInstance().showExitToast(SignUpActivity.this, "Please enter mobile number at least 7 characters");
        }
        else {
            try {

               JSONObject jsonObject = new JSONObject();
                jsonObject.put("address", address);
                jsonObject.put("contact", contactString);
                jsonObject.put("description", "");
                jsonObject.put("deviceToken", regid);
                jsonObject.put("email", emailString);
                jsonObject.put("loginOrSignup", "2");
                jsonObject.put("name", name);
                jsonObject.put("password",passString);
                jsonObject.put("platform", PatientConstant.PLATFORM);
                jsonObject.put("signupType", "1");
                jsonObject.put("socialLoginMedium", "3");
                jsonObject.put("file",imagePath);

                new SignUpAsync(SignUpActivity.this, new LoginNetworkInterface() {
                    @Override
                    public void  SmileLoginResponse(ResponseModel response) {
                        if(response!=null && response.getStatus()==1)
                        {
                            if(response.getObject()!=null && response.getObject() instanceof LoginModel) {
                                LoginModel loginModel = (LoginModel) response.getObject();
                                Logger.error("Patient Id", loginModel.getId() + "we");
                                sharedPref.setLoggedIn(true);
                                sharedPref.setLoginType(PatientConstant.S_LOGIN);
                                sharedPref.setPatientID(loginModel.getId());
                                sharedPref.setAccessToken(loginModel.getAccessToken());
                                String doctorID = loginModel.getDoctorId();
                                String doctorName = loginModel.getDoctorname();

                                if (doctorID != null && !doctorID.equalsIgnoreCase("null"))
                                    sharedPref.setDoctorID(doctorID);

                                if (doctorName != null && !doctorName.equalsIgnoreCase("null"))
                                    sharedPref.setDoctorName(doctorName);

                                setResult(RESULT_OK);

                                Intent send = new Intent(SignUpActivity.this, ScanQRCodeActivity.class);
                                startActivity(send);
                                finish();
                            }
                        }
                        else if(response!=null && response.getMessage()!=null)
                        {
                            setupForPush();
                            PatientHelper.getInstance().showExitToast(SignUpActivity.this,response.getMessage());
                        }
                        else{
                            PatientHelper.getInstance().showExitToast(SignUpActivity.this,"Network Error!");
                        }
                    }

                    @Override
                    public void facebookLoginResponse(ResponseModel response) {

                    }

                    @Override
                    public void gPlusLoginResponse(ResponseModel response) {

                    }
                }, jsonObject).execute(PatientConstant.PATIENT_URL);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @OnClick(R.id.imgBack)
    public void closeActivity(){
        finish();
    }


    private GoogleCloudMessaging gcm;
    private String regid=null;
    public void setupForPush() {
        if (checkPlayServices()) {
            gcm = GoogleCloudMessaging.getInstance(this);
            regid = getRegistrationId(SignUpActivity.this);
            if (regid.isEmpty())
            {
                registerInBackground();
            }
        } else {
            Log.i(TAG, "No valid Google Play Services APK found.");
        }
    }
    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Log.i(TAG, "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }


    /**
     * Gets the current registration ID for application on GCM service.
     * <p>
     * If result is empty, the app needs to register.
     *
     * @return registration ID, or empty string if there is no existing
     *         registration ID.
     */
    private String getRegistrationId(Context context) {
        final SharedPreferences prefs = getGCMPreferences();
        String registrationId = prefs.getString(PROPERTY_REG_ID, "");
        if (registrationId.isEmpty()) {
            Log.i(TAG, "Registration not found.");
            return "";
        }
        // Check if app was updated; if so, it must clear the registration ID
        // since the existing regID is not guaranteed to work with the new
        // app version.
        int registeredVersion = prefs.getInt(PROPERTY_APP_VERSION, Integer.MIN_VALUE);
        int currentVersion = getAppVersion(context);
        if (registeredVersion != currentVersion) {
            Log.i(TAG, "App version changed.");
            return "";
        }
        return registrationId;
    }


    /**
     * @return Application's {@code SharedPreferences}.
     */
    private SharedPreferences getGCMPreferences() {
        // This sample app persists the registration ID in shared preferences, but
        // how you store the regID in your app is up to you.
        return getSharedPreferences("GCM_PREF",
                Context.MODE_PRIVATE);
    }

    private void clearGCMPref() {
        SharedPreferences.Editor editor = getGCMPreferences().edit();
        editor.clear();
        editor.commit();
    }

    /**
     * @return Application's version code from the {@code PackageManager}.
     */
    private static int getAppVersion(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            // should never happen
            throw new RuntimeException("Could not get package name: " + e);
        }
    }

    /**
     * Registers the application with GCM servers asynchronously.
     * <p>
     * Stores the registration ID and app versionCode in the application's
     * shared preferences.
     */
    private void registerInBackground() {
        new GCMUpdateAsync().execute();
    }

    class GCMUpdateAsync extends AsyncTask<Void, Void, String> {
        @Override
        protected String doInBackground(Void... params) {
            String msg = "";
            try {
                if (gcm == null)
                {
                    gcm = GoogleCloudMessaging.getInstance(SignUpActivity.this);
                }
                regid = gcm.register(PatientConstant.SENDER_ID);
                msg = "Device registered, registration ID=" + regid;

                // You should send the registration ID to your server over HTTP,
                // so it can use GCM/HTTP or CCS to send messages to your app.
                // The request to your server should be authenticated if your app
                // is using accounts.
                sendRegistrationIdToBackend();

                // For this demo: we don't need to send it because the device
                // will send upstream messages to a server that echo back the
                // message using the 'from' address in the message.

                // Persist the regID - no need to register again.
                storeRegistrationId(SignUpActivity.this, regid);
            } catch (IOException ex) {
                msg = "Error :" + ex.getMessage();
                // If there is an error, don't just keep trying to register.
                // Require the user to click a button again, or perform
                // exponential back-off.
            }
            return msg;
        }
        @Override
        protected void onPostExecute(String msg) {
            //	 mDisplay.append(msg + "\n");
            //				 sendPush();
        }
    }

    /**
     * Sends the registration ID to your server over HTTP, so it can use GCM/HTTP
     * or CCS to send messages to your app. Not needed for this demo since the
     * device sends upstream messages to a server that echoes back the message
     * using the 'from' address in the message.
     */
    private void sendRegistrationIdToBackend() {

    }

    /**
     * Stores the registration ID and app versionCode in the application's
     * {@code SharedPreferences}.
     *
     * @param context application's context.
     * @param regId registration ID
     */
    private void storeRegistrationId(Context context, String regId) {
        final SharedPreferences prefs = getGCMPreferences();
        int appVersion = getAppVersion(context);
        Log.i(TAG, "Saving regId on app version " + appVersion);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(PROPERTY_REG_ID, regId);
        editor.commit();
    }

}
