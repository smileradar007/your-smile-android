package patient.com.yoursmile.model;

import java.util.List;

/**
 * Created by vineet on 13/9/15.
 */
public class ResponseModel {

    private int status;
    private String message;
    private Object object;
    private int other1;
    private List<Object> objectList;


    public ResponseModel(int status,String message)
    {
        this.status = status;
        this.message = message;
    }


    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
       this.message = message;
    }

    public Object getObject() {
        return object;
    }

    public void setObject(Object object) {
        this.object = object;
    }

    public int getOther1() {
        return other1;
    }

    public void setOther1(int other1) {
        this.other1 = other1;
    }

    public List<Object> getObjectList() {
        return objectList;
    }

    public void setObjectList(List<Object> objectList) {
        this.objectList = objectList;
    }
}
