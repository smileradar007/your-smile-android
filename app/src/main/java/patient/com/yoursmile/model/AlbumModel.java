package patient.com.yoursmile.model;

import java.io.Serializable;

/**
 * Created by vineet on 31/10/15.
 */
public class AlbumModel implements Serializable {

     private String AlbumID;
     private String LeftImage;
     private String CenterImage;
     private String RightImage;
     private String AlbumStatus;
     private String LeftImageStatus;
     private String RightImageStaus;
     private String CenterImageStatus;
     private String LastUpdated;
     private String CreatedDate;

    public AlbumModel(String albumID, String leftImage, String centerImage, String rightImage, String albumStatus, String leftImageStatus, String rightImageStaus, String centerImageStatus, String lastUpdated, String createdDate) {
        AlbumID = albumID;
        LeftImage = leftImage;
        CenterImage = centerImage;
        RightImage = rightImage;
        AlbumStatus = albumStatus;
        LeftImageStatus = leftImageStatus;
        RightImageStaus = rightImageStaus;
        CenterImageStatus = centerImageStatus;
        LastUpdated = lastUpdated;
        CreatedDate = createdDate;
    }

    public String getLeftImage() {
        return LeftImage;
    }

    public void setLeftImage(String leftImage) {
        LeftImage = leftImage;
    }

    public String getAlbumID() {
        return AlbumID;
    }

    public void setAlbumID(String albumID) {
        AlbumID = albumID;
    }

    public String getCenterImage() {
        return CenterImage;
    }

    public void setCenterImage(String centerImage) {
        CenterImage = centerImage;
    }

    public String getRightImage() {
        return RightImage;
    }

    public void setRightImage(String rightImage) {
        RightImage = rightImage;
    }

    public String getAlbumStatus() {
        return AlbumStatus;
    }

    public void setAlbumStatus(String albumStatus) {
        AlbumStatus = albumStatus;
    }

    public String getLeftImageStatus() {
        return LeftImageStatus;
    }

    public void setLeftImageStatus(String leftImageStatus) {
        LeftImageStatus = leftImageStatus;
    }

    public String getRightImageStaus() {
        return RightImageStaus;
    }

    public void setRightImageStaus(String rightImageStaus) {
        RightImageStaus = rightImageStaus;
    }

    public String getCenterImageStatus() {
        return CenterImageStatus;
    }

    public void setCenterImageStatus(String centerImageStatus) {
        CenterImageStatus = centerImageStatus;
    }

    public String getLastUpdated() {
        return LastUpdated;
    }

    public void setLastUpdated(String lastUpdated) {
        LastUpdated = lastUpdated;
    }

    public String getCreatedDate() {
        return CreatedDate;
    }

    public void setCreatedDate(String createdDate) {
        CreatedDate = createdDate;
    }
}
