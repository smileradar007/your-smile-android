package patient.com.yoursmile.model;

import java.io.Serializable;

/**
 *
 * Created by vineet on 30/10/15.
 *
 *
 */

public class NotificationModel implements Serializable{

    private String NotificationID;
    private String Sender;
    private String Message;
    private String  ObjectID;
    private String CreatedDate;
    private String  Status;
    private String Type;

    public NotificationModel(String notificationID, String sender, String message, String objectID, String createdDate, String status, String type) {
        NotificationID = notificationID;
        Sender = sender;
        Message = message;
        ObjectID = objectID;
        CreatedDate = createdDate;
        Status = status;
        Type = type;
    }


    public String getNotificationID() {
        return NotificationID;
    }

    public void setNotificationID(String notificationID) {
        NotificationID = notificationID;
    }

    public String getSender() {
        return Sender;
    }

    public void setSender(String sender) {
        Sender = sender;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public String getObjectID() {
        return ObjectID;
    }

    public void setObjectID(String objectID) {
        ObjectID = objectID;
    }

    public String getCreatedDate() {
        return CreatedDate;
    }

    public void setCreatedDate(String createdDate) {
        CreatedDate = createdDate;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }
}
