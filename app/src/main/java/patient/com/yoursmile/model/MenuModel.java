package patient.com.yoursmile.model;

/**
 * Created by vineet on 11/10/15.
 */
public class MenuModel {

   private int imgResource,textResource;

    public MenuModel(int imgResource, int textResource) {
        this.imgResource = imgResource;
        this.textResource = textResource;
    }

    public int getImgResource() {
        return imgResource;
    }

    public void setImgResource(int imgResource) {
        this.imgResource = imgResource;
    }

    public int getTextResource() {
        return textResource;
    }

    public void setTextResource(int textResource) {
        this.textResource = textResource;
    }
}
