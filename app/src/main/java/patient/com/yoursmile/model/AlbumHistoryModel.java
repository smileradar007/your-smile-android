package patient.com.yoursmile.model;

/**
 * Created by vineet on 16/12/15.
 */
public class AlbumHistoryModel {

     private String AlbumID;
    private String LeftImage;
    private String CenterImage;
    private String RightImage;
    private String AlbumStatus;
    private String LeftImageStatus;
    private String RightImageStatus;
    private String CenterImageStatus;
    private String LastUpdated;
    private String CreatedDate;

    public AlbumHistoryModel(String albumID, String leftImage, String centerImage, String rightImage, String leftImageStatus, String albumStatus, String rightImageStatus, String centerImageStatus, String lastUpdated, String createdDate) {
        AlbumID = albumID;
        LeftImage = leftImage;
        CenterImage = centerImage;
        RightImage = rightImage;
        LeftImageStatus = leftImageStatus;
        AlbumStatus = albumStatus;
        RightImageStatus = rightImageStatus;
        CenterImageStatus = centerImageStatus;
        LastUpdated = lastUpdated;
        CreatedDate = createdDate;
    }


    public String getAlbumID() {
        return AlbumID;
    }

    public void setAlbumID(String albumID) {
        AlbumID = albumID;
    }

    public String getLeftImage() {
        return LeftImage;
    }

    public void setLeftImage(String leftImage) {
        LeftImage = leftImage;
    }

    public String getCenterImage() {
        return CenterImage;
    }

    public void setCenterImage(String centerImage) {
        CenterImage = centerImage;
    }

    public String getRightImage() {
        return RightImage;
    }

    public void setRightImage(String rightImage) {
        RightImage = rightImage;
    }

    public String getAlbumStatus() {
        return AlbumStatus;
    }

    public void setAlbumStatus(String albumStatus) {
        AlbumStatus = albumStatus;
    }

    public String getLeftImageStatus() {
        return LeftImageStatus;
    }

    public void setLeftImageStatus(String leftImageStatus) {
        LeftImageStatus = leftImageStatus;
    }

    public String getRightImageStatus() {
        return RightImageStatus;
    }

    public void setRightImageStatus(String rightImageStatus) {
        RightImageStatus = rightImageStatus;
    }

    public String getCenterImageStatus() {
        return CenterImageStatus;
    }

    public void setCenterImageStatus(String centerImageStatus) {
        CenterImageStatus = centerImageStatus;
    }

    public String getLastUpdated() {
        return LastUpdated;
    }

    public void setLastUpdated(String lastUpdated) {
        LastUpdated = lastUpdated;
    }

    public String getCreatedDate() {
        return CreatedDate;
    }

    public void setCreatedDate(String createdDate) {
        CreatedDate = createdDate;
    }
}
