package patient.com.yoursmile.model;

/**
 * Created by vineet on 14/12/15.
 */
public class PhotoRequestModel {

    int albumRequired;
    int SOS;
    int counter;
    int treetment;

    public PhotoRequestModel(int albumRequired, int SOS, int counter, int treetment) {
        this.albumRequired = albumRequired;
        this.SOS = SOS;
        this.counter = counter;
        this.treetment = treetment;
    }

    public int getAlbumRequired() {
        return albumRequired;
    }

    public void setAlbumRequired(int albumRequired) {
        this.albumRequired = albumRequired;
    }

    public int getSOS() {
        return SOS;
    }

    public void setSOS(int SOS) {
        this.SOS = SOS;
    }

    public int getCounter() {
        return counter;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }

    public int getTreetment() {
        return treetment;
    }

    public void setTreetment(int treetment) {
        this.treetment = treetment;
    }
}
