package patient.com.yoursmile.model;

/**
 * Created by vineet on 13/9/15.
 */
public class SettingModel {

    private int sound,pushNotification;

public SettingModel(int sound,int push){

    this.sound = sound;
    this.pushNotification = push;
}

    public int getSound() {
        return sound;
    }

    public void setSound(int sound) {
        this.sound = sound;
    }

    public int getPushNotification() {
        return pushNotification;
    }

    public void setPushNotification(int pushNotification) {
        this.pushNotification = pushNotification;
    }
}
