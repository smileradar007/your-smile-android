package patient.com.yoursmile.model;

import java.io.Serializable;

/**
 * Created by vineet on 17/9/15.
 */
public class PatientModel implements Serializable {

    private String name;
    private String address;
    private String email;
    private String contact;
    private String profileimage;
    private String description;
    private String demo;



    public PatientModel(String name, String address, String email, String contact, String profileimage, String description, String demo) {
        this.name = name;
        this.address = address;
        this.email = email;
        this.contact = contact;
        this.profileimage = profileimage;
        this.description = description;
        this.demo = demo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getProfileimage() {
        return profileimage;
    }

    public void setProfileimage(String profileimage) {
        this.profileimage = profileimage;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDemo() {
        return demo;
    }

    public void setDemo(String demo) {
        this.demo = demo;
    }
}
