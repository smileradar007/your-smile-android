package patient.com.yoursmile.model;

import java.io.Serializable;

/**
 * Created by vineet on 10/1/16.
 */
public class PagerLimitModel implements Serializable{

    private String imageURl;
    private String timeStamp;

    public PagerLimitModel(String imageURl, String timeStamp) {
        this.imageURl = imageURl;
        this.timeStamp = timeStamp;
    }


    public String getImageURl() {
        return imageURl;
    }

    public void setImageURl(String imageURl) {
        this.imageURl = imageURl;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }
}


