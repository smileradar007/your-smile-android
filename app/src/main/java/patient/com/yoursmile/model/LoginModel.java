package patient.com.yoursmile.model;

import patient.com.yoursmile.helper.Logger;

public class LoginModel {

      private int status;
      private int socialLoginMedium;
      private String message;
      private String socialId,name,profileimage,email,id,contact,deviceToken,accessToken;

    private String doctorname;
    private String doctorId;

   public LoginModel(){

    }

   public LoginModel(int status,
                     int socialLoginMedium,
                     String message,
                     String socialId,
                     String name,
                     String profileimage,
                     String email,
                     String id,
                     String contact,
                     String deviceToken,
                     String accessToken,
                     String doctorname,String doctorId)
   {
       Logger.error("LoginModel",id+ "we");
       this.status = status;
       this.socialLoginMedium = socialLoginMedium;
       this.message = message;
       this.socialId = socialId;
       this.name = name;
       this.deviceToken = deviceToken;
       this.profileimage = profileimage;
       this.email = email;
       this.contact = contact;
       this.profileimage = profileimage;
       this.accessToken = accessToken;
       this.id = id;
       this.doctorname = doctorname;
       this.doctorId = doctorId;
   }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getSocialLoginMedium() {
        return socialLoginMedium;
    }

    public void setSocialLoginMedium(int socialLoginMedium) {
        this.socialLoginMedium = socialLoginMedium;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getSocialId() {
        return socialId;
    }

    public void setSocialId(String socialId) {
        this.socialId = socialId;
    }

    public String getProfileimage() {
        return profileimage;
    }

    public void setProfileimage(String profileimage) {
        this.profileimage = profileimage;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getDeviceToken() {
        return deviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getDoctorname() {
        return doctorname;
    }

    public void setDoctorname(String doctorname) {
        this.doctorname = doctorname;
    }

    public String getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(String doctorId) {
        this.doctorId = doctorId;
    }
}
