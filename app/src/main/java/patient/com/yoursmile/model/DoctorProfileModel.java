package patient.com.yoursmile.model;

/**
 * Created by vineet on 16/9/15.
 */
public class DoctorProfileModel {

    private String address;
    private String description;
    private String email;
    private String emmergency_telephone;
    private String fbpageurl;
    private String latitude;
    private String longitude;
    private String name;
    private String practice_name;
    private String practice_telephone;
    private String profileimage;
    private String twhandle;
    private String websiteurl;
    private String morph;
    private String qualification;


    public DoctorProfileModel(String address, String description, String email, String emmergency_telephone, String fbpageurl, String latitude, String longitude, String name, String practice_name, String practice_telephone, String profileimage, String twhandle, String websiteurl
                             ,String Morph,String qualification) {
        this.address = address;
        this.description = description;
        this.email = email;
        this.emmergency_telephone = emmergency_telephone;
        this.fbpageurl = fbpageurl;
        this.latitude = latitude;
        this.longitude = longitude;
        this.name = name;
        this.practice_name = practice_name;
        this.practice_telephone = practice_telephone;
        this.profileimage = profileimage;
        this.twhandle = twhandle;
        this.websiteurl = websiteurl;
        this.morph = Morph;
        this.qualification = qualification;

    }


    public String getQualification() {
        return qualification;
    }

    public void setQualification(String qualification) {
        this.qualification = qualification;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmmergency_telephone() {
        return emmergency_telephone;
    }

    public void setEmmergency_telephone(String emmergency_telephone) {
        this.emmergency_telephone = emmergency_telephone;
    }

    public String getFbpageurl() {
        return fbpageurl;
    }

    public void setFbpageurl(String fbpageurl) {
        this.fbpageurl = fbpageurl;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPractice_name() {
        return practice_name;
    }

    public void setPractice_name(String practice_name) {
        this.practice_name = practice_name;
    }

    public String getPractice_telephone() {
        return practice_telephone;
    }

    public void setPractice_telephone(String practice_telephone) {
        this.practice_telephone = practice_telephone;
    }

    public String getProfileimage() {
        return profileimage;
    }

    public void setProfileimage(String profileimage) {
        this.profileimage = profileimage;
    }

    public String getTwhandle() {
        return twhandle;
    }

    public void setTwhandle(String twhandle) {
        this.twhandle = twhandle;
    }

    public String getWebsiteurl() {
        return websiteurl;
    }

    public void setWebsiteurl(String websiteurl) {
        this.websiteurl = websiteurl;
    }

    public String getMorph() {
        return morph;
    }

    public void setMorph(String morph) {
        this.morph = morph;
    }

    /*    private String albumRequired;
    private String SOS;*/
// String albumRequired,String SOS
    /*        this.SOS = SOS;
        this.albumRequired = albumRequired;*/
/*    public String getAlbumRequired() {
        return albumRequired;
    }

    public void setAlbumRequired(String albumRequired) {
        this.albumRequired = albumRequired;
    }

    public String getSOS() {
        return SOS;
    }

    public void setSOS(String SOS) {
        this.SOS = SOS;
    }*/
}
