package patient.com.yoursmile.network;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import org.json.JSONException;
import org.json.JSONObject;

import patient.com.yoursmile.helper.Logger;
import patient.com.yoursmile.model.ResponseModel;
import patient.com.yoursmile.R;

/**
 *
 * Created by vineet on 24/9/15.
 *
 */
public class SetAlbumAsyncTask extends AsyncTask<String,Void,ResponseModel> {

    private Context context;
    private GenericNetworkInterface interface1;
    private ProgressDialog dialog;
    private JSONObject jsonObject;
    private static final int FB_LOGIN = 1, GPLUS_LOGIN = 2;

    public SetAlbumAsyncTask(Context context, GenericNetworkInterface interface1,JSONObject jsonObject) {
        this.context = context;
        this.interface1 = interface1;
        this.jsonObject = jsonObject;
    }

    @Override
    protected ResponseModel doInBackground(String... params) {
        // TODO Auto-generated method stub\
        try {
            String param = new NetworkConnection().uploadAlbum(context, params[0], jsonObject);
            if (param != null) {
                Logger.error("SetAlbumAsyncTask","response: "+param);
                JSONObject jsonObject = new JSONObject(param);
                int success = jsonObject.getInt("success");
                ResponseModel responseModel;
                if (success == 1) {
                    responseModel = new ResponseModel(success, jsonObject.getString("errstr"));
                    return responseModel;
                } else {
                    responseModel = new ResponseModel(success, jsonObject.getString("errstr"));
                    return responseModel;
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPreExecute() {
        // TODO Auto-generated method stub
        super.onPreExecute();
        dialog = new ProgressDialog(context);
        dialog.setTitle(context.getString(R.string.app_name));
        dialog.setMessage("Please wait...");
        dialog.show();
    }

    @Override
    protected void onPostExecute(ResponseModel result) {
        // TODO Auto-generated method stub
        super.onPostExecute(result);
        if (dialog != null)
            dialog.dismiss();
        interface1.networkResponse(result);
    }
}