package patient.com.yoursmile.network;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import patient.com.yoursmile.helper.PatientHelper;
import patient.com.yoursmile.model.NotificationModel;
import patient.com.yoursmile.model.ResponseModel;
import patient.com.yoursmile.R;

/**
 * Created by vineet on 25/10/15.
 */
public class GetNotificationListAsynctask extends AsyncTask<String,Void,ResponseModel> {
    private Context context;
    private GenericNetworkInterface interface1;
    private ProgressDialog dialog;
    private static final int FB_LOGIN=1,GPLUS_LOGIN=2;
    private boolean show;

    public GetNotificationListAsynctask(Context context,boolean show, GenericNetworkInterface interface1) {
        this.context=context;
        this.interface1 = interface1;
        this.show  = show;
    }

    @Override
    protected ResponseModel doInBackground(String... params) {
        // TODO Auto-generated method stub\
        try {
            String param = new NetworkConnection().networkHitAuthGetWithAccessTokenAndDoctor(context, params[0]);
            if (param != null) {
                JSONObject jsonObject = new JSONObject(param);
                int success = jsonObject.getInt("success");
                ResponseModel responseModel;
                if (success == 1) {
                    JSONArray jsonObject1 = new JSONArray(jsonObject.getString("data"));
                    responseModel = new ResponseModel(success, jsonObject.getString("errstr"));
                    if (jsonObject1 != null && jsonObject1.length() > 0) {
                        List<Object> objects = new ArrayList<>();
                        int len = jsonObject1.length();
                        for (int i = 0; i < len; i++) {
                            JSONObject jsonObject2 = jsonObject1.optJSONObject(i);
                            NotificationModel notificationModel = new NotificationModel(
                                    jsonObject2.getString("NotificationID"),
                                    jsonObject2.getString("Sender"),
                                    jsonObject2.getString("Message"),
                                    jsonObject2.getString("ObjectID"),
                                    PatientHelper.getDateFromString(jsonObject2.getString("CreatedDate")),
                                    jsonObject2.getString("Status"),
                                    jsonObject2.getString("Type"));
                            objects.add(notificationModel);
                        }
                        responseModel.setObjectList(objects);
                        return responseModel;
                    }else{
                        responseModel = new ResponseModel(success, jsonObject.getString("errstr"));
                        return responseModel;
                    }
                }
            }

        } catch (JSONException e)
        {
            e.printStackTrace();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return null;
    }
    @Override
    protected void onPreExecute() {
        // TODO Auto-generated method stub
        super.onPreExecute();
        dialog = new ProgressDialog(context);
        dialog.setTitle(context.getString(R.string.app_name));
        dialog.setMessage("Please wait...");
        if(show)
        dialog.show();
    }

    @Override
    protected void onPostExecute(ResponseModel result) {
        // TODO Auto-generated method stub
        super.onPostExecute(result);
        if(dialog!=null)
            dialog.dismiss();
        interface1.networkResponse(result);
    }
}