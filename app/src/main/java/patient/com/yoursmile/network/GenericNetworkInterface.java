package patient.com.yoursmile.network;

import patient.com.yoursmile.model.ResponseModel;

/**
 * Created by vineet on 13/9/15.
 */
public interface GenericNetworkInterface {
     /**
      * @param responseModel
      */
     public void networkResponse(ResponseModel responseModel);
}
