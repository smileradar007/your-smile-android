package patient.com.yoursmile.network;

import patient.com.yoursmile.model.LoginModel;
import patient.com.yoursmile.model.ResponseModel;

/**
 * Created by vineet on 29/8/15.
 */
public interface LoginNetworkInterface {
    public void gPlusLoginResponse(ResponseModel response);
    public void facebookLoginResponse(ResponseModel response);
    public void SmileLoginResponse(ResponseModel response);
}
