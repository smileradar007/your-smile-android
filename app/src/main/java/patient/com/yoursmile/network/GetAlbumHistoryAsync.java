package patient.com.yoursmile.network;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import patient.com.yoursmile.R;
import patient.com.yoursmile.helper.PatientHelper;
import patient.com.yoursmile.model.AlbumHistoryModel;
import patient.com.yoursmile.model.ResponseModel;

/**
 * Created by vineet on 14/12/15.
 */
public class GetAlbumHistoryAsync extends AsyncTask<String, Void, ResponseModel> {
    private static final int FB_LOGIN = 1, GPLUS_LOGIN = 2;
    private Context context;
    private GenericNetworkInterface interface1;
    private ProgressDialog dialog;
    private boolean value;

    public GetAlbumHistoryAsync(Context context, boolean value, GenericNetworkInterface interface1) {
        this.context = context;
        this.interface1 = interface1;
        this.value = value;
    }

    @Override
    protected ResponseModel doInBackground(String... params) {
        // TODO Auto-generated method stub\
        try {
            String param = new NetworkConnection().networkHitAuthGetWithAccessToken(context, params[0]);
            if (param != null) {

                JSONObject jsonObject = new JSONObject(param);
                int success = jsonObject.getInt("success");
                ResponseModel responseModel;
                if (success == 1) {
                    responseModel = new ResponseModel(success, jsonObject.getString("errstr"));
                    JSONArray jsonArray = jsonObject.optJSONArray("data");
                    if (jsonArray != null && jsonArray.length() > 0) {
                        List<Object> list = new ArrayList<>();
                        int len = jsonArray.length();
                        for (int i = 0; i < len; i++) {
                            JSONObject jsonObject1 = jsonArray.optJSONObject(i);

                            String leftStringUrl = jsonObject1.getString("LeftImage");
                            if (leftStringUrl.contains("http://dentist.yoursmiledirect.com"))
                                leftStringUrl = PatientHelper.getNewImageString(leftStringUrl);
                            else
                                leftStringUrl = PatientHelper.getImageString(leftStringUrl);

                            String centerUrl = jsonObject1.getString("CenterImage");
                            if (centerUrl.contains("http://dentist.yoursmiledirect.com"))
                                centerUrl = PatientHelper.getNewImageString(centerUrl);
                            else
                                centerUrl = PatientHelper.getImageString(centerUrl);

                            String rightUrl = jsonObject1.getString("RightImage");
                            if (rightUrl.contains("http://dentist.yoursmiledirect.com"))
                                rightUrl = PatientHelper.getNewImageString(rightUrl);
                            else
                                rightUrl = PatientHelper.getImageString(rightUrl);

                            AlbumHistoryModel historyModel = new AlbumHistoryModel(
                                    jsonObject1.getString("AlbumID"),
                                    leftStringUrl,
                                    centerUrl,
                                    rightUrl,
                                    jsonObject1.getString("LeftImageStatus"),
                                    jsonObject1.getString("AlbumStatus"),
                                    jsonObject1.getString("RightImageStatus"),
                                    jsonObject1.getString("CenterImageStatus"),
                                    jsonObject1.getString("LastUpdated"),
                                    PatientHelper.getDateFromString(jsonObject1.getString("CreatedDate")));
                            list.add(historyModel);
                        }
                        responseModel.setObjectList(list);
                    }
                    return responseModel;
                } else {
                    responseModel = new ResponseModel(success, jsonObject.getString("errstr"));
                    return responseModel;
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPreExecute() {
        // TODO Auto-generated method stub
        super.onPreExecute();
        dialog = new ProgressDialog(context);
        dialog.setTitle(context.getString(R.string.app_name));
        dialog.setMessage("Please wait...");
        if (value)
            dialog.show();
    }

    @Override
    protected void onPostExecute(ResponseModel result) {
        // TODO Auto-generated method stub
        super.onPostExecute(result);
        if (dialog != null)
            dialog.dismiss();
        interface1.networkResponse(result);
    }
}