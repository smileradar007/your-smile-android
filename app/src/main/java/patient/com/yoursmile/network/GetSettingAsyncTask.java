package patient.com.yoursmile.network;

import android.content.Context;
import android.os.AsyncTask;
import org.json.JSONException;
import org.json.JSONObject;

import patient.com.yoursmile.model.ResponseModel;
import patient.com.yoursmile.model.SettingModel;

/**
 * Created by vineet on 13/9/15.
 */

public class GetSettingAsyncTask extends AsyncTask<String,Void,ResponseModel> {

    private Context context;
    private GenericNetworkInterface interface1;
    private JSONObject jsonObject;
    private static final int FB_LOGIN=1,GPLUS_LOGIN=2;

    public GetSettingAsyncTask(Context context, GenericNetworkInterface interface1) {
        this.context=context;
        this.interface1 = interface1;
        this.jsonObject = jsonObject;
    }

    @Override
    protected ResponseModel doInBackground(String... params) {
        // TODO Auto-generated method stub\
        try {
            String param = new NetworkConnection().networkHitAuthGetWithAccessToken(context, params[0]);
            if (param != null) {
                JSONObject jsonObject = new JSONObject(param);
                int success = jsonObject.getInt("success");
                ResponseModel responseModel;
                if (success == 1 || success==2) {
                    JSONObject jsonObject1 = new JSONObject(jsonObject.getString("data"));
                    responseModel = new ResponseModel(success, jsonObject.getString("errstr"));
                    SettingModel settingModel = new SettingModel(jsonObject1.getInt("sound"),jsonObject1.getInt("PushNotification"));
                    responseModel.setObject(settingModel);
                    return responseModel;
                } else {
                    responseModel = new ResponseModel(success, jsonObject.getString("errstr"));
                    return responseModel;
                }
            }
        } catch (JSONException e)
        {
            e.printStackTrace();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(ResponseModel result) {
        // TODO Auto-generated method stub
        super.onPostExecute(result);
        interface1.networkResponse(result);
    }
}
