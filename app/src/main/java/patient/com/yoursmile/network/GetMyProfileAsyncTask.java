package patient.com.yoursmile.network;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import org.json.JSONException;
import org.json.JSONObject;

import patient.com.yoursmile.helper.Logger;
import patient.com.yoursmile.helper.PatientHelper;
import patient.com.yoursmile.model.PatientModel;
import patient.com.yoursmile.model.ResponseModel;
import patient.com.yoursmile.R;

/**
 * Created by vineet on 17/9/15.
 */
public class GetMyProfileAsyncTask extends AsyncTask<String,Void,ResponseModel> {

    private Context context;
    private GenericNetworkInterface interface1;
    private ProgressDialog dialog;
    private JSONObject jsonObject;
    private static final int FB_LOGIN=1,GPLUS_LOGIN=2;

    public GetMyProfileAsyncTask(Context context, GenericNetworkInterface interface1) {
        this.context=context;
        this.interface1 = interface1;
        this.jsonObject = jsonObject;
    }

    @Override
    protected ResponseModel doInBackground(String... params) {
        // TODO Auto-generated method stub\
        try {
            String param = new NetworkConnection().networkHitAuthGetWithAccessToken(context, params[0]);
            if (param != null) {
                JSONObject jsonObject = new JSONObject(param);
                int success = jsonObject.getInt("success");
                Logger.error("GetMyProfileAsyncTask","profile response:"+param);
                ResponseModel responseModel;
                if (success == 1 || success==2) {
                    JSONObject jsonObject1 = new JSONObject(jsonObject.getString("data"));
                    responseModel = new ResponseModel(success, jsonObject.getString("errstr"));
                    PatientModel settingModel = new PatientModel(
                            jsonObject1.getString("name"),
                            jsonObject1.getString("address"),
                            jsonObject1.getString("email"),
                            jsonObject1.getString("contact"),
                            PatientHelper.getImageString(jsonObject1.getString("profileimage")),
                            jsonObject1.getString("description"),"");//jsonObject1.getString("demo")

                    responseModel.setObject(settingModel);
                    return responseModel;
                } else {
                    responseModel = new ResponseModel(success, jsonObject.getString("errstr"));
                    return responseModel;
                }
            }
        } catch (JSONException e)
        {
            e.printStackTrace();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPreExecute() {
        // TODO Auto-generated method stub
        super.onPreExecute();
        dialog = new ProgressDialog(context);
        dialog.setTitle(context.getString(R.string.app_name));
        dialog.setMessage("Please wait...");
        dialog.show();
    }

    @Override
    protected void onPostExecute(ResponseModel result) {
        // TODO Auto-generated method stub
        super.onPostExecute(result);
        if(dialog!=null)
            dialog.dismiss();
        interface1.networkResponse(result);
    }
}