package patient.com.yoursmile.network;

import android.content.Context;
import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ContentBody;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;

import patient.com.yoursmile.helper.Logger;
import patient.com.yoursmile.helper.SharedPref;

/**
 * Created by vineet on 29/8/15.
 */
public class NetworkConnection {
    private static final int TIME_OUT = 10000;
    private String TAG = NetworkConnection.class.getName();

    public String networkHitAuthGet(Context context, String url) {
        String response = null;
        try {
            HttpParams httpParameters = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(httpParameters, TIME_OUT);
            HttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            HttpResponse httpresponse = client.execute(post);
            HttpEntity resEntity = httpresponse.getEntity();
            int status = getStatusCode(httpresponse);
            if (status == 200)
                response = EntityUtils.toString(resEntity);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        Log.e("response---> ", "" + response);
        return response;
    }

    public String networkHitAuthDeleteWithAccessToken(Context context, String url) {
        SharedPref sharedPref = SharedPref.getInstance(context);
        String response = null;
        try {
            HttpParams httpParameters = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(httpParameters, TIME_OUT);
            HttpClient client = new DefaultHttpClient();
            HttpDelete post = new HttpDelete(url);
            Logger.error("URl", url);
            Logger.error("Access Token", sharedPref.getAccessToken());
            post.addHeader("accessToken", sharedPref.getAccessToken());
            HttpResponse httpresponse = client.execute(post);
            HttpEntity resEntity = httpresponse.getEntity();
            int status = getStatusCode(httpresponse);
            if (status == 200)
                response = EntityUtils.toString(resEntity);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        Log.e("response---> ", "" + response);
        return response;
    }

    public String networkHitAuthGetWithAccessToken(Context context, String url) {
        SharedPref sharedPref = SharedPref.getInstance(context);
        String response = null;
        try {
            HttpParams httpParameters = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(httpParameters, TIME_OUT);
            HttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            Logger.error("URl", url);
            Log.e("album url",url);
            Logger.error("Access Token", sharedPref.getAccessToken());
            post.addHeader("accessToken", sharedPref.getAccessToken());
            HttpResponse httpresponse = client.execute(post);
            HttpEntity resEntity = httpresponse.getEntity();
            int status = getStatusCode(httpresponse);
            if (status == 200)
                response = EntityUtils.toString(resEntity);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        Log.e("response---> ", "" + response);
        return response;
    }


    public String networkHitAuthGetWithAccessTokenAndDoctor(Context context, String url) {
        SharedPref sharedPref = SharedPref.getInstance(context);
        String response = null;
        try {
            HttpParams httpParameters = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(httpParameters, TIME_OUT);
            HttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            Logger.error("URl", url);
            Logger.error("Access Token", sharedPref.getAccessToken());
            post.addHeader("accessToken", sharedPref.getAccessToken());
            post.addHeader("doctorId", sharedPref.getDoctorID());
            HttpResponse httpresponse = client.execute(post);
            HttpEntity resEntity = httpresponse.getEntity();
            int status = getStatusCode(httpresponse);
            if (status == 200)
                response = EntityUtils.toString(resEntity);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        Log.e("response---> ", "" + response);
        return response;
    }


    public String networkHitJson(Context context, String json, String url) {
        Logger.info(TAG, "Post url---> " + url);
        Logger.info(TAG, "Post Params---> " + json.toString());
        String response = null;
        try {
            HttpParams httpParameters = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(httpParameters, TIME_OUT);
            DefaultHttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(url);
            StringEntity se = new StringEntity(json, HTTP.UTF_8);
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(se);
            HttpResponse httpresponse = client.execute(post);
            HttpEntity resEntity = httpresponse.getEntity();
            int status = getStatusCode(httpresponse);
            Logger.info(TAG, "response Code---> " + status);
            response = EntityUtils.toString(resEntity);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        Logger.info(TAG, "response---> " + response);
        return response;
    }


    public String networkHitJsonWithAccessToken(Context context, String url, String json) {
        Logger.info(TAG, "Post url---> " + url);
        Logger.info(TAG, "Post Params---> " + json.toString());
        SharedPref sharedPref = SharedPref.getInstance(context);
        String response = null;
        try {
            HttpParams httpParameters = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(httpParameters, TIME_OUT);
            DefaultHttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(url);
            post.addHeader("accessToken", sharedPref.getAccessToken());
            StringEntity se = new StringEntity(json, HTTP.UTF_8);
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));

            post.setEntity(se);
            HttpResponse httpresponse = client.execute(post);
            HttpEntity resEntity = httpresponse.getEntity();
            int status = getStatusCode(httpresponse);
            Logger.info(TAG, "response Code---> " + status);
            response = EntityUtils.toString(resEntity);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        Logger.info(TAG, "response---> " + response);
        return response;
    }

    public String networkHitForDoctorLink(Context context, String url, JSONObject json) throws Exception {
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost post = new HttpPost(url);
        SharedPref sharedPref = SharedPref.getInstance(context);
        String patientId = json.getString("patientId");
        String doctorId = json.getString("doctorId");
        String action = json.getString("action");
        Logger.error("Link doctor", "params: " + json.toString() + ", " + sharedPref.getAccessToken());
        MultipartEntity reqEntity = new MultipartEntity();
        post.addHeader("accessToken", sharedPref.getAccessToken());
        reqEntity.addPart("patientId", new StringBody(patientId));
        reqEntity.addPart("doctorId", new StringBody(doctorId));
        reqEntity.addPart("action", new StringBody(action));
        post.setEntity(reqEntity);
        HttpResponse httpResponse = httpClient.execute(post);
        int responseCode = httpResponse.getStatusLine().getStatusCode();
        Logger.error("Status", responseCode + "");
        String response = streamToString(httpResponse.getEntity().getContent());
        Logger.error("Response", response + "");
        return response;
    }

    public String networkLoginHit(Context context, String URL, JSONObject json) throws JSONException, UnsupportedEncodingException, Exception {
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost post = new HttpPost(URL);
        Logger.error(TAG, "response: " + json.toString());
        String signupType = json.getString("signupType");
        String socialLoginMedium = json.getString("socialLoginMedium");
        String socialId = json.getString("socialId");
        String platform = json.getString("platform");
        String name = json.getString("name");
        String deviceToken = json.getString("deviceToken");
        String address = json.getString("address");
        String email = json.getString("email");
        String contact = json.getString("contact");
        String description = json.getString("description");
        String profileimage = json.getString("profileimage");
        MultipartEntity reqEntity = new MultipartEntity();
        reqEntity.addPart("signupType", new StringBody(signupType));
        reqEntity.addPart("socialLoginMedium", new StringBody(socialLoginMedium));
        reqEntity.addPart("socialId", new StringBody(socialId));
        reqEntity.addPart("platform", new StringBody(platform));
        reqEntity.addPart("name", new StringBody(name));
        reqEntity.addPart("deviceToken", new StringBody(deviceToken));
        reqEntity.addPart("address", new StringBody(address));
        reqEntity.addPart("email", new StringBody(email));
        reqEntity.addPart("contact", new StringBody(contact));
        reqEntity.addPart("description", new StringBody(description));
        reqEntity.addPart("profileimage", new StringBody(profileimage));

        post.setEntity(reqEntity);
        HttpResponse httpResponse = httpClient.execute(post);
        int responseCode = httpResponse.getStatusLine().getStatusCode();
//        Logger.error("Status", responseCode + "");
        String response = streamToString(httpResponse.getEntity().getContent());
        Logger.error("Response", response + "");
        return response;
    }

    public String editProfileNetwork(Context context, String URL, JSONObject json) throws JSONException, UnsupportedEncodingException, Exception {

        SharedPref sharedPref = SharedPref.getInstance(context);
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost post = new HttpPost(URL);
        Logger.error("Payload ", json.toString());
        String patientId = json.getString("patientId");
        String action = json.getString("action");
        String userName = json.getString("userName");
        String userAddress = json.getString("userAddress");
        String userEmailid = json.getString("userEmailid");
        String userContact = json.getString("userContact");
        String userDescription = json.getString("userDescription");
        MultipartEntity reqEntity = new MultipartEntity();
        String profileimage = json.getString("file");

        if (profileimage.length() > 0) {
            ContentBody cbFile = new FileBody(new File(profileimage), "image/png");
            reqEntity.addPart("file", cbFile);
        } else {
            String removeImage = "1";
            reqEntity.addPart("removeImage", new StringBody(removeImage));
        }
        reqEntity.addPart("patientId", new StringBody(patientId));
        reqEntity.addPart("action", new StringBody(action));
        reqEntity.addPart("userName", new StringBody(userName));
        reqEntity.addPart("userAddress", new StringBody(userAddress));
        reqEntity.addPart("userEmailid", new StringBody(userEmailid));
        reqEntity.addPart("userContact", new StringBody(userContact));
        reqEntity.addPart("userDescription", new StringBody(userDescription));

        post.setEntity(reqEntity);
        post.addHeader("accessToken", sharedPref.getAccessToken());
        Logger.error("Access Token", sharedPref.getAccessToken());
        HttpResponse httpResponse = httpClient.execute(post);
        int responseCode = httpResponse.getStatusLine().getStatusCode();
        Logger.error("Status", responseCode + "");
        String response = streamToString(httpResponse.getEntity().getContent());
        Logger.error("Response", response + "");
        return response;
    }

    public String uploadFile(Context context, String URL, JSONObject json) throws JSONException, UnsupportedEncodingException, Exception {
        DefaultHttpClient httpClient = new DefaultHttpClient();
        HttpPost post = new HttpPost(URL);
        Logger.error("Payload ", json.toString());

        String signupType = json.getString("signupType");
        String socialLoginMedium = json.getString("socialLoginMedium");
        String socialId = json.getString("socialId");
        String platform = json.getString("platform");
        String name = json.getString("name");
        String deviceToken = json.getString("deviceToken");

        String pname = json.getString("pname");
        String ptelephone = json.getString("ptelephone");
        String websiteurl = json.getString("websiteurl");
        String twhandle = json.getString("twhandle");
        String fbpageurl = json.getString("fbpageurl");

        String address = json.getString("address");
        String email = json.getString("email");
        String description = json.getString("description");
        String profileimage = json.getString("profileimage");
        String longitude = json.getString("longitude");
        String latitude = json.getString("latitude");
        String etelephone = json.getString("etelephone");

        MultipartEntity reqEntity = new MultipartEntity();

        ContentBody cbFile = new FileBody(new File(profileimage), "image/png");

        reqEntity.addPart("file", cbFile);
        reqEntity.addPart("signupType", new StringBody(signupType));
        reqEntity.addPart("socialLoginMedium", new StringBody(socialLoginMedium));
        reqEntity.addPart("socialId", new StringBody(socialId));
        reqEntity.addPart("platform", new StringBody(platform));
        reqEntity.addPart("name", new StringBody(name));
        reqEntity.addPart("deviceToken", new StringBody(deviceToken));
        reqEntity.addPart("address", new StringBody(address));
        reqEntity.addPart("email", new StringBody(email));
        reqEntity.addPart("pname", new StringBody(pname));
        reqEntity.addPart("description", new StringBody(description));
        reqEntity.addPart("ptelephone", new StringBody(ptelephone));
        reqEntity.addPart("longitude", new StringBody(longitude));
        reqEntity.addPart("latitude", new StringBody(latitude));
        reqEntity.addPart("websiteurl", new StringBody(websiteurl));
        reqEntity.addPart("etelephone", new StringBody(etelephone));
        reqEntity.addPart("twhandle", new StringBody(twhandle));
        reqEntity.addPart("fbpageurl", new StringBody(fbpageurl));


        post.setEntity(reqEntity);
        HttpResponse httpResponse = httpClient.execute(post);
        int responseCode = httpResponse.getStatusLine().getStatusCode();
        Logger.error("Status", responseCode + "");
        String response = streamToString(httpResponse.getEntity().getContent());
        Logger.error("Response", response + "");
        return response;
    }


    public String uploadAlbum(Context context, String URL, JSONObject json) throws JSONException, UnsupportedEncodingException, Exception {
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost post = new HttpPost(URL);
        Logger.error("Payload ", json.toString());
        String patientId = json.getString("patientId");
        String doctorId = json.getString("doctorId");

        MultipartEntity reqEntity = new MultipartEntity();

        ContentBody left = new FileBody(new File(json.getString("left")), "image/png");
        ContentBody center = new FileBody(new File(json.getString("center")), "image/png");
        ContentBody right = new FileBody(new File(json.getString("right")), "image/png");

        reqEntity.addPart("left", left);
        reqEntity.addPart("center", center);
        reqEntity.addPart("right", right);
        reqEntity.addPart("patientId", new StringBody(patientId));
        reqEntity.addPart("doctorId", new StringBody(doctorId));

        post.setEntity(reqEntity);
        HttpResponse httpResponse = httpClient.execute(post);
        int responseCode = httpResponse.getStatusLine().getStatusCode();
        Logger.error("Status", responseCode + "");
        String response = streamToString(httpResponse.getEntity().getContent());
        Logger.error("Response", response + "");
        return response;
    }

    public String editAlbum(Context context, String URL, String albumId, String leftString, String centerString, String rightString) throws JSONException, UnsupportedEncodingException, Exception {
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost post = new HttpPost(URL);

        SharedPref sharedPref = SharedPref.getInstance(context);
        MultipartEntity reqEntity = new MultipartEntity();

        if (leftString != null) {
            ContentBody left = new FileBody(new File(leftString), "image/png");
            reqEntity.addPart("left", left);
        }

        if (centerString != null) {
            ContentBody center = new FileBody(new File(centerString), "image/png");
            reqEntity.addPart("center", center);
        }

        if (rightString != null) {
            ContentBody right = new FileBody(new File(rightString), "image/png");
            reqEntity.addPart("right", right);
        }

        reqEntity.addPart("patientId", new StringBody(sharedPref.getPatientID()));
        reqEntity.addPart("doctorId", new StringBody(sharedPref.getDoctorID()));
        reqEntity.addPart("albumId", new StringBody(albumId));

        post.setEntity(reqEntity);
        post.addHeader("accessToken", sharedPref.getAccessToken());
        HttpResponse httpResponse = httpClient.execute(post);
        int responseCode = httpResponse.getStatusLine().getStatusCode();
        Logger.error("Status", responseCode + "");
        String response = streamToString(httpResponse.getEntity().getContent());
        Logger.error("Response", response + "");
        return response;
    }

    public String signup_Network(Context context, String URL, JSONObject json) throws JSONException, UnsupportedEncodingException, Exception {
        SharedPref sharedPref = SharedPref.getInstance(context);
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost post = new HttpPost(URL);
        Logger.error("Payload ", json.toString());

        String address = json.getString("address");
        String contact = json.getString("contact");
        String description = json.getString("description");
        String deviceToken = json.getString("deviceToken");
        String email = json.getString("email");
        String loginOrSignup = json.getString("loginOrSignup");
        String name = json.getString("name");
        String password = json.getString("password");
        String platform = json.getString("platform");
        String signupType = json.getString("signupType");
        String socialLoginMedium = json.getString("socialLoginMedium");


        MultipartEntity reqEntity = new MultipartEntity();
        String file = json.getString("file");

        if (file.length() > 0) {
            ContentBody cbFile = new FileBody(new File(file), "image/png");
            reqEntity.addPart("file", cbFile);
        }

        reqEntity.addPart("address", new StringBody(address));
        reqEntity.addPart("contact", new StringBody(contact));
        reqEntity.addPart("description", new StringBody(description));
        reqEntity.addPart("deviceToken", new StringBody(deviceToken));
        reqEntity.addPart("email", new StringBody(email));
        reqEntity.addPart("loginOrSignup", new StringBody(loginOrSignup));
        reqEntity.addPart("name", new StringBody(name));
        reqEntity.addPart("password", new StringBody(password));
        reqEntity.addPart("platform", new StringBody(platform));
        reqEntity.addPart("signupType", new StringBody(signupType));
        reqEntity.addPart("socialLoginMedium", new StringBody(socialLoginMedium));


        post.setEntity(reqEntity);
        HttpResponse httpResponse = httpClient.execute(post);
        int responseCode = httpResponse.getStatusLine().getStatusCode();
        Logger.error("Status", responseCode + "");
        String response = streamToString(httpResponse.getEntity().getContent());
        Logger.error("Response", response + "");
        return response;
    }


    public String login_Network(Context context, String URL, JSONObject json) throws JSONException, UnsupportedEncodingException, Exception {
        SharedPref sharedPref = SharedPref.getInstance(context);
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost post = new HttpPost(URL);
        Logger.error("Payload ", json.toString());


        String deviceToken = json.getString("deviceToken");
        String email = json.getString("email");
        String loginOrSignup = json.getString("loginOrSignup");
        String password = json.getString("password");
        String platform = json.getString("platform");
        String signupType = json.getString("signupType");
        String socialLoginMedium = json.getString("socialLoginMedium");

        MultipartEntity reqEntity = new MultipartEntity();
        reqEntity.addPart("deviceToken", new StringBody(deviceToken));
        reqEntity.addPart("email", new StringBody(email));
        reqEntity.addPart("loginOrSignup", new StringBody(loginOrSignup));
        reqEntity.addPart("password", new StringBody(password));
        reqEntity.addPart("platform", new StringBody(platform));
        reqEntity.addPart("signupType", new StringBody(signupType));
        reqEntity.addPart("socialLoginMedium", new StringBody(socialLoginMedium));

        post.setEntity(reqEntity);
        HttpResponse httpResponse = httpClient.execute(post);
        int responseCode = httpResponse.getStatusLine().getStatusCode();
        Logger.error("Status", responseCode + "");
        String response = streamToString(httpResponse.getEntity().getContent());
        Logger.error("Response", response + "");
        return response;
    }


    public String streamToString(InputStream is) {
        java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
        return s.hasNext() ? s.next() : "";
    }

    private int getStatusCode(HttpResponse response) {
        StatusLine statusLine = response.getStatusLine();
        int status_code = statusLine.getStatusCode();
        return status_code;
    }
}
