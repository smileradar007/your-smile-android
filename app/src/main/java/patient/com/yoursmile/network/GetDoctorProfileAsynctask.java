package patient.com.yoursmile.network;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import org.json.JSONException;
import org.json.JSONObject;

import patient.com.yoursmile.helper.PatientHelper;
import patient.com.yoursmile.model.DoctorProfileModel;
import patient.com.yoursmile.model.ResponseModel;
import patient.com.yoursmile.R;

/**
 *
 *
 * Created by vineet on 13/9/15.
 *
 *
 */
public class GetDoctorProfileAsynctask extends AsyncTask<String,Void,ResponseModel> {

    private Context context;
    private GenericNetworkInterface interface1;
    private ProgressDialog dialog;
    private boolean showProgress;

    public GetDoctorProfileAsynctask(Context context, GenericNetworkInterface interface1,boolean showProgress) {
        this.context=context;
        this.interface1 = interface1;
        this.showProgress = showProgress;
    }

    @Override
    protected ResponseModel doInBackground(String... params) {
        // TODO Auto-generated method stub\
        try {
            String param = new NetworkConnection().networkHitAuthGetWithAccessToken(context,params[0]);
            if (param != null) {
                JSONObject jsonObject = new JSONObject(param);
                int success = jsonObject.getInt("success");
                int accountVerificationKey = jsonObject.getInt("accountVerificationKey");

                ResponseModel model;
                if (success == 1 && accountVerificationKey==1 ) {
                    JSONObject jsonObject1 = new JSONObject(jsonObject.getString("data"));
                    model = new ResponseModel(success, jsonObject.optString("errstr"));
                    model.setOther1(accountVerificationKey);
                    DoctorProfileModel doctorProfileModel = new DoctorProfileModel(
                            jsonObject1.optString("address"),
                            jsonObject1.optString("description"),
                            jsonObject1.optString("email"),
                            jsonObject1.optString("emmergency-telephone"),
                            jsonObject1.optString("fbpageurl"),
                            jsonObject1.optString("latitude"),
                            jsonObject1.optString("longitude"),
                            jsonObject1.optString("name"),
                            jsonObject1.optString("practice-name"),
                            jsonObject1.optString("practice-telephone"),
                            PatientHelper.getImageString(jsonObject1.optString("profileimage")),
                            jsonObject1.optString("twhandle"),
                            jsonObject1.optString("websiteurl"),
                            PatientHelper.getImageString(jsonObject.optString("Morph")),
                            jsonObject1.optString("qualification"));
                    model.setObject(doctorProfileModel);
                    return model;
                } else {
                    model = new ResponseModel(success, jsonObject.optString("errstr"));
                    model.setOther1(accountVerificationKey);
                    return model;
                }
            }
        } catch (JSONException e)
        {
            e.printStackTrace();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onPreExecute() {
        // TODO Auto-generated method stub
        super.onPreExecute();

        dialog = new ProgressDialog(context);
        dialog.setTitle(context.getString(R.string.app_name));
        dialog.setCancelable(false);
        dialog.setMessage("Please wait...");
        if(showProgress)
        dialog.show();
    }

    @Override
    protected void onPostExecute(ResponseModel result) {
        // TODO Auto-generated method stub
        super.onPostExecute(result);
        if(dialog!=null && showProgress)
            dialog.dismiss();
        interface1.networkResponse(result);
    }
}
