package patient.com.yoursmile.network;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import org.json.JSONException;
import org.json.JSONObject;

import patient.com.yoursmile.helper.PatientHelper;
import patient.com.yoursmile.model.AlbumModel;
import patient.com.yoursmile.model.ResponseModel;
import patient.com.yoursmile.R;
/**
 * Created by vineet on 31/10/15.
 */
public class GetAlbumAsynctask extends AsyncTask<String,Void,ResponseModel> {
    private Context context;
    private GenericNetworkInterface interface1;
    private ProgressDialog dialog;
    private static final int FB_LOGIN=1,GPLUS_LOGIN=2;

    public GetAlbumAsynctask(Context context, GenericNetworkInterface interface1) {
        this.context=context;
        this.interface1 = interface1;
    }

    @Override
    protected ResponseModel doInBackground(String... params) {
        // TODO Auto-generated method stub\
        try {

            String param = new NetworkConnection().networkHitAuthGetWithAccessToken(context, params[0]);
            if (param != null) {
                JSONObject jsonObject = new JSONObject(param);
                int success = jsonObject.getInt("success");
                ResponseModel responseModel;
                if (success == 1) {
                    JSONObject jsonObject1 = new JSONObject(jsonObject.getString("data"));
                    responseModel = new ResponseModel(success, jsonObject.getString("errstr"));
                    if (jsonObject1 != null ) {

                        String leftStringUrl = jsonObject1.getString("LeftImage");
                        if(leftStringUrl.contains("www.smile-radar.com"))
                            leftStringUrl = PatientHelper.getNewImageString(leftStringUrl);
                        else
                            leftStringUrl = PatientHelper.getImageString(leftStringUrl);

                        String centerUrl = jsonObject1.getString("CenterImage");
                        if(centerUrl.contains("www.smile-radar.com"))
                            centerUrl = PatientHelper.getNewImageString(centerUrl);
                        else
                            centerUrl = PatientHelper.getImageString(centerUrl);

                        String rightUrl = jsonObject1.getString("RightImage");
                        if(rightUrl.contains("www.smile-radar.com"))
                            rightUrl = PatientHelper.getNewImageString(rightUrl);
                        else
                            rightUrl = PatientHelper.getImageString(rightUrl);

                        AlbumModel albumModel = new AlbumModel(
                                jsonObject1.getString("AlbumID"),
                                leftStringUrl,
                                centerUrl,
                                rightUrl,
                                jsonObject1.getString("AlbumStatus"),
                                jsonObject1.getString("LeftImageStatus"),
                                jsonObject1.getString("RightImageStatus"),
                                jsonObject1.getString("CenterImageStatus"),
                                jsonObject1.getString("LastUpdated"),
                                jsonObject1.getString("CreatedDate"));
                        responseModel.setObject(albumModel);
                    }
                    return responseModel;
                }else{
                    responseModel = new ResponseModel(success, jsonObject.getString("errstr"));
                    return responseModel;
                }
            }
        } catch (JSONException e)
        {
            e.printStackTrace();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return null;
    }
    @Override
    protected void onPreExecute() {
        // TODO Auto-generated method stub
        super.onPreExecute();
        dialog = new ProgressDialog(context);
        dialog.setTitle(context.getString(R.string.app_name));
        dialog.setMessage("Please wait...");
        dialog.show();
    }

    @Override
    protected void onPostExecute(ResponseModel result) {
        // TODO Auto-generated method stub
        super.onPostExecute(result);
        if(dialog!=null)
            dialog.dismiss();
        interface1.networkResponse(result);
    }
}

