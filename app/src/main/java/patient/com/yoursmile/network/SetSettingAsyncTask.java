package patient.com.yoursmile.network;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import org.json.JSONException;
import org.json.JSONObject;

import patient.com.yoursmile.helper.PatientConstant;
import patient.com.yoursmile.model.ResponseModel;
import patient.com.yoursmile.model.SettingModel;
import patient.com.yoursmile.R;

/**
 * Created by vineet on 15/9/15.
 */
public class SetSettingAsyncTask extends AsyncTask<String,Void,ResponseModel> {

    private Context context;
    private GenericNetworkInterface interface1;
    private ProgressDialog dialog;
    private JSONObject jsonObject;
    private static final int FB_LOGIN=1,GPLUS_LOGIN=2;


    public SetSettingAsyncTask(Context context, GenericNetworkInterface interface1,JSONObject jsonObject) {
        this.context=context;
        this.interface1 = interface1;
        this.jsonObject = jsonObject;
    }

    @Override
    protected ResponseModel doInBackground(String... params) {
        // TODO Auto-generated method stub\
        try {
            String param = new NetworkConnection().networkHitJsonWithAccessToken(context,PatientConstant.BASE_URL+"setting.json",jsonObject.toString());
            if (param != null) {
                JSONObject jsonObject = new JSONObject(param);
                int success = jsonObject.getInt("success");
                ResponseModel responseModel;
                if (success == 1) {
                    JSONObject jsonObject1 = new JSONObject(jsonObject.getString("data"));
                    responseModel = new ResponseModel(success, jsonObject.getString("errstr"));
                    SettingModel settingModel = new SettingModel(jsonObject1.getInt("Sound"),jsonObject1.getInt("PushNotification"));
                    responseModel.setObject(settingModel);
                    return responseModel;
                } else {
                    responseModel = new ResponseModel(success, jsonObject.getString("errstr"));
                    return responseModel;
                }
            }
        } catch (JSONException e)
        {
            e.printStackTrace();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPreExecute() {
        // TODO Auto-generated method stub
        super.onPreExecute();
        dialog = new ProgressDialog(context);
        dialog.setTitle(context.getString(R.string.app_name));
        dialog.setMessage("Please wait...");
        dialog.show();
    }

    @Override
    protected void onPostExecute(ResponseModel result) {
        // TODO Auto-generated method stub
        super.onPostExecute(result);
        if(dialog!=null)
            dialog.dismiss();
        interface1.networkResponse(result);
    }
}