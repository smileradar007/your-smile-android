package patient.com.yoursmile.network;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import org.json.JSONException;
import org.json.JSONObject;

import patient.com.yoursmile.model.ResponseModel;
import patient.com.yoursmile.R;

/**
 * Created by vineet on 18/11/15.
 */
public class AlbumEditAsynctask extends AsyncTask<String,Void,ResponseModel> {

    private Context context;
    private GenericNetworkInterface interface1;
    private ProgressDialog dialog;
    private JSONObject jsonObject;
    private static final int FB_LOGIN = 1, GPLUS_LOGIN = 2;

    private String left,center,right,albumID;
    public AlbumEditAsynctask(Context context, GenericNetworkInterface interface1,String albumId,String leftString,String centerString,String rightString) {
        this.context = context;
        this.interface1 = interface1;
        this.jsonObject = jsonObject;

        this.albumID = albumId;
        this.left = leftString;
        this.center = centerString;
        this.right = rightString;
    }

    @Override
    protected ResponseModel doInBackground(String... params) {
        // TODO Auto-generated method stub\
        try {
            String param = new NetworkConnection().editAlbum(context, params[0], albumID,left,center,right);
            if (param != null) {
                JSONObject jsonObject = new JSONObject(param);
                int success = jsonObject.getInt("success");
                ResponseModel responseModel;
                if (success == 1) {
                    responseModel = new ResponseModel(success, jsonObject.getString("errstr"));
                    return responseModel;
                } else {
                    responseModel = new ResponseModel(success, jsonObject.getString("errstr"));
                    return responseModel;
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPreExecute() {
        // TODO Auto-generated method stub
        super.onPreExecute();
        dialog = new ProgressDialog(context);
        dialog.setTitle(context.getString(R.string.app_name));
        dialog.setMessage("Please wait...");
        dialog.show();
    }

    @Override
    protected void onPostExecute(ResponseModel result) {
        // TODO Auto-generated method stub
        super.onPostExecute(result);
        if (dialog != null)
            dialog.dismiss();
        interface1.networkResponse(result);
    }
}
