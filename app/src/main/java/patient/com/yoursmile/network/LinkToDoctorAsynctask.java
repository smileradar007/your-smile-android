package patient.com.yoursmile.network;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import org.json.JSONException;
import org.json.JSONObject;

import patient.com.yoursmile.helper.Logger;
import patient.com.yoursmile.helper.PatientConstant;
import patient.com.yoursmile.model.LoginModel;
import patient.com.yoursmile.model.ResponseModel;
import patient.com.yoursmile.R;

/**
 * Created by vineet on 13/9/15.
 */
public class LinkToDoctorAsynctask extends AsyncTask<String,Void,ResponseModel> {

    private Context context;
    private LoginNetworkInterface interface1;
    private ProgressDialog dialog;
    private JSONObject jsonObject;
    private static final int FB_LOGIN=1,GPLUS_LOGIN=2;

    public LinkToDoctorAsynctask(Context context, LoginNetworkInterface interface1,JSONObject jsonObject) {
        this.context=context;
        this.interface1 = interface1;
        this.jsonObject = jsonObject;
    }

    @Override
    protected ResponseModel doInBackground(String... params) {
        // TODO Auto-generated method stub\
        try {
            Logger.error("link doctor URL",PatientConstant.LINK_DOCTOR);
            String param = new NetworkConnection().networkHitForDoctorLink(context, PatientConstant.LINK_DOCTOR, jsonObject);
            if (param != null) {
                JSONObject jsonObject = new JSONObject(param);
                Logger.error("LinkToDoctorAsyncTask","response:"+param);
                int success = jsonObject.getInt("success");
                String error = jsonObject.optString("errstr");
                ResponseModel responseModel = new ResponseModel(success,error);
                LoginModel loginModel;
                if (success == 1 || success==2) {
//                    JSONObject jsonObject1 = new JSONObject(jsonObject.getString("data"));
                    loginModel = new LoginModel(
                            success,
                            0,
                            jsonObject.getString("errstr"),
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "","","");
                    responseModel.setObject(loginModel);
                    return responseModel;
                } else {
                    loginModel = new LoginModel(
                            success,
                            0,
                            jsonObject.getString("errstr"),
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "","","");
                    responseModel.setObject(loginModel);
                    return responseModel;
                }
            }
        } catch (JSONException e)
        {
            e.printStackTrace();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPreExecute() {
        // TODO Auto-generated method stub
        super.onPreExecute();
        dialog = new ProgressDialog(context);
        dialog.setTitle(context.getString(R.string.app_name));
        dialog.setMessage("Please wait...");
        dialog.show();
    }

    @Override
    protected void onPostExecute(ResponseModel result) {
        // TODO Auto-generated method stub
        super.onPostExecute(result);
        if(dialog!=null)
            dialog.dismiss();
            interface1.gPlusLoginResponse(result);
    }
}
