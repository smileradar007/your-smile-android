package patient.com.yoursmile.network;


import org.json.JSONException;
import org.json.JSONObject;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import patient.com.yoursmile.helper.Logger;
import patient.com.yoursmile.helper.PatientConstant;
import patient.com.yoursmile.model.LoginModel;
import patient.com.yoursmile.model.ResponseModel;
import patient.com.yoursmile.R;


public class LoginAsnctask extends AsyncTask<String,Void,ResponseModel> {
	private Context context;
	private LoginNetworkInterface interface1;
	private ProgressDialog dialog;
	private JSONObject jsonObject;
	private int loginType;
	private static final int FB_LOGIN=1,GPLUS_LOGIN=2;

	public LoginAsnctask(Context context, LoginNetworkInterface interface1,JSONObject jsonObject,int loginType) {
		this.context=context;
		this.loginType = loginType;
		this.interface1 = interface1;
		this.jsonObject = jsonObject;
		Logger.error("LoginAsnctask","login URL: "+PatientConstant.SIGNUP_URL+",parameters: "+jsonObject.toString());
	}

	/**
	 http://sawbros.com/services/patientSignup

	 socialLoginMedium = “FACEBOOK”
	 socialId = ”25432465325635”
	 deviceToken = ”25432465325635”
	 userName = ”ABC”
	 userAddress = ”Noida,India”
	 userEmailid = “abc@abc.com”
	 userContact = “8734753475”
	 userProfileImage = image url
	 userDescription = “sadfeasdgfvdsaf”
	 platform = “IOS”

	 Response-
	 status- 0/1   // 0 for failed, 1 for success
	 message= @"successfull"
	 data={
	 all profile data
	 }
	 */

	@Override
	protected ResponseModel doInBackground(String... params) {
		// TODO Auto-generated method stub\
		try {
			String param = new NetworkConnection().networkLoginHit(context, PatientConstant.SIGNUP_URL, jsonObject);
			if (param != null) {
				JSONObject jsonObject = new JSONObject(param);
				int success = jsonObject.getInt("success");
				String error = jsonObject.optString("errstr");
				ResponseModel responseModel = new ResponseModel(success,error);
				LoginModel loginModel;
				if (success == 1 || success==2) {
					JSONObject jsonObject1 = new JSONObject(jsonObject.getString("data"));
					String id = jsonObject1.optString("id");
					Logger.error("doInBackground", "Login response:"+ param);
					loginModel = new LoginModel(
							success,
							jsonObject1.optInt("socialLoginMedium"),
							jsonObject.optString("errstr"),
							jsonObject1.optString("socialId"),
							jsonObject1.optString("name"),
							jsonObject1.optString("profileimage"),
							jsonObject1.optString("email"),
							id,
							jsonObject1.optString("contact"),
							jsonObject1.optString("deviceToken"),
							jsonObject1.optString("accessToken"),
							jsonObject1.optString("doctorname"),
							jsonObject1.optString("doctorId"));
					responseModel.setObject(loginModel);
					return responseModel;
				} else {
					loginModel = new LoginModel(
							success,
							0,
							jsonObject.getString("errstr"),
							"",
							"",
							"",
							"",
							"",
							"",
							"",
							"","","");
					responseModel.setObject(loginModel);
					return responseModel;
				}
			}
		} catch (JSONException e)
		{
			e.printStackTrace();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

		return null;
	}

	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();
		dialog = new ProgressDialog(context);
		dialog.setTitle(context.getString(R.string.app_name));
		dialog.setMessage("Please wait...");
		dialog.show();
	}

	@Override
	protected void onPostExecute(ResponseModel result) {
		// TODO Auto-generated method stub
		super.onPostExecute(result);
		if(dialog!=null)
			dialog.dismiss();
		if(loginType==FB_LOGIN)
			interface1.facebookLoginResponse(result);
		else if(loginType==GPLUS_LOGIN)
			interface1.gPlusLoginResponse(result);
	}
}