package patient.com.yoursmile.adapter;

import java.util.ArrayList;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import patient.com.yoursmile.fragment.MenuFragment;
import patient.com.yoursmile.model.PagerLimitModel;

public class SliderAdapter extends FragmentPagerAdapter
{
	private ArrayList<PagerLimitModel> mMenuLinksList;
	public SliderAdapter(FragmentManager fm,ArrayList<PagerLimitModel> mMenuLinksList)
	{
		super(fm);
		this.mMenuLinksList = mMenuLinksList;
	}

	@Override
	public Fragment getItem(int position)
	{
		MenuFragment fragment = new MenuFragment();
		Bundle bundle = new Bundle();
		bundle.putInt("pos", position);
		bundle.putSerializable("List",mMenuLinksList);
		fragment.setArguments(bundle);
		return fragment;
	}

	@Override
	public int getCount()
	{
		return mMenuLinksList.size();
	}

	@Override
	public int getItemPosition(Object object)
	{
		return POSITION_NONE;
	}
}

