package patient.com.yoursmile.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import patient.com.yoursmile.helper.SharedPref;
import patient.com.yoursmile.model.MenuModel;
import patient.com.yoursmile.R;

import java.util.List;

/**
 * Created by vineet on 11/10/15.
 */
public class SideMenuListAdapter extends BaseAdapter {


    private Context mContext;
    private List<MenuModel> mList;
    private LayoutInflater inflater;
    private boolean treatmentBoolean = false;
    private SharedPref pref;

    public SideMenuListAdapter(Context context, List<MenuModel> list){
        mContext = context;
        mList = list;
        inflater = LayoutInflater.from(context);
        pref = SharedPref.getInstance(context);

    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public MenuModel getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
    ViewHolder viewHolder=null;
        if(view==null){
            view = inflater.inflate(R.layout.inflate_side_menu,null,false);
            viewHolder = new ViewHolder();
            viewHolder.txtName = (TextView)view.findViewById(R.id.txt_menu);
            viewHolder.imgMenu = (ImageView)view.findViewById(R.id.img_menu);
            view.setTag(viewHolder);
        }else {
            viewHolder = (ViewHolder) view.getTag();
        }
        MenuModel model = getItem(position);
        viewHolder.txtName.setText(mContext.getString(model.getTextResource()));
        if(pref.isMenuRed() && mContext.getString(model.getTextResource()).equalsIgnoreCase(mContext.getString(R.string.photo_request)))
        {
            viewHolder.txtName.setTextColor(mContext.getResources().getColor(R.color.red));
        }else {
            viewHolder.txtName.setTextColor(mContext.getResources().getColor(R.color.black));
        }
        viewHolder.imgMenu.setImageDrawable(mContext.getResources().getDrawable(model.getImgResource()));

        if(model.getTextResource()==R.string.treatment_outcome){
            if(isTreatmentBoolean()){
                viewHolder.txtName.setTextColor(mContext.getResources().getColor(R.color.black));
            }
            else{
                Log.e("Treatment"," Black");
                viewHolder.txtName.setTextColor(mContext.getResources().getColor(R.color.dark_grey));
            }
        }
     return view;
    }
    public boolean isTreatmentBoolean() {
        return treatmentBoolean;
    }
    public void setTreatmentBoolean(boolean treatmentBoolean) {
        this.treatmentBoolean = treatmentBoolean;
        notifyDataSetChanged();
    }

    private static class ViewHolder{
        private TextView txtName;
        private ImageView imgMenu;
    }
}
