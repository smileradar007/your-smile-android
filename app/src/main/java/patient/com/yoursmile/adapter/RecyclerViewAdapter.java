package patient.com.yoursmile.adapter;

/**
 *
 *
 * Created by vineet on 25/10/15.
 *
 *
 */

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import java.util.ArrayList;
import patient.com.yoursmile.model.NotificationModel;
import patient.com.yoursmile.R;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.SimpleViewHolder> {
    public static class SimpleViewHolder extends RecyclerView.ViewHolder {
        TextView txtMessage;
        TextView txtDate;
        RelativeLayout relativeLayout;
        public SimpleViewHolder(View itemView) {
            super(itemView);
            txtMessage = (TextView) itemView.findViewById(R.id.txtMessage);
            txtDate = (TextView) itemView.findViewById(R.id.txtDate);
            relativeLayout = (RelativeLayout)itemView.findViewById(R.id.layNotification);
        }
    }

    //http://api.sawbros.com/main/notifications.json/id/1/offset/0/usertype/2

    private Context mContext;
    private ArrayList<NotificationModel> mDataset;
    private NotificationInterface notificationInterface;

    //protected SwipeItemRecyclerMangerImpl mItemManger = new SwipeItemRecyclerMangerImpl(this);

    public RecyclerViewAdapter(Context context, ArrayList<NotificationModel> objects, NotificationInterface notificationInterface) {
        this.mContext = context;
        this.mDataset = objects;
        this.notificationInterface = notificationInterface;
    }

    @Override
    public SimpleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.inflate_notification_1, parent, false);
        return new SimpleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final SimpleViewHolder viewHolder, final int position) {
//        mItemManger.bindView(viewHolder.itemView, position);

        NotificationModel item = mDataset.get(position);
        viewHolder.txtMessage.setText(item.getMessage());
        viewHolder.txtDate.setText(item.getCreatedDate());
        viewHolder.txtMessage.setTag(position);
        viewHolder.txtDate.setTag(position);
        viewHolder.relativeLayout.setTag(position);

        viewHolder.relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int postion = (Integer) view.getTag();
                notificationInterface.notificationItemClick(mDataset.get(postion));
            }
        });

        viewHolder.relativeLayout.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                int postion = (Integer) view.getTag();
                notificationInterface.notificationItemLongClick(mDataset.get(postion));
                return false;
            }
        });
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public interface NotificationInterface {
        public void notificationItemClick(NotificationModel notificationModel);
        public void notificationItemLongClick(NotificationModel notificationModel);
        public void gotoBottomScreen();
    }
}