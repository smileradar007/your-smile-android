package patient.com.yoursmile.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import patient.com.yoursmile.R;
import patient.com.yoursmile.model.AlbumHistoryModel;

/**
 * Created by vineet on 17/12/15.
 */
public class AlbumHistoryModelAdapter extends RecyclerView.Adapter<AlbumHistoryModelAdapter.AlbumViewHolder> {
    public static class AlbumViewHolder extends RecyclerView.ViewHolder {
        private ImageView mImgLeft, mImgCenter, mImgRight;
        private TextView txtTime;

        public AlbumViewHolder(View itemView) {
            super(itemView);
            mImgCenter = (ImageView) itemView.findViewById(R.id.imgCenter);
            mImgLeft = (ImageView) itemView.findViewById(R.id.imgLeft);
            mImgRight = (ImageView) itemView.findViewById(R.id.imgRight);
            txtTime = (TextView) itemView.findViewById(R.id.txtTime);

        }
    }

    public interface AlbumHistoryAdapterInteface {
        public void clickedOnItem(int position, int type);
    }

    private Context mContext;
    private ArrayList<AlbumHistoryModel> mlist;
    private AlbumHistoryAdapterInteface albumHistoryAdapterInteface;

    //protected SwipeItemRecyclerMangerImpl mItemManger = new SwipeItemRecyclerMangerImpl(this);

    public AlbumHistoryModelAdapter(Context context, ArrayList<AlbumHistoryModel> objects, AlbumHistoryAdapterInteface notificationInterface) {
        this.mContext = context;
        this.mlist = objects;
        this.albumHistoryAdapterInteface = notificationInterface;
    }

    @Override
    public AlbumViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.inflate_adapter_album_history, parent, false);
        return new AlbumViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AlbumHistoryModelAdapter.AlbumViewHolder viewHolder, int position) {
        final AlbumHistoryModel historyModel = mlist.get(position);

        if (historyModel != null) {

            viewHolder.txtTime.setText(historyModel.getCreatedDate());

            viewHolder.mImgCenter.setTag(position);
            viewHolder.mImgLeft.setTag(position);
            viewHolder.mImgRight.setTag(position);

            if (historyModel.getCenterImage() != null && historyModel.getCenterImage().length() > 0) {

                final ImageView imageView = viewHolder.mImgCenter;
                Picasso.with(mContext)
                        .load(historyModel.getCenterImage())
                        .resizeDimen(R.dimen.adpter_album_history_image, R.dimen.adpter_album_history_image)
                        .into(imageView, new Callback() {
                            @Override
                            public void onSuccess() {
                            }

                            @Override
                            public void onError() {
                                Picasso.with(mContext)
                                        .load(historyModel.getCenterImage())
                                        .resizeDimen(R.dimen.adpter_album_history_image, R.dimen.adpter_album_history_image)
                                        .into(imageView, new Callback() {
                                            @Override
                                            public void onSuccess() {
                                            }

                                            @Override
                                            public void onError() {
                                            }
                                        });
                            }
                        });
            }

            if (historyModel.getLeftImage() != null && historyModel.getLeftImage().length() > 0) {

                final ImageView imageView = viewHolder.mImgLeft;
                Picasso.with(mContext)
                        .load(historyModel.getLeftImage())
                        .resizeDimen(R.dimen.adpter_album_history_image, R.dimen.adpter_album_history_image)
                        .into(imageView, new Callback() {
                            @Override
                            public void onSuccess() {
                            }

                            @Override
                            public void onError() {
                                Picasso.with(mContext)
                                        .load(historyModel.getLeftImage())
                                        .resizeDimen(R.dimen.adpter_album_history_image, R.dimen.adpter_album_history_image)
                                        .into(imageView, new Callback() {
                                            @Override
                                            public void onSuccess() {
                                            }

                                            @Override
                                            public void onError() {
                                            }
                                        });
                            }
                        });
            }
            if (historyModel.getRightImage() != null && historyModel.getRightImage().length() > 0) {
                final ImageView imageView = viewHolder.mImgRight;
                Picasso.with(mContext)
                        .load(historyModel.getRightImage())
                        .resizeDimen(R.dimen.adpter_album_history_image, R.dimen.adpter_album_history_image)
                        .into(imageView, new Callback() {
                            @Override
                            public void onSuccess() {
                            }

                            @Override
                            public void onError() {
                                Picasso.with(mContext)
                                        .load(historyModel.getRightImage())
                                        .resizeDimen(R.dimen.adpter_album_history_image, R.dimen.adpter_album_history_image)
                                        .into(imageView, new Callback() {
                                            @Override
                                            public void onSuccess() {
                                            }

                                            @Override
                                            public void onError() {
                                            }
                                        });
                            }
                        });
            }

            viewHolder.mImgCenter.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = (int) v.getTag();
                    albumHistoryAdapterInteface.clickedOnItem(position, 2);
                }
            });

            viewHolder.mImgLeft.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = (int) v.getTag();
                    albumHistoryAdapterInteface.clickedOnItem(position, 1);
                }
            });


            viewHolder.mImgRight.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = (int) v.getTag();
                    albumHistoryAdapterInteface.clickedOnItem(position, 3);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return mlist.size();
    }
}